#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <vector>

#include "helper.cpp"
#include "python_c_extension/Volume_filter.h"


std::vector<double> read(int &height, int &width) {

    std::ifstream infile("data.file", std::ifstream::binary);

    infile.read(reinterpret_cast<char *>(&height), sizeof(int));
    infile.read(reinterpret_cast<char *>(&width), sizeof(int));

    double d;
    std::vector<double> res(width * height);
    size_t counter = 0;

    while (infile.read(reinterpret_cast<char *>(&d), sizeof(double))) {
        res[counter++] = d;
    }
    infile.close();

    return res;
}


int main() {
    int height, width;
    int radius = 11;
    auto d = read(height, width);
    auto v = new ii_filter::Volume_filter(std::move(d), height, width, radius, true);
    auto res = v->filter();

    debug_save("result.file", res, height - ((radius * 2) + 1), width - ((radius * 2) + 1));

    return 0;
}