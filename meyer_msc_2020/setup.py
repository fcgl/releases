#!/usr/bin/env python

from setuptools import setup, Extension
from distutils.command.build_ext import build_ext

compiler_args = {'msvc': ['/openmp', '/Ox', '/fp:fast'],
                 'mingw32': ['-fopenmp', '-O3', '-ffast-math'],
                 'unix': ['-fopenmp', '-O3', '-ffast-math'],
                 'cygwin': ['-fopenmp', '-O3', '-ffast-math'],
                 'bcpp': []}

linker_args = {'msvc': [],
               'mingw32': ['-fopenmp'],
               'unix': ['-fopenmp'],  # -lgomp
               'cygwin': ['-fopenmp'],
               'bcpp': []}


class build_ext_crossplatform(build_ext):
    def build_extensions(self):
        compiler = self.compiler.compiler_type
        print("using: %s" % compiler)
        if compiler in compiler_args:
            for e in self.extensions:
                e.extra_compile_args = compiler_args[compiler]
        if compiler in linker_args:
            for e in self.extensions:
                e.extra_link_args = linker_args[compiler]
        build_ext.build_extensions(self)


setup(name='ii_filter',
      version='1.0',
      author_email='lukas.meyer@stud.uni-heidelberg.de',
      packages=['ii_filter'],
      package_dir={'ii_filter': 'module_ii_filter'},
      ext_package='ii_filter',
      install_requires=["numpy", "scipy"],
      cmdclass={'build_ext': build_ext_crossplatform},
      ext_modules=[Extension('_ii_filter',
                             include_dirs=['python_c_extension'],
                             sources=["python_c_extension/ii_filterWrapper.cpp",
                                      "python_c_extension/Surface_filter.cpp",
                                      "python_c_extension/Volume_filter.cpp", "helper.cpp"])],
      zip_safe=True
      )
