#include <vector>
#include <string>
#include <iostream>
#include <sstream>
#include <fstream>

static void debug_save(std::string name, std::vector<double> data, int height, int width) {
    std::ofstream outfile(name, std::ofstream::binary);

    outfile.write(reinterpret_cast<char *>(&height), sizeof(int));
    outfile.write(reinterpret_cast<char *>(&width), sizeof(int));

    std::vector<double> res;

    for (auto value:data) {
        outfile.write(reinterpret_cast<char *>(&value), sizeof(double));
    }
    outfile.close();
}

