#!/usr/bin/env python

import time
import numpy as np
from scipy.signal import convolve2d
from multiprocessing import cpu_count

# c++ module
from . import _ii_filter


def kirsch_filter(im):
    g_kirsch_000 = [[5, 5, 5], [-3, 0, -3], [-3, -3, -3]]
    g_kirsch_045 = [[5, 5, -3], [5, 0, -3], [-3, -3, -3]]
    g_kirsch_090 = [[5, -3, -3], [5, 0, -3], [5, -3, -3]]
    g_kirsch_135 = [[-3, -3, -3], [5, 0, -3], [5, 5, -3]]
    g_kirsch_180 = [[-3, -3, -3], [-3, 0, -3], [5, 5, 5]]
    g_kirsch_225 = [[-3, -3, -3], [-3, 0, 5], [-3, 5, 5]]
    g_kirsch_270 = [[-3, -3, 5], [-3, 0, 5], [-3, -3, 5]]
    g_kirsch_315 = [[-3, 5, 5], [-3, 0, 5], [-3, -3, -3]]

    g1 = convolve2d(im, g_kirsch_000, mode="same")
    g2 = convolve2d(im, g_kirsch_045, mode="same")
    g3 = convolve2d(im, g_kirsch_090, mode="same")
    g4 = convolve2d(im, g_kirsch_135, mode="same")
    g5 = convolve2d(im, g_kirsch_180, mode="same")
    g6 = convolve2d(im, g_kirsch_225, mode="same")
    g7 = convolve2d(im, g_kirsch_270, mode="same")
    g8 = convolve2d(im, g_kirsch_315, mode="same")

    return np.maximum.reduce([g1, g2, g3, g4, g5, g6, g7, g8])


def calc_number_of_threads(parallel, number_of_threads):
    if parallel:
        max_count = cpu_count()
        if number_of_threads >= max_count or number_of_threads <= 0:
            number_of_threads = max_count
        return number_of_threads
    else:
        return 1


def iisurface_filter(im, radius, parallel=True, number_of_threads=0):
    start = time.time()
    height, width = im.shape
    filterwidth = int((radius * 2) + 1)
    G_ks = kirsch_filter(im)

    number_of_threads = calc_number_of_threads(parallel, number_of_threads)

    result = _ii_filter.surface_filter(list(im.flatten()), list(G_ks.flatten()), height, width, radius,
                                       number_of_threads)

    result = np.array(result).reshape([height - filterwidth, width - filterwidth])

    print("iisurface_filter_c: filterwidth %s, time in sec %s" % (filterwidth, time.time() - start))

    return result


def iivolume_filter(im, radius, parallel=True, number_of_threads=0):
    start = time.time()
    height, width = im.shape
    filterwidth = int((radius * 2) + 1)

    number_of_threads = calc_number_of_threads(parallel, number_of_threads)

    result = _ii_filter.volume_filter(list(im.flatten()), height, width, radius, number_of_threads)

    result = np.array(result).reshape([height - filterwidth, width - filterwidth])

    print("iivolume_filter_c: filterwidth %s, time in sec %s" % (filterwidth, time.time() - start))

    return result
