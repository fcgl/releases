#include <iostream>
#include <vector>
#include <cmath>
#include <fstream>
#include <algorithm>
#include <numeric>
#include <omp.h>

#include "../helper.cpp"
#include "Surface_filter.h"

namespace ii_filter {

    Surface_filter::Surface_filter(std::vector<double> &&im, std::vector<double> &&gradient, int height, int width,
                                   int radius, int numberOfThreads) {
        this->im = im;
        this->gradient = gradient;
        this->width = width;
        this->height = height;
        this->filterwidth = (radius * 2) + 1;
        this->numberOfThreads = numberOfThreads;

        this->radiiBaseSquared.resize(this->filterwidth * this->filterwidth);

        size_t x = 0;
        size_t y = 0;
        double radiiBase;
        for (size_t i = 0; i < (this->filterwidth * this->filterwidth); i++) {
            x = i / this->filterwidth - radius;
            y = i % this->filterwidth - radius;
            radiiBase = std::sqrt(x * x + y * y) / radius;
            this->radiiBaseSquared[i] = radiiBase * radiiBase;
        }
        //debug_save("radiiBaseSquared.file", this->radiiBaseSquared, this->filterwidth, this->filterwidth);

    }

    std::vector<double> Surface_filter::filter() {
        std::vector<double> result((this->width - this->filterwidth) * (this->height - this->filterwidth));

        #pragma omp parallel for num_threads(this->numberOfThreads)
        for (long image_index = 0; image_index < result.size(); image_index++) {

            if (image_index == 0) {
                std::cout << "OMPThreads: " << omp_get_num_threads() << std::endl;
            }

            size_t ix = int(image_index / (this->width - filterwidth));
            size_t iy = image_index % (this->width - filterwidth);

            size_t img_pos = ix * this->width + iy;

            //std::cout << ix << " " << iy << " " << img_pos << std::endl;

            size_t central_x = ix + int((this->filterwidth / 2));
            size_t central_y = iy + int((this->filterwidth / 2));

            double centralpix = this->im[central_x * this->width + central_y];

            std::vector<double> small_result(this->filterwidth * this->filterwidth);

            for (size_t i = 0; i < small_result.size(); i++) {
                double sub_center_square = (this->im[img_pos] - centralpix) * (this->im[img_pos] - centralpix);

                double subimage_distances = std::sqrt(this->radiiBaseSquared[i] + sub_center_square);

                if (subimage_distances <= 1.0) {
                    small_result[i] = this->gradient[img_pos];
                } else {
                    small_result[i] = 0.0;
                }
                img_pos++;

                //jump with img_pos to next row
                if (((i + 1) % this->filterwidth) == 0) {
                    img_pos = img_pos + this->width - this->filterwidth;
                }
            }

            result[image_index] = accumulate(small_result.begin(), small_result.end(), 0.0);
        }


        return result;
    }

}