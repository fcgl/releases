#include <Python.h>

#include <iostream>
#include <vector>
#include <cstdio>
#include <stdexcept>

#include "Volume_filter.h"
#include "Surface_filter.h"


long py_to_long(PyObject *arg) {
    if (PyLong_Check(arg)) {
        return PyLong_AsLong(arg);
    } else {
        throw std::logic_error("wrong param");
    }
}

void py_to_vector(PyObject *arg, std::vector<double> &vec) {
    if (PyList_Check(arg)) {
        for (Py_ssize_t i = 0; i < PyList_Size(arg); i++) {
            PyObject *value = PyList_GetItem(arg, i);
            vec.push_back(PyFloat_AsDouble(value));
        }
    } else {
        throw std::logic_error("wrong list");
    }
}

PyObject *vec_to_py(std::vector<double> &vec) {
    PyObject *res = PyTuple_New(vec.size());
    if (!res) {
        std::cout << "Unable to allocate memory for Python tuple" << std::endl;
        throw std::logic_error("Unable to allocate memory for Python tuple");
    }

    for (unsigned int i = 0; i < vec.size(); i++) {
        PyObject *num = PyFloat_FromDouble((double) vec[i]);
        if (!num) {
            Py_DECREF(res);
            std::cout << "Unable to allocate memory for Python tuple" << std::endl;
            throw std::logic_error("Unable to allocate memory for Python tuple");
        }
        PyTuple_SET_ITEM(res, i, num);
    }
    return res;
}


void parse_py_volume_args(PyObject *args, std::vector<double> &c_im, int &c_height, int &c_width, int &c_radius, int &c_number_of_threads) {
    PyObject *im;
    PyObject *width;
    PyObject *height;
    PyObject *radius;
    PyObject *number_of_threads;

    if (!PyArg_UnpackTuple(args, "init", 5, 5, &im, &height, &width, &radius, &number_of_threads))
        throw std::logic_error("wrong parameter");

    py_to_vector(im, c_im);

    c_height = py_to_long(height);
    c_width = py_to_long(width);
    c_radius = py_to_long(radius);
    c_number_of_threads = py_to_long(number_of_threads);
}


void parse_py_surface_args(PyObject *args, std::vector<double> &c_im, std::vector<double> &c_gradient, int &c_height,
                           int &c_width, int &c_radius, int &c_number_of_threads) {
    PyObject *im;
    PyObject *gradient;
    PyObject *width;
    PyObject *height;
    PyObject *radius;
    PyObject *number_of_threads;

    if (!PyArg_UnpackTuple(args, "init", 6, 6, &im, &gradient, &height, &width, &radius, &number_of_threads))
        throw std::logic_error("wrong parameter");

    py_to_vector(im, c_im);
    py_to_vector(gradient, c_gradient);

    c_height = py_to_long(height);
    c_width = py_to_long(width);
    c_radius = py_to_long(radius);
    c_number_of_threads = py_to_long(number_of_threads);
}

static PyObject *volume_filter(PyObject *self, PyObject *args) {

    std::vector<double> c_im;
    int c_width, c_height, c_radius, c_number_of_threads;
    parse_py_volume_args(args, c_im, c_height, c_width, c_radius, c_number_of_threads);

    auto obj = ii_filter::Volume_filter(std::move(c_im), c_height, c_width, c_radius, c_number_of_threads);

    std::vector<double> data;
    try {
        data = obj.filter();
    } catch (const std::exception &ex) {
        std::cout << "std::exception: " << ex.what() << std::endl;
        PyErr_Format(PyExc_ValueError,
                     "std::exception: %s", ex.what());
        return NULL;
    }

    return vec_to_py(data);
}

static PyObject *surface_filter(PyObject *self, PyObject *args) {
    std::vector<double> c_im, c_gradient;
    int c_width, c_height, c_radius, c_number_of_threads;
    parse_py_surface_args(args, c_im, c_gradient, c_height, c_width, c_radius, c_number_of_threads);


    auto obj = ii_filter::Surface_filter(std::move(c_im), std::move(c_gradient), c_height, c_width, c_radius, c_number_of_threads);
    std::vector<double> data;
    try {
        data = obj.filter();
    } catch (const std::exception &ex) {
        std::cout << "std::exception: " << ex.what() << std::endl;
        PyErr_Format(PyExc_ValueError,
                     "std::exception: %s", ex.what());
        return NULL;
    }


    return vec_to_py(data);
}


static PyMethodDef ii_filter_methods[] = {
        {"volume_filter",  volume_filter,  METH_VARARGS, "integral invariant volume filter"},
        {"surface_filter", surface_filter, METH_VARARGS, "integral invariant surface filter"},
        {NULL, NULL, 0, NULL}        /* Sentinel */
};

static struct PyModuleDef ii_filter_module = {
        PyModuleDef_HEAD_INIT,
        "_ii_filter",   /* name of module */
        "integral invariant filter methods", /* module documentation, may be NULL */
        -1,       /* size of per-interpreter state of the module,
                 or -1 if the module keeps state in global variables. */
        ii_filter_methods
};


PyMODINIT_FUNC
PyInit__ii_filter(void) {
    return PyModule_Create(&ii_filter_module);
}