#include <iostream>
#include <vector>
#include <cmath>
#include <fstream>
#include <algorithm>
#include <numeric>
#include <omp.h>

#include "../helper.cpp"
#include "Volume_filter.h"

namespace ii_filter {
    Volume_filter::Volume_filter(std::vector<double> &&im, int height, int width, int radius, int numberOfThreads) {
        this->im = im;
        this->width = width;
        this->height = height;
        this->filterwidth = (radius * 2) + 1;
        this->numberOfThreads = numberOfThreads;

        double radiiBase;
        this->s_half.resize(this->filterwidth * this->filterwidth);
        this->s_plus.resize(this->filterwidth * this->filterwidth);

        size_t x = 0;
        size_t y = 0;
        for (size_t i = 0; i < (this->filterwidth * this->filterwidth); i++) {
            x = i / this->filterwidth - radius;
            y = i % this->filterwidth - radius;
            radiiBase = std::sqrt(x * x + y * y) / radius;
            if (radiiBase <= 1) {
                this->s_half[i] = std::sqrt(1 - (radiiBase * radiiBase));
                this->s_plus[i] = this->s_half[i] * 2;
            } else {
                this->s_half[i] = 0;
                this->s_plus[i] = 0;
            }
        }
        this->s_plus_sum = std::accumulate(this->s_plus.begin(), this->s_plus.end(), 0.0);
        //debug_save("s_half.file", this->s_half, this->filterwidth, this->filterwidth);
    }

    std::vector<double> Volume_filter::filter() {
        std::vector<double> result((this->width - this->filterwidth) * (this->height - this->filterwidth));

        #pragma omp parallel for num_threads(this->numberOfThreads)
        for (long image_index = 0; image_index < result.size(); image_index++) {

            if (image_index == 0) {
                std::cout << "OMPThreads: " << omp_get_num_threads() << std::endl;
            }

            size_t ix = int(image_index / (this->width - filterwidth));
            size_t iy = image_index % (this->width - filterwidth);

            size_t img_pos = ix * this->width + iy;

            //std::cout << ix << " " << iy << " " << img_pos << std::endl;

            size_t central_x = ix + int((this->filterwidth / 2));
            size_t central_y = iy + int((this->filterwidth / 2));

            double centralpix = this->im[central_x * this->width + central_y];
            std::vector<double> small_result(this->filterwidth * this->filterwidth);

            for (size_t i = 0; i < small_result.size(); i++) {
                double subimg_dali = this->im[img_pos++] + this->s_half[i] - centralpix;

                if (subimg_dali < this->s_plus[i]) {
                    small_result[i] = std::max(subimg_dali, 0.0);
                } else {
                    small_result[i] = std::max(this->s_plus[i], 0.0);
                }

                //jump with img_pos to next row
                if (((i + 1) % this->filterwidth) == 0) {
                    img_pos = img_pos + this->width - this->filterwidth;
                }
            }

            //debug_save("small_result.file", small_result, this->filterwidth, this->filterwidth);
            double res_pixel = accumulate(small_result.begin(), small_result.end(), 0.0);
            result[image_index] = res_pixel / s_plus_sum;
        }
        //debug_save("result.file", result, this->width - filterwidth, this->height - filterwidth);
        return result;
    }

}