#include <vector>

namespace ii_filter {

    class Volume_filter {
    public:
        int width, height, filterwidth, numberOfThreads;
        std::vector<double> im, s_half, s_plus;
        double s_plus_sum;

        Volume_filter(std::vector<double> &&im, int height, int width, int radius, int numberOfThreads);

        std::vector<double> filter();
    };

}