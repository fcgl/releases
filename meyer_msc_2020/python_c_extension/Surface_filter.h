#include <vector>

namespace ii_filter {

    class Surface_filter {
    public:
        int width, height, filterwidth, numberOfThreads;
        std::vector<double> im, gradient, radiiBaseSquared;

        Surface_filter(std::vector<double> &&im, std::vector<double> &&gradient, int height, int width, int radius,
                       int numberOfThreads);

        std::vector<double> filter();
    };

}