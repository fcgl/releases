Here per-tablet files will be written, e.g. `HS0092.json` if the respective
option `-f` has been set.

As an example see `HS_768.json`. Most files have the following sections.

    {
        "from_gigamesh_info_file": {
            ...
        },
        "from_ply_filename": {
            ...
        },
        "from_hilprecht3d": {
            "hs_number": "HS 0768",
            "tablet_url": "https://hilprecht.mpiwg-berlin.mpg.de/object3d/31959",
            "bibliography": "1935: TMH 5, 018. \u2013 1975: ECTJ 18",
            "cdli_number": "20432"
        },
        "from_cdli_search_view": {
            ...
            "primary_publication": "TMH 05, 018",
            "author": "Pohl, Alfred",
            "publication_date": "1935",
            "secondary_publication": "A. Westenholz, ECTJ 18",
            "collection": "Hilprecht Collection, University of Jena, Germany",
            "museum_number": "HS 0768",
            "accession_number": "",
            "provenience": "Nippur (mod. Nuffar)",
            "excavation_number": "",
            "period": "ED IIIb (ca. 2500-2340 BC)",
            "dates_referenced": "",
            "object_type": "tablet",
            "remarks": "",
            "material": "clay",
            "language": "Sumerian",
            "genre": "Administrative",
            "sub_genre": "",
            "cdli_comments": "",
            "catalogue_source": "20040921 krebernik_tmh5",
            "atf_source": "cdlistaff",
            "translation": "no translation",
            "ucla_library_ark": "21198/zz001td5xd",
            "composite_number": "",
            "seal_number": "",
            "cdli_number": "P020432"
        },
        "from_cdli_archival_view": {
            ...
            "catalogue": "20040921 krebernik_tmh5",
            "transliteration_remarks": "cdlistaff",
            "translation_remarks": "no translation",
            "owner": "Hilprecht Collection, University of Jena, Germany",
            ...
            "textual_content": "@Tablet\n@obverse \ncolumn 1 \n1. 1(u@c) 6(asz@c) lu2 \n2. lugal-x-szu2-DU \n3. 7(asz@c) lu2 \n4. ur-tur \n5. 5(asz@c) lu2 \n6. puzur4-asz-dar \ncolumn 2 \n1. 7(asz@c)# lu2# \n2. u3#-ma-ni \n$blank space \n@reverse \n$blank space ",
            "lineart_url": "https://cdli.ucla.edu/dl/tn_lineart/P020432_l.jpg",
            "photo_url": "https://cdli.ucla.edu/dl/tn_photo/P020432.jpg"
        },
        "from_gigamesh_info_program": {
            ...
        }
    }
