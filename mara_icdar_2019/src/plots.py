# coding=utf-8

import json
import numpy
import matplotlib.pyplot


def main():
    matplotlib.rcParams.update({'font.size': 16})

    print("Loading database from results/database.json ...")
    with open("results/database.json", "r") as f:
        database = json.load(f)

    # Collect data

    array_of_numbers = []
    array_of_areas = []
    array_of_volumes = []
    array_of_faces_changed = []
    array_of_vertices_changed = []
    array_of_bounding_box_widths = []
    array_of_bounding_box_heigths = []
    array_of_bounding_box_thicknesses = []
    array_of_text_lengths = []
    array_of_vertex_counts = []
    array_of_face_counts = []
    array_of_dpi = []
    array_of_cleaning_duration_in_s = []
    array_of_msii_duration_in_s = []

    for key, record in database.items():
        if "area_in_cm2" not in record["from_gigamesh_info_file"]:
            print("FIXME: Fix the database and remove this check")
            continue

        array_of_numbers.append(key)

        cleaning_duration_in_s = record["from_gigamesh_info_clean_file"]\
            ["processing_wall_time_in_s"]
        msii_duration_in_s = record["from_gigamesh_info_file"]\
            ["processing_wall_time_in_s"]
        vertex_count = record["from_gigamesh_info_file"]["count_of_vertices"]
        face_count = record["from_gigamesh_info_file"]["count_of_faces"]
        area = record["from_gigamesh_info_file"]["area_in_cm2"]
        volume_dx = record["from_gigamesh_info_file"]["volume_dx_in_cm3"]
        volume_dy = record["from_gigamesh_info_file"]["volume_dy_in_cm3"]
        volume_dz = record["from_gigamesh_info_file"]["volume_dz_in_cm3"]
        faces_changed = record["from_gigamesh_info_clean_file"]\
            ["count_of_faces_diff"]
        vertices_changed = record["from_gigamesh_info_clean_file"]\
            ["count_of_vertices_diff"]
        bounding_box_width = record["from_gigamesh_info_program"]\
            ["boundingbox_width"]
        bounding_box_height = record["from_gigamesh_info_program"]\
            ["boundingbox_height"]
        bounding_box_thickness = record["from_gigamesh_info_program"]\
            ["boundingbox_thick"]

        if "from_cdli_archival_view" in record:
            text_length = \
                len(record["from_cdli_archival_view"]["textual_content"])
        else:
            text_length = 0

        cleaning_duration_in_s = float(cleaning_duration_in_s)
        msii_duration_in_s = float(msii_duration_in_s)
        vertex_count = int(vertex_count)
        face_count = int(face_count)
        area = float(area)
        volume = max([float(volume_dx), float(volume_dy), float(volume_dz)])
        faces_changed = int(faces_changed)
        vertices_changed = int(vertices_changed)
        bounding_box_width = float(bounding_box_width)
        bounding_box_height = float(bounding_box_height)
        bounding_box_thickness = float(bounding_box_thickness)

        array_of_cleaning_duration_in_s.append(cleaning_duration_in_s)
        array_of_msii_duration_in_s.append(msii_duration_in_s)
        array_of_areas.append(area)
        array_of_volumes.append(volume)
        array_of_faces_changed.append(faces_changed)
        array_of_vertices_changed.append(vertices_changed)
        array_of_bounding_box_widths.append(bounding_box_width)
        array_of_bounding_box_heigths.append(bounding_box_height)
        array_of_bounding_box_thicknesses.append(bounding_box_thickness)
        array_of_text_lengths.append(text_length)
        array_of_vertex_counts.append(vertex_count)
        array_of_face_counts.append(face_count)

    array_of_cleaning_duration_in_s = \
        numpy.array(array_of_cleaning_duration_in_s)
    array_of_msii_duration_in_s = \
        numpy.array(array_of_msii_duration_in_s)
    array_of_areas = numpy.array(array_of_areas)
    array_of_volumes = numpy.array(array_of_volumes)
    array_of_faces_changed = numpy.array(array_of_faces_changed)
    array_of_bounding_box_widths = numpy.array(array_of_bounding_box_widths)
    array_of_bounding_box_heigths = \
        numpy.array(array_of_bounding_box_heigths)
    array_of_bounding_box_thicknesses = \
        numpy.array(array_of_bounding_box_thicknesses)
    array_of_text_lengths = numpy.array(array_of_text_lengths)
    array_of_vertex_counts = numpy.array(array_of_vertex_counts)
    array_of_face_counts = numpy.array(array_of_face_counts)

    # Processing stats

    print("Sum of cleaning durations in seconds {}"
          .format(numpy.sum(array_of_cleaning_duration_in_s)))

    print("Sum of MSII durations in seconds {}"
          .format(numpy.sum(array_of_msii_duration_in_s)))

    fig, axes = matplotlib.pyplot.subplots(1, 1, dpi=90, figsize=(8, 4))
    axes.hist(array_of_cleaning_duration_in_s, bins=100)
    axes.set_ylabel("Count")
    axes.set_yscale("log")
    axes.set_xlabel("Cleaning duration in seconds")
    fig.tight_layout()
    fig.savefig("results/cleaning_duration_histogram.png")

    fig, axes = matplotlib.pyplot.subplots(1, 1, dpi=90, figsize=(8, 4))
    axes.hist(array_of_msii_duration_in_s, bins=100)
    axes.set_ylabel("Count")
    axes.set_yscale("log")
    axes.set_xlabel("MSII duration in seconds")
    fig.tight_layout()
    fig.savefig("results/msii_duration_histogram.png")

    # Vertex and resolution stats

    quantiles = numpy.quantile(array_of_vertex_counts,
                                 [0, 0.25, 0.5, 0.75, 1.0])
    print("Quantiles of vertex counts: {}".format(str(quantiles)))

    array_of_resolutions = \
        numpy.sqrt(array_of_vertex_counts / (array_of_areas * 10**2)) * 25.4

    fig, axes = matplotlib.pyplot.subplots(1, 1, dpi=90, figsize=(8, 4))
    axes.hist(array_of_resolutions, bins=100)
    axes.set_ylabel("Count")
    axes.set_yscale("log")
    axes.set_xlabel("Resolution in DPI")
    fig.tight_layout()
    fig.savefig("results/resolution_histogram.png")

    print("Mean of resolutions: {}".format(numpy.mean(array_of_resolutions)))
    print("Standard deviation of resolutions: {}"
          .format(numpy.std(array_of_resolutions)))

    quantiles = numpy.quantile(array_of_resolutions,
                                 [0, 0.25, 0.5, 0.75, 1.0])
    print("Quantiles of resolutions: {}".format(str(quantiles)))

    threshold = numpy.quantile(array_of_resolutions, 0.005)
    mask = array_of_resolutions < threshold
    numbers = [array_of_numbers[i] for i in numpy.nonzero(mask)[0]]
    print("Tablets with resolutions below {}:\n{}"
          .format(threshold, ", ".join(map(str, numbers))))

    threshold = numpy.quantile(array_of_resolutions, 0.995)
    mask = array_of_resolutions > threshold
    numbers = [array_of_numbers[i] for i in numpy.nonzero(mask)[0]]
    print("Tablets with resolutions above {}:\n{}"
          .format(threshold, ", ".join(map(str, numbers))))

    fig, axes = matplotlib.pyplot.subplots(1, 1, dpi=90, figsize=(8, 4))
    axes.hist(array_of_vertex_counts, bins=100)
    axes.set_ylabel("Count")
    axes.set_yscale("log")
    axes.set_xlabel("Vertices")
    fig.tight_layout()
    fig.savefig("results/vertex_count_histogram.png")

    fig, axes = matplotlib.pyplot.subplots(1, 1, dpi=90, figsize=(8, 4))
    axes.hist(array_of_face_counts, bins=100)
    axes.set_ylabel("Count")
    axes.set_yscale("log")
    axes.set_xlabel("Faces")
    fig.tight_layout()
    fig.savefig("results/face_count_histogram.png")

    # Volume versus area

    fig, axes = matplotlib.pyplot.subplots(1, 1, dpi=90, figsize=(8, 4))
    axes.scatter(array_of_areas, array_of_volumes)
    axes.set_xscale("log")
    axes.set_xlabel("Area in cm²")
    axes.set_yscale("log")
    axes.set_ylabel("Volume in cm³")
    fig.tight_layout()
    fig.savefig("results/area_vs_volume.png")

    # Area / volume ratio

    array_of_av_ratio = array_of_areas / array_of_volumes

    fig, axes = matplotlib.pyplot.subplots(1, 1, dpi=90, figsize=(8, 4))
    axes.hist(array_of_av_ratio, bins=100)
    axes.set_ylabel("Count")
    axes.set_yscale("log")
    axes.set_xlabel("Ratio in 1/cm")
    fig.tight_layout()
    fig.savefig("results/area_to_volume_ratio_histogram.png")

    threshold = numpy.quantile(array_of_av_ratio, 0.995)
    mask = array_of_av_ratio > threshold
    numbers = [array_of_numbers[i] for i in numpy.nonzero(mask)[0]]
    print("Tablets with area to volume ratio above {}:\n{}"
          .format(threshold, ", ".join(map(str, numbers))))

    # Area histogram

    fig, axes = matplotlib.pyplot.subplots(1, 1, dpi=90, figsize=(8, 4))
    axes.hist(array_of_areas, bins=100)
    axes.set_ylabel("Count")
    axes.set_yscale("log")
    axes.set_xlabel("Area in cm²")
    fig.tight_layout()
    fig.savefig("results/area_histogram.png")

    fig, axes = matplotlib.pyplot.subplots(1, 1, dpi=90, figsize=(8, 4))
    axes.hist(array_of_areas, bins=100, range=(0, 300))
    axes.set_ylabel("Count")
    axes.set_yscale("log")
    axes.set_xlabel("Area in cm²")
    fig.tight_layout()
    fig.savefig("results/area_histogram_cropped.png")

    # Volume histogram

    fig, axes = matplotlib.pyplot.subplots(1, 1, dpi=90, figsize=(8, 4))
    axes.hist(array_of_volumes, bins=100)
    axes.set_ylabel("Count")
    axes.set_yscale("log")
    axes.set_xlabel("Volume in cm³")
    fig.tight_layout()
    fig.savefig("results/volume_histogram.png")

    fig, axes = matplotlib.pyplot.subplots(1, 1, dpi=90, figsize=(8, 4))
    axes.hist(array_of_volumes, bins=100, range=(0, 300))
    axes.set_ylabel("Count")
    axes.set_yscale("log")
    axes.set_xlabel("Volume in cm³")
    fig.tight_layout()
    fig.savefig("results/volume_histogram_cropped.png")

    # Faces changed histogram

    fig, axes = matplotlib.pyplot.subplots(1, 1, dpi=90, figsize=(8, 4))
    axes.hist(array_of_faces_changed, bins=100)
    axes.set_ylabel("Count")
    axes.set_yscale("log")
    axes.set_xlabel("Faces changed")
    fig.tight_layout()
    fig.savefig("results/faces_changed_histogram.png")

    fig, axes = matplotlib.pyplot.subplots(1, 1, dpi=90, figsize=(8, 4))
    axes.hist(array_of_faces_changed, bins=100, range=(-5000, 20000))
    axes.set_ylabel("Count")
    axes.set_yscale("log")
    axes.set_xlabel("Faces changed")
    fig.tight_layout()
    fig.savefig("results/faces_changed_histogram_cropped.png")

    threshold = numpy.quantile(array_of_faces_changed, 0.005)
    mask = array_of_faces_changed < threshold
    numbers = [array_of_numbers[i]
                     for i in numpy.nonzero(mask)[0]]
    print("Tablets with count of faces changed below {}:\n{}"
          .format(threshold, ", ".join(map(str, numbers))))

    threshold = numpy.quantile(array_of_faces_changed, 0.995)
    mask = array_of_faces_changed > threshold
    numbers = [array_of_numbers[i]
                     for i in numpy.nonzero(mask)[0]]
    print("Tablets with count of faces changed above {}:\n{}"
          .format(threshold, ", ".join(map(str, numbers))))

    # Vertices changed histogram

    fig, axes = matplotlib.pyplot.subplots(1, 1, dpi=90, figsize=(8, 4))
    axes.hist(array_of_vertices_changed, bins=100)
    axes.set_yscale("log")
    axes.set_xlabel("Vertices changed")
    fig.tight_layout()
    fig.savefig("results/vertices_changed_histogram.png")

    fig, axes = matplotlib.pyplot.subplots(1, 1, dpi=90, figsize=(8, 4))
    axes.hist(array_of_vertices_changed, bins=100, range=(-5000, 10000))
    axes.set_yscale("log")
    axes.set_xlabel("Vertices changed")
    fig.tight_layout()
    fig.savefig("results/vertices_changed_histogram_cropped.png")

    threshold = numpy.quantile(array_of_vertices_changed, 0.005)
    mask = array_of_vertices_changed < threshold
    numbers = [array_of_numbers[i]
                     for i in numpy.nonzero(mask)[0]]
    print("Tablets with count of vertices changed below {}:\n{}"
          .format(threshold, ", ".join(map(str, numbers))))

    threshold = numpy.quantile(array_of_vertices_changed, 0.995)
    mask = array_of_vertices_changed > threshold
    numbers = [array_of_numbers[i]
                     for i in numpy.nonzero(mask)[0]]
    print("Tablets with count of vertices changed above {}:\n{}"
          .format(threshold, ", ".join(map(str, numbers))))

    # Bounding box histograms

    fig, axes = matplotlib.pyplot.subplots(1, 1, dpi=90, figsize=(8, 4))
    axes.hist(array_of_bounding_box_widths, bins=100)
    axes.set_ylabel("Count")
    axes.set_yscale("log")
    axes.set_xlabel("Bounding box widths")
    fig.tight_layout()
    fig.savefig("results/bounding_box_widths_histogram.png")

    fig, axes = matplotlib.pyplot.subplots(1, 1, dpi=90, figsize=(8, 4))
    axes.hist(array_of_bounding_box_heigths, bins=100)
    axes.set_ylabel("Count")
    axes.set_yscale("log")
    axes.set_xlabel("Bounding box heights")
    fig.tight_layout()
    fig.savefig("results/bounding_box_heights_histogram.png")

    fig, axes = matplotlib.pyplot.subplots(1, 1, dpi=90, figsize=(8, 4))
    axes.hist(array_of_bounding_box_thicknesses, bins=100)
    axes.set_ylabel("Count")
    axes.set_yscale("log")
    axes.set_xlabel("Bounding box thicknesses")
    fig.tight_layout()
    fig.savefig("results/bounding_box_thicknesses_histogram.png")

    # Bounding box volumes

    array_of_bounding_box_volumes = \
        array_of_bounding_box_widths * \
        array_of_bounding_box_heigths * \
        array_of_bounding_box_thicknesses

    fig, axes = matplotlib.pyplot.subplots(1, 1, dpi=90, figsize=(8, 4))
    axes.hist(array_of_bounding_box_volumes, bins=100)
    axes.set_ylabel("Count")
    axes.set_yscale("log")
    axes.set_xlabel("Bounding box volumes in cm³")
    fig.tight_layout()
    fig.savefig("results/bounding_box_volumes_histogram.png")

    # Area vs bounding box volume

    fig, axes = matplotlib.pyplot.subplots(1, 1, dpi=90, figsize=(8, 4))
    axes.scatter(array_of_areas, array_of_bounding_box_volumes)
    axes.set_xscale("log")
    axes.set_xlabel("Area in cm²")
    axes.set_yscale("log")
    axes.set_ylabel("Volume in cm³")
    fig.tight_layout()
    fig.savefig("results/area_vs_bounding_box_volume.png")

    # Textual content length histogram

    fig, axes = matplotlib.pyplot.subplots(1, 1, dpi=90, figsize=(8, 4))
    axes.hist(array_of_text_lengths, bins=100)
    axes.set_yscale("log")
    axes.set_xlabel("Textual content length")
    fig.tight_layout()
    fig.savefig("results/textual_content_length_histogram.png")

    fig, axes = matplotlib.pyplot.subplots(1, 1, dpi=90, figsize=(8, 4))
    axes.hist(array_of_text_lengths, bins=100, range=(0, 5000))
    axes.set_yscale("log")
    axes.set_xlabel("Textual content length")
    fig.tight_layout()
    fig.savefig("results/textual_content_length_histogram_cropped.png")

if __name__ == "__main__":
    main()
