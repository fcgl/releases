# coding=utf-8

import re
import json
import numpy
import hashlib
import argparse
import os.path
import requests
import subprocess


def extract_jena_meta(text):
    without_cdli = (
        r'''<tr>''' '\n'
        r'''<td><a href="(/object3d/\d+)">(HS \w+)</a>''' '\n'
        r'''<table>''' '\n'
        # No newline on the following line
        r'''<tr><td class="text-left text-top">Maße:'''
        r'''</td><td class="text-left">.*?</td></tr>''' '\n'
        # No newline on the following line
        r'''<tr><td class="text-left" valign="top" width="100px">'''
        # No newline at the end of the regular expression
        r'''Bibl.:</td><td class="text-left">(.*?)</td></tr>'''
    )

    with_cdli = (
        r'''<tr>''' '\n'
        r'''<td><a href="(/object3d/\d+)">(HS \w+)</a>''' '\n'
        r'''<table>''' '\n'
        # No newline on the following line
        r'''<tr><td class="text-left text-top">Maße:'''
        r'''</td><td class="text-left">.*?</td></tr>''' '\n'
        # No newline on the following line
        r'''<tr><td class="text-left" valign="top" width="100px">'''
        r'''Bibl.:</td><td class="text-left">(.*?)</td></tr>''' '\n'
        '\n'
        # No newlines on the following lines and the end of the regular
        # expression
        r'''<tr><td class="text-left">CDLI \(extern\):'''
        r'''</td><td valign="top" class="text-left">'''
        r'''<a href=".*?">(\d+)</a></td></tr>'''
    )

    records = []

    matches = re.finditer(without_cdli, text)
    for match in matches:
        record = {
            "hs_number": match.group(2),
            "tablet_url": ("https://hilprecht.mpiwg-berlin.mpg.de" +
                           match.group(1)),
            "bibliography": match.group(3)
        }
        records.append(record)

    matches = re.finditer(with_cdli, text)
    for match in matches:
        record = {
            "hs_number": match.group(2),
            "tablet_url": ("https://hilprecht.mpiwg-berlin.mpg.de" +
                           match.group(1)),
            "bibliography": match.group(3),
            "cdli_number": match.group(4)
        }
        records.append(record)

    return records


def test_extract_jena_meta():
    url = "https://hilprecht.mpiwg-berlin.mpg.de/search3d"
    text = requests.get(url).text
    records = extract_jena_meta(text)
    assert len(records) > 0


def extract_jena_pages(text):
    regexp = r"""<a href="\?page=(\d+)" class="page">(.*?)</a>"""
    matches = re.finditer(regexp, text)

    pages = []
    for match in matches:
        url = ("https://hilprecht.mpiwg-berlin.mpg.de/search3d?page="
               + match.group(1))
        pages.append(url)
    return pages


def test_extract_jena_pages():
    url = "https://hilprecht.mpiwg-berlin.mpg.de/search3d"
    reqres = requests.get(url)
    pages = extract_jena_pages(reqres.text)
    assert len(pages) > 1


def extract_cdli_meta(text):
    regexp = (
        r'''href="archival_view\.php\?ObjectID=(.*?)">'''
        r'''.*?'''
        r'''Primary publication</td><td>(.*?)</td>'''
        r'''.*?'''
        r'''Author\(s\)</td><td>(.*?)</td>'''
        r'''.*?'''
        r'''Publication date</td><td>(.*?)</td>'''
        r'''.*?'''
        r'''Secondary publication\(s\)</td><td>(.*?)</td>'''
        r'''.*?'''
        r'''Collection</td><td>(.*?)</td>'''
        r'''.*?'''
        r'''Museum no.</td><td>(.*?)</td>'''
        r'''.*?'''
        r'''Accession no.</td><td>(.*?)</td>'''
        r'''.*?'''
        r'''Provenience</td><td>(.*?)</td>'''
        r'''.*?'''
        r'''Excavation no.</td><td>(.*?)</td>'''
        r'''.*?'''
        r'''Period</td><td>(.*?)</td>'''
        r'''.*?'''
        r'''Dates referenced</td><td>(.*?)</td>'''
        r'''.*?'''
        r'''Object type</td><td>(.*?)</td>'''
        r'''.*?'''
        r'''Remarks</td><td>(.*?)</td>'''
        r'''.*?'''
        r'''Material</td><td>(.*?)</td>'''
        r'''.*?'''
        r'''Language</td><td>(.*?)</td>'''
        r'''.*?'''
        r'''Genre</td><td>(.*?)</td>'''
        r'''.*?'''
        r'''Sub-genre</td><td>(.*?)</td>'''
        r'''.*?'''
        r'''CDLI comments</td><td>(.*?)</td>'''
        r'''.*?'''
        r'''Catalogue source</td><td>(.*?)</td>'''
        r'''.*?'''
        r'''ATF source</td><td>(.*?)</td>'''
        r'''.*?'''
        r'''Translation</td><td>(.*?)</td>'''
        r'''.*?'''
        r'''UCLA Library ARK</td><td>(.*?)</td>'''
        r'''.*?'''
        r'''Composite no.</td><td>(.*?)</td>'''
        r'''.*?'''
        r'''Seal no.</td><td>(.*?)</td>'''
        r'''.*?'''
        r'''CDLI no.</td><td>(.*?)</td>''')

    records = []
    matches = re.finditer(regexp, text)
    for match in matches:
        record = {
            "archival_view":
                "https://cdli.ucla.edu/search/archival_view.php?ObjectID=" +
                match.group(1),
            "primary_publication": match.group(2),
            "author": match.group(3),
            "publication_date": match.group(4),
            "secondary_publication": match.group(5),
            "collection": match.group(6),
            "museum_number": match.group(7),
            "accession_number": match.group(8),
            "provenience": match.group(9),
            "excavation_number": match.group(10),
            "period": match.group(11),
            "dates_referenced": match.group(12),
            "object_type": match.group(13),
            "remarks": match.group(14),
            "material": match.group(15),
            "language": match.group(16),
            "genre": match.group(17),
            "sub_genre": match.group(18),
            "cdli_comments": match.group(19),
            "catalogue_source": match.group(20),
            "atf_source": match.group(21),
            "translation": match.group(22),
            "ucla_library_ark": match.group(23),
            "composite_number": match.group(24),
            # Strip HTML tags from seal number
            "seal_number": re.sub(r'<.+?>', '', match.group(25)).strip(),
            "cdli_number": match.group(26),
        }

        assert "<" not in record["seal_number"], \
            "Possible HTML tag < in seal_number {}".format(repr(record))

        assert ">" not in record["seal_number"], \
            "Possible HTML tag > in seal_number {}".format(repr(record))

        records.append(record)

    return records


def test_extract_cdli_meta():
    url = ("https://cdli.ucla.edu/search/search_results.php"
           "?SearchMode=Text&PrimaryPublication=&MuseumNumber="
           "&Provenience=&Period=&TextSearch=&ObjectID=300108"
           "&requestFrom=Submit+Query")
    result = requests.get(url)
    records = extract_cdli_meta(result.text)
    assert len(records) > 0


def extract_cdli_meta_from_archival_view(text):
    fields = {
        "primary_publication":
        r'''Primary publication:</td>.*?<td>(.*?)</td>''',
        "author":
        r'''Author:</td>.*?<td class="value">(.*?)</td>''',
        "publication_date":
        r'''Publication date:</td>.*?<td class="value">(.*?)</td>''',
        "secondary_publications":
        r'''Secondary publication\(s\):</td>.*?<td class="value">(.*?)</td>''',
        "citation":
        r'''Citation:</td>.*?<td class="value">(.*?)</td>''',
        "author_remarks":
        r'''Author remarks:</td>.*?<td class="value">(.*?)</td>''',
        "published_collation":
        r'''Published collation:</td>.*?<td class="value">(.*?)</td>''',
        "cdli_number":
        r'''CDLI no.:</td>.*?<td class="value"><a HREF=".*?" />(.*?)</a></td>''',
        "ucla_library_ark":
        r'''UCLA Library ARK</td>.*?<td class="ark">(.*?)</FONT>.*?</td>''',
        "cdli_comments":
        r'''CDLI comments:</td>.*?<td class="value">(.*?)</td>''',
        "catalogue":
        r'''Catalogue:</td>.*?class="value">(.*?)</td>''',
        "transliteration_remarks":
        r'''Transliteration:</td>.*?class="value">(.*?)</td>''',
        "translation_remarks":
        r'''Translation:</td>.*?class="value">(.*?)</td>''',
        "owner":
        r'''Owner:</td>.*?class="value">(.*?)</td>''',
        "museum_number":
        r'''Museum no.:</td>.*?class="value">(.*?)</td>''',
        "accession_number":
        r'''Accession no.:</td>.*?class="value">(.*?)</td>''',
        "acquisition_history":
        r'''Acquisition history:</td>.*?class="value">(.*?)</td>''',
        "genre":
        r'''Genre:</td>.*?class="value">(.*?)</td>''',
        "sub_genre":
        r'''Sub-genre:</td>.*?class="value">(.*?)</td>''',
        "sub_genre_remarks":
        r'''Sub-genre remarks:</td>.*?class="value">(.*?)</td>''',
        "composite_number":
        r'''Composite no.:</td>.*?class="value"><a HREF=".*?" />(.*?)</a>''',
        "language":
        r'''Language:</td>.*?class="value">(.*?)</td>''',
        "material":
        r'''Material:</td>.*?class="value">(.*?)</td>''',
        "object_remarks":
        r'''Object remarks:</td>.*?class="value">(.*?)</td>''',
        "measurements_in_mm":
        r'''Measurements \(mm\):</td>.*?class="value">(.*?)</td>''',
        "object_preservation":
        r'''Object preservation:</td>.*?class="value">(.*?)</td>''',
        "surface_preservation":
        r'''Surface preservation:</td>.*?class="value">(.*?)</td>''',
        "condition_description":
        r'''Condition description:</td>.*?class="value">(.*?)</td>''',
        "join_information":
        r'''Join information:</td>.*?class="value">(.*?)</td>''',
        "seal_number":
        r'''Seal no.:</td>.*?COLOR="#0099FF">(.*?)</FONT>''',
        "seal_information":
        r'''Seal information:</td>.*?class="value">(.*?)</td>''',
        "provenience":
        r'''Provenience:</td>.*?class="value">(.*?)</td>''',
        "excavation_number":
        r'''Excavation no.:</td>.*?class="value">(.*?)</td>''',
        "findspot_square":
        r'''Findspot square:</td>.*?class="value">(.*?)</td>''',
        "elevation":
        r'''Elevation:</td>.*?class="value">(.*?)</td>''',
        "stratigraphic_level":
        r'''Stratigraphic level:</td>.*?class="value">(.*?)</td>''',
        "findspot_remarks":
        r'''Findspot remarks:</td>.*?class="value">(.*?)</td>''',
        "period":
        r'''Period:</td>.*?class="value">(.*?)</td>''',
        "period_remarks":
        r'''Period remarks:</td>.*?class="value">(.*?)</td>''',
        "date_of_origin":
        r'''Date of Origin:</td>.*?class="value">(.*?)</td>''',
        "dates_referenced":
        r'''Dates referenced:</td>.*?class="value">(.*?)</td>''',
        "alternative_years":
        r'''Alternative years:</td>.*?class="value">(.*?)</td>''',
        "accounting_period":
        r'''Accounting period:</td>.*?class="value">(.*?)</td>''',
        "textual_content":
        r'''<!-- TRANSLITERATION PART -->'''
        r'''(.*?)'''
        r'''<!-- REVISION HISTORY PART -->''',
    }

    record = {}
    for key, value in fields.items():
        match = re.search(value, text, re.DOTALL)
        assert match is not None, "Could not match field {}.".format(key)
        record[key] = match.group(1).strip()

    record["textual_content"] = \
        re.sub(r"<br>", "", record["textual_content"])
    record["textual_content"] = \
        re.sub(r"<b>(.*?)</b>", r"@\1", record["textual_content"])
    record["textual_content"] = \
        re.sub(r"<i>(.*?)</i>", r"@\1", record["textual_content"])

    assert "<" not in record["textual_content"], \
        "Possible HTML tag < in {}.".format(record["textual_content"])
    assert ">" not in record["textual_content"], \
        "Possible HTML tag > in {}.".format(record["textual_content"])

    lineart_url_regex = r'''<img src="/dl/tn_lineart/(.*?)\.jpg">'''
    match = re.search(lineart_url_regex, text, re.DOTALL)
    if match is not None:
        lineart_url = ('''https://cdli.ucla.edu/dl/tn_lineart/{}.jpg'''
                       .format(match.group(1)))
        record["lineart_url"] = lineart_url

    photo_url_regex = r'''<img src="/dl/tn_photo/(.*?)\.jpg">'''
    match = re.search(photo_url_regex, text, re.DOTALL)
    if match is not None:
        photo_url = ('''https://cdli.ucla.edu/dl/tn_photo/{}.jpg'''
                     .format(match.group(1)))
        record["photo_url"] = photo_url

    return record


def test_extract_cdli_meta_from_archival_view():
    url = ("https://cdli.ucla.edu/search/archival_view.php?ObjectID=P101852")
    result = requests.get(url)
    record = extract_cdli_meta_from_archival_view(result.text)
    assert "primary_publication" in record


def resolve_cdli_number(number):
    assert number is not None
    query = ("https://cdli.ucla.edu/search/search_results.php"
             "?SearchMode=Text&PrimaryPublication=&MuseumNumber="
             "&Provenience=&Period=&TextSearch=&ObjectID={}"
             "&requestFrom=Submit+Query".format(number))
    result = requests.get(query)
    records = extract_cdli_meta(result.text)
    if len(records) == 0:
        return None
    elif len(records) == 1:
        return "https://cdli.ucla.edu/" + records[0]["cdli_number"]
    else:
        return resolve_cdli_number("0"+number)


def test_resolve_cdli_number():
    url = resolve_cdli_number("300108")
    assert url == "https://cdli.ucla.edu/P300108"


def extract_from_gigamesh_info_file(path):
    regex = r'HS_(\w+)_GMOCF_r(\d+\.\d+)_n(\d+)_v(\d+)\.info.txt$'

    match = re.search(regex, path)
    if match is None:
        return None

    record = {
        "filename": match.group(0),
        "hs_number": "HS " + match.group(1),
        "radius_in_mm": match.group(2),
        "count_of_radii": 2**int(match.group(3)),
        "rastersize": int(match.group(4)),
    }

    with open(path, 'r') as f:
        text = f.read()

    fields = {
        "gigamesh_version": r"^GigaMesh Version (\d+)$",
        "threads_used": r"^Threads:\s+(\d+)$",
        "input_filename": r"^File IN:\s+(.*)$",
        "output_filename": r"^File OUT/Prefix:\s+(.*)$",
        "radius_in_mm": r"^Radius:\s+(\d+\.\d+) mm",
        "count_of_radii": r"^Radii:\s+\d+\^\d+ = (\d+)$",
        "rastersize": r"^Rastersize:\s+(\d+\^\d+)$",
        "radii": r"^Radii:\s+((\d+\.\d+ ?)+)$",
        "model_id": r"^Model ID:\s+(.*)$",
        "material": r"^Material:\s+(.*)$",
        "count_of_vertices": r"^Vertices:\s+(\d+)$",
        "count_of_faces": r"^Faces:\s+(\d+)$",
        "bounding_box_in_cm": r"^Bounding Box:\s+(.*)$",
        "area_in_cm2": r"^Area:\s+(\d+\.\d+) cm\^2$",
        "volume_dx_in_cm3": r"^Volume \(dx\):\s+(\d+\.\d+) cm\^3$",
        "volume_dy_in_cm3": r"^Volume \(dy\):\s+(\d+\.\d+) cm\^3$",
        "volume_dz_in_cm3": r"^Volume \(dz\):\s+(\d+\.\d+) cm\^3$",
        "processing_began_on": r"^Start date/time is:\s+(.*)$",
        "processing_finished_on": r"^End date/time is:\s+(.*)$",
        "processing_wall_time_in_s":
            r"^Parallel processing took (\d+) seconds\.$"
    }

    for key, value in fields.items():
        match = re.search(value, text, re.MULTILINE)
        if match is None:
            continue
        record[key] = match.group(1)

    if "radii" in record:
        record["radii"] = [float(r) for r in record["radii"].split()]

    if "bounding_box_in_cm" in record:
        match = re.match(r"(\d+\.\d\d) x (\d+\.\d\d) x (\d+\.\d\d) cm",
                         record["bounding_box_in_cm"])
        record["bounding_box_in_cm"] = (float(match.group(1)),
                                        float(match.group(2)),
                                        float(match.group(3)))

    if "input_filename" in record:
        _, tail = os.path.split(record["input_filename"])
        record["input_filename"] = tail

    if "output_filename" in record:
        _, tail = os.path.split(record["output_filename"])
        record["output_filename"] = tail

    if len(record) == 5:
        print("Warning: No properties discovered in {}.".format(path))

    return record


def test_extract_from_gigamesh_info_file():
    path = "/export/data/vNGG/tmp/Hilprecht_Sammlung/" \
           "04_MSII_filtered/HS_00xx_MSII/" \
           "HS_0789_GMOCF_r1.50_n4_v512.info.txt"

    record = extract_from_gigamesh_info_file(path)

    assert record["radii"][-1] == 0.06
    assert record["area_in_mm_2"] == 296140
    assert record["volume_dx_in_mm_3"] == 220000000
    assert "/" not in record["input_filename"]
    assert "/" not in record["output_filename"]


def extract_from_gigamesh_clean_file(path):
    regex = r'HS_(\w+)_GMCF\.info-clean.txt$'

    match = re.search(regex, path)
    if match is None:
        return None

    record = {
        "filename": match.group(0),
        "hs_number": "HS " + match.group(1),
    }

    with open(path, 'r') as f:
        text = f.read()

    fields = {
        "gigamesh_clean_version": r"^\[GigaMesh\] CLEAN v\.(\d+)$",
        "input_filename": r"^File Input:\s+(.*)$",
        "output_filename": r"^File Output:\s+(.*)$",
        "model_id": r"^Model ID:\s+(.*)$",
        "material": r"^Model Material:\s+(.*)$",
        "count_of_vertices_in_out": r"^Vertex count \(in, out\):\s+(\d+, \d+)$",
        "count_of_faces_in_out": r"^Face count \(in, out\):\s+(\d+, \d+)$",
        "count_of_vertices_diff": r"^Vertex count \(diff\):\s+(-?\d+)$",
        "count_of_faces_diff": r"^Face count \(diff\):\s+(-?\d+)$",
        "count_of_vertices_at_border_out": r"^Vertices at border \(out\):\s+(\d+)$",
        "count_of_synthetic_vertices_out": r"^Vertices synthetic \(out\):\s+(\d+)$",
        "count_of_synthetic_faces_out": r"^Faces synthetic \(out\):\s+(\d+)$",
        "processing_began_on": r"^Start time:\s+(.*)$",
        "processing_finished_on": r"^Finish time:\s+(.*)$",
        "processing_wall_time_in_s": r"^Timespan \(sec\):\s+(\d+.\d+)$"
    }

    for key, value in fields.items():
        match = re.search(value, text, re.MULTILINE)
        if match is None:
            continue
        record[key] = match.group(1)

    if "count_of_vertices_diff" in record:
        record["count_of_vertices_diff"] = \
            int(record["count_of_vertices_diff"])

    if "count_of_faces_diff" in record:
        record["count_of_faces_diff"] = \
            int(record["count_of_faces_diff"])

    if "count_of_vertices_at_border_out" in record:
        record["count_of_vertices_at_border_out"] = \
            int(record["count_of_vertices_at_border_out"])

    if "count_of_synthetic_vertices_out" in record:
        record["count_of_synthetic_vertices_out"] = \
            int(record["count_of_synthetic_vertices_out"])

    if "count_of_synthetic_faces_out" in record:
        record["count_of_synthetic_faces_out"] = \
            int(record["count_of_synthetic_faces_out"])

    if "count_of_vertices_in_out" in record:
        tup = record["count_of_vertices_in_out"].split(",")
        record["count_of_vertices_in"] = int(tup[0])
        record["count_of_vertices_out"] = int(tup[1])
        record.pop("count_of_vertices_in_out")

    if "count_of_faces_in_out" in record:
        tup = record["count_of_faces_in_out"].split(",")
        record["count_of_faces_in"] = int(tup[0])
        record["count_of_faces_out"] = int(tup[1])
        record.pop("count_of_faces_in_out")

    if "input_filename" in record:
        _, tail = os.path.split(record["input_filename"])
        record["input_filename"] = tail

    if "output_filename" in record:
        _, tail = os.path.split(record["output_filename"])
        record["output_filename"] = tail

    if len(record) == 2:
        print("Warning: No properties discovered in {}.".format(path))

    return record


def test_extract_from_gigamesh_clean_file():
    path = "/export/data/vNGG/tmp/Hilprecht_Sammlung/" \
           "02_PLYs_cleaned/HS_0044_GMCF.info-clean.txt"

    record = extract_from_gigamesh_clean_file(path)

    assert record["count_of_vertices_at_border_out"] == 0
    assert "/" not in record["input_filename"]
    assert "/" not in record["output_filename"]


def extract_from_mat_filename(name):
    regex = r'HS_(\w+)_GMOCF_r(\d+\.\d+)_n(\d+)_v(\d+)' \
            r'\.(.*?)\.mat\.bz2$'

    match = re.search(regex, name)
    if match is None:
        return None

    record = {
        "filename": name,
        "hs_number": "HS " + match.group(1),
        "radius_in_mm": match.group(2),
        "count_of_radii": 2**int(match.group(3)),
        "rastersize": int(match.group(4)),
    }

    return record


def test_extract_from_mat_filename():
    names = os.listdir("/export/data/vNGG/tmp/Hilprecht_Sammlung/"
                       "04_MSII_filtered/HS_00xx_MSII/")

    record = extract_from_mat_filename(names[0])

    assert "filename" in record


def extract_from_ply_filename(name):
    regex = r'HS_(\w+)_GMOCF_r(\d+\.\d+)_n(\d+)_v(\d+)\.ply$'

    match = re.search(regex, name)
    if match is None:
        return None

    record = {
        "filename": name,
        "hs_number": "HS " + match.group(1),
        "radius_in_mm": match.group(2),
        "count_of_radii": 2**int(match.group(3)),
        "rastersize": int(match.group(4)),
    }

    return record


def test_extract_from_ply_filename():
    names = os.listdir("/export/data/vNGG/tmp/Hilprecht_Sammlung/"
                       "04_MSII_filtered/HS_00xx_MSII_PLY/")

    record = extract_from_ply_filename(names[0])

    assert "filename" in record


def extract_from_gigamesh_info_program(path):
    command = os.path.expanduser("~/.local/bin/gigamesh-info")
    output = subprocess.check_output([command, path])
    output = output.decode()
    regex = r"(.*;)"
    match = re.search(regex, output)
    values = match.group(0).split(";")
    properties = [
        "filename",
        "model_id",
        "model_material",
        "vertices_total",
        "vertices_nan",
        "vertices_solo",
        "vertices_polyline",
        "vertices_border",
        "vertices_nonmanifold",
        "vertices_on_inverted_edge",
        "vertices_part_of_zero_face",
        "vertices_synthetic",
        "vertices_manual",
        "vertices_circle_center",
        "vertices_selected",
        "vertices_funcval_finite",
        "vertices_funcval_local_min",
        "vertices_funcval_local_max",
        "faces_total",
        "faces_solo",
        "faces_border",
        "faces_border_dangling",
        "faces_manifold",
        "faces_nonmanifold",
        "faces_sticky",
        "faces_zeroarea",
        "faces_inverted",
        "faces_with_synth_vertices",
        "boundingbox_min_x",
        "boundingbox_min_y",
        "boundingbox_min_z",
        "boundingbox_max_x",
        "boundingbox_max_y",
        "boundingbox_max_z",
        "boundingbox_width",
        "boundingbox_height",
        "boundingbox_thick",
        "total_area",
        "total_volume_dx",
        "total_volume_dy",
        "total_volume_dz",
    ]
    record = dict(zip(properties, values))
    return record


def test_extract_from_gigamesh_info_program():
    ply_filename = "/export/data/vNGG/tmp/Hilprecht_Sammlung/" \
                   "04_MSII_filtered/HS_00xx_MSII_PLY/" \
                   "HS_0044_GMOCF_r1.50_n4_v512.ply"

    record = extract_from_gigamesh_info_program(ply_filename)

    assert record["filename"] == "HS_0044_GMOCF_r1.50_n4_v512"


def fix_short_hs_number(hs_number):
    four_digits = r'HS (\d\d\d\d.*)'
    three_digits = r'HS (\d\d\d\D?.*)'
    match_four = re.search(four_digits, hs_number)
    match_three = re.search(three_digits, hs_number)

    assert match_four != match_three

    if match_four:
        return hs_number
    else:
        return "HS 0" + match_three.group(1)


def test_fix_short_hs_number():
    assert fix_short_hs_number("HS 0786abc_1") == "HS 0786abc_1"
    assert fix_short_hs_number("HS 786") == "HS 0786"


def scrape_files(database, root):
    for dirpath, dirnames, filenames in os.walk(root):
        for filename in filenames:
            path = os.path.join(dirpath, filename)

            if path.endswith(".ply"):
                source = "from_ply_filename"
                properties = extract_from_ply_filename(path)
            elif path.endswith(".normal.mat.bz2"):
                source = "from_normal_mat_bz2"
                properties = extract_from_mat_filename(path)
            elif path.endswith(".surface.mat.bz2"):
                source = "from_surface_mat_bz2"
                properties = extract_from_mat_filename(path)
            elif path.endswith(".volume.mat.bz2"):
                source = "from_volume_mat_bz2"
                properties = extract_from_mat_filename(path)
            elif path.endswith(".info.txt"):
                source = "from_gigamesh_info_file"
                properties = extract_from_gigamesh_info_file(path)
            elif path.endswith(".info-clean.txt"):
                source = "from_gigamesh_info_clean_file"
                properties = extract_from_gigamesh_clean_file(path)
            else:
                source = None
                properties = None

            if properties is None:
                continue

            #properties["hs_number"] = \
            #    fix_short_hs_number(properties["hs_number"])

            hs_number = properties["hs_number"]

            if hs_number not in database:
                database[hs_number] = {}

            database[hs_number][source] = properties

            yield properties


def test_scrape_files():
    database = {}
    root = "/export/data/vNGG/tmp/Hilprecht_Sammlung/"
    assert next(scrape_files(database, root)) is not None


def scrape_with_gigamesh_info_program(database, root):
    for record in database.values():
        if "from_ply_filename" not in record:
            continue

        source = "from_gigamesh_info_program"
        filename = record['from_ply_filename']['filename']
        path = os.path.join(root, filename)
        properties = extract_from_gigamesh_info_program(path)
        assert properties is not None
        record[source] = properties

        yield properties


def test_scrape_with_gigamesh_info_program():
    root = "/export/data/vNGG/tmp/Hilprecht_Sammlung/"
    database = {}
    assert next(scrape_files(database, root)) is not None
    assert next(scrape_with_gigamesh_info_program(database, root)) is not None


def scrape_hilprecht3d(database):
    known = {"https://hilprecht.mpiwg-berlin.mpg.de/search3d"}
    visited = set()

    while len(visited) < len(known):
        url = (known - visited).pop()
        result = requests.get(url)
        known.update(extract_jena_pages(result.text))

        new_records = extract_jena_meta(result.text)
        for properties in new_records:

            hs_number = properties["hs_number"]
            if hs_number not in database:
                if hs_number.startswith("HS 0"):
                    hs_number = "HS " + hs_number[4:]

            if hs_number not in database:
                continue

            database[hs_number]["from_hilprecht3d"] = properties

            yield properties

        visited.add(url)


def test_scrape_hilprecht3d():
    records = {}
    assert next(scrape_hilprecht3d(records)) is not None


def resolve_hs_number_in_cdli(number):
    assert number is not None
    long_number = fix_short_hs_number(number)
    query = ("https://cdli.ucla.edu/search/search_results.php"
             "?SearchMode=Text&PrimaryPublication=&MuseumNumber={}"
             "&Provenience=&Period=&TextSearch=&ObjectID="
             "&requestFrom=Submit+Query".format(number))
    result = requests.get(query)
    records = extract_cdli_meta(result.text)

    if len(records) == 1:
        if records[0]["museum_number"] == long_number:
            return records[0]["cdli_number"]

    if number != long_number:
        return resolve_hs_number_in_cdli(long_number)

    return None


def test_resolve_hs_number_in_cdli():
    cdli_number = resolve_hs_number_in_cdli("HS 294")
    assert cdli_number == "P358085"


def scrape_cdli_search_views(database):
    for record in database.values():
        if "from_cdli_search_view" in record:
            del record["from_cdli_search_view"]

        if "from_hilprecht3d" not in record:
            continue

        cdli_number = resolve_hs_number_in_cdli(
            record["from_hilprecht3d"]["hs_number"])
        if cdli_number is None:
            continue

        cdli_url = resolve_cdli_number(cdli_number)
        if cdli_url is None:
            continue

        result = requests.get(cdli_url)
        cdli_meta = extract_cdli_meta(result.text)
        assert len(cdli_meta) == 1
        properties = cdli_meta[0]
        record["from_cdli_search_view"] = properties

        yield properties


def test_scrape_cdli_search_views():
    database = {}
    assert next(scrape_hilprecht3d(database)) is not None
    assert next(scrape_cdli_search_views(database)) is not None


def scrape_cdli_archival_views(database):
    for record in database.values():
        if "from_cdli_archival_view" in record:
            del record["from_cdli_archival_view"]

        if "from_cdli_search_view" not in record:
            continue

        properties = record["from_cdli_search_view"]
        result = requests.get(properties["archival_view"])
        properties = extract_cdli_meta_from_archival_view(result.text)
        record["from_cdli_archival_view"] = properties

        yield properties


def test_scrape_cdli_archival_views():
    database = {}
    assert next(scrape_hilprecht3d(database)) is not None
    assert next(scrape_cdli_search_views(database)) is not None
    assert next(scrape_cdli_archival_views(database)) is not None


def scrape_cdli_bytes(database):
    for record in database.values():
        if "from_cdli_archival_view" not in record:
            continue

        properties = record["from_cdli_archival_view"]

        if "lineart_url" in properties:
            lineart_url = properties["lineart_url"]
            lineart_bytes = requests.get(lineart_url).content
            lineart_sha224 = hashlib.sha224(lineart_bytes).hexdigest()

            with open("assets/{}.jpg".format(lineart_sha224), "wb") as f:
                f.write(lineart_bytes)

            record["lineart_sha224"] = lineart_sha224

            yield lineart_sha224

        if "photo_url" in properties:
            photo_url = properties["photo_url"]
            photo_bytes = requests.get(photo_url).content
            photo_sha224 = hashlib.sha224(photo_bytes).hexdigest()

            with open("assets/{}.jpg".format(photo_sha224), "wb") as f:
                f.write(photo_bytes)

            record["photo_sha224"] = photo_sha224

            yield photo_sha224


def test_scrape_cdli_bytes():
    database = {}
    assert next(scrape_hilprecht3d(database)) is not None
    assert next(scrape_cdli_search_views(database)) is not None
    assert next(scrape_cdli_archival_views(database)) is not None
    assert next(scrape_cdli_bytes(database)) is not None


def count_database_stats(database):
    stats = {
        "from_ply_filename": 0,
        "from_normal_mat_bz2": 0,
        "from_surface_mat_bz2": 0,
        "from_volume_mat_bz2": 0,
        "from_gigamesh_info_file": 0,
        "from_gigamesh_info_clean_file": 0,
        "from_gigamesh_info_program": 0,
        "from_hilprecht3d": 0,
        "from_cdli_search_view": 0,
        "from_cdli_archival_view": 0,
        "have_cdli_photo": 0,
        "have_cdli_lineart": 0,
        "have_cdli_transcription": 0,
    }

    for tablet in database.values():
        for prop in stats:
            if prop in tablet:
                stats[prop] += 1

        if "from_cdli_archival_view" in tablet:
            if "photo_url" in tablet["from_cdli_archival_view"]:
                stats["have_cdli_photo"] += 1

            if "lineart_url" in tablet["from_cdli_archival_view"]:
                stats["have_cdli_lineart"] += 1

            textual_content = \
                tablet["from_cdli_archival_view"]["textual_content"]

            if len(textual_content) > 30:
                stats["have_cdli_transcription"] += 1

    return stats


def test_count_database_stats():
    database = { "HS 2568": {
                    "from_gigamesh_info_clean_file": None,
                    "from_ply_filename": None} }

    stats = count_database_stats(database)

    assert stats["from_gigamesh_info_clean_file"] == 1
    assert stats["from_ply_filename"] == 1


def main():
    parser = argparse.ArgumentParser("Scrape Hilprecht3D and CDLI collections.")
    parser.add_argument("-x", "--drop-database", action="store_true")
    parser.add_argument("-a", "--scrape-files", action="store_true")
    parser.add_argument("-b", "--scrape-hilprecht3d", action="store_true")
    parser.add_argument("-c", "--scrape-cdli-search", action="store_true")
    parser.add_argument("-d", "--scrape-cdli-archive", action="store_true")
    parser.add_argument("-e", "--scrape-gigamesh-info", action="store_true")
    parser.add_argument("-y", "--scrape-cdli-line-and-photo", action="store_true")
    parser.add_argument("-f", "--split-database", action="store_true")
    parser.add_argument("path")
    args = parser.parse_args()

    if args.drop_database:
        print("Using an empty database.")
        database = {}
    else:
        print("Loading database from results/database.json ...")
        with open("results/database.json", "r") as f:
            database = json.load(f)

    if args.scrape_files:
        print("Scraping files ...")
        for status in scrape_files(database, args.path):
            pass

        with open("results/database.json", "w") as f:
            json.dump(database, f, indent=4)

    if args.scrape_hilprecht3d:
        print("Scraping Hilprecht3D collection ...")
        for status in scrape_hilprecht3d(database):
            print(status)

        with open("results/database.json", "w") as f:
            json.dump(database, f, indent=4)

    if args.scrape_cdli_search:
        print("Scraping CDLI from search views ...")
        for status in scrape_cdli_search_views(database):
            print(status)

        with open("results/database.json", "w") as f:
            json.dump(database, f, indent=4)

    if args.scrape_cdli_archive:
        print("Scraping CDLI from archival views ...")
        for status in scrape_cdli_archival_views(database):
            print(status)

        with open("results/database.json", "w") as f:
            json.dump(database, f, indent=4)

    if args.scrape_gigamesh_info:
        print("Running gigamesh-info for known PLY files ...")
        for status in scrape_with_gigamesh_info_program(database, args.path):
            print(status)

        with open("results/database.json", "w") as f:
            json.dump(database, f, indent=4)

    if args.scrape_cdli_line_and_photo:
        print("Scraping lineart and photos from CDLI ..")
        for status in scrape_cdli_bytes(database):
            print(status)

        with open("results/database.json", "w") as f:
            json.dump(database, f, indent=4)

    if args.split_database:
        print("Splitting database into one file per tablet ...")
        for key, value in database.items():
            filename = os.path.join(
                "results", "single", key.replace(" ", "_") + ".json")
            with open(filename, "w") as f:
                json.dump(value, f, indent=4)

    with open("results/stats.json", "w") as f:
        json.dump(count_database_stats(database), f, indent=4)


if __name__ == "__main__":
    main()
