# coding=utf-8

import json
import os.path

with open("database.json", "r") as f:
    database = json.load(f)

for hs_number, record in database.items():
    if "from_cdli_search_view" not in record:
        continue

    cdli_number = record["from_cdli_search_view"]["cdli_number"]
    cdli_url = "https://cdli.ucla.edu/" + cdli_number
    filename = hs_number.replace(" ", "_") + ".txt"
    path = os.path.join("hs_number_to_cdli_number", filename)

    with open(path, "w") as f:
        print(cdli_url, file=f)
