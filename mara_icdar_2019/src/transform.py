# coding=utf-8

import sys
import numpy
import numpy.linalg


def transformation(a, b):
    assert a.shape == (3, 3)
    assert b.shape == (3, 3)

    centroid_a = numpy.mean(a, axis=0)
    centroid_b = numpy.mean(b, axis=0)
    centered_a = a - centroid_a
    centered_b = b - centroid_b

    covariances = numpy.dot(centered_a.T, centered_b)
    u, s, vh = numpy.linalg.svd(covariances)
    rotate_to_b = numpy.dot(vh.T, u.T)

    if numpy.linalg.det(rotate_to_b) < 0:
        vh[2, :] *= -1
        rotate_to_b = numpy.dot(vh.T, u.T)

    translate_to_b = centroid_b.T - numpy.dot(rotate_to_b, centroid_a.T)

    mat = numpy.identity(4)
    mat[0:3, 0:3] = rotate_to_b
    mat[0:3, 3] = translate_to_b

    return mat


def test_transformation():
    a = numpy.array([
        (1, 0, 0),
        (0, 1, 0),
        (0, 0, 1)
    ])
    m = transformation(a, a)
    e = numpy.array([
        [1, 0, 0, 0],
        [0, 1, 0, 0],
        [0, 0, 1, 0],
        [0, 0, 0, 1]
    ])
    assert numpy.allclose(m, e)

    a = numpy.array([
        (2, -1, -1),
        (-1, 2, -1),
        (-1, -1, 2)
    ])
    b = numpy.array([
        (1, 2, -1),
        (-2, -1, -1),
        (1, -1, 2)
    ])
    m = transformation(a, b)
    e = numpy.array([
        [0, -1, 0, 0],
        [1, 0, 0, 0],
        [0, 0, 1, 0],
        [0, 0, 0, 1]
    ])
    assert numpy.allclose(m, e)


def main():
    assert len(sys.argv) == 2, "Expected CSV as first argument."
    fname = sys.argv[1]
    points = numpy.loadtxt(fname, delimiter=",")
    assert points.shape == (6, 3)
    mat = transformation(points[0:3], points[3:6])
    for i in range(4):
        print("{:+8e}, {:+8e}, {:+8e}, {:+8e}"
              .format(mat[i, 0], mat[i, 1], mat[i, 2], mat[i, 3]))


if __name__ == "__main__":
    main()
