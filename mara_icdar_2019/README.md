Example invocation:

    python3 src/scrape.py -xabcde /export/data/vNGG/tmp/Hilprecht_Sammlung/04_MSII_filtered/


## References

Published paper: [doi:10.1109/ICDAR.2019.00032](https://doi.org/10.1109/ICDAR.2019.00032)

HeiCu3Da Hilprecht – Heidelberg Cuneiform 3D Database – Hilprecht Collection: [doi:10.11588/heidicon.hilprecht](https://doi.org/10.11588/heidicon.hilprecht)

HeiCuBeDa Hilprecht – Heidelberg Cuneiform Benchmark Dataset for the Hilprecht Collection: [doi:10.11588/data/IE8CCN](https://doi.org/10.11588/data/IE8CCN)
