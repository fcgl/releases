Collection of programming code released to the public from the
Forensic Computational Geometry Lab containing
student projects, peer-reviewed papers, and published blog articles.

