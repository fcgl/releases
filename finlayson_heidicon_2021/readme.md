Documentation of work
=====================

Contributors
------------

Hubert Mara, Diamantis Panagiotopoulos: Coordination
Emmanouil Spanoudakis, Jochanan Abitbol: 3D scanning and documentation
Sarah Finalyson: Archaeological documentation
Steffen Bauer: Parameters and rendering of PNGs
Bartosz Bogacz: General editing and technical documentation

Computation
-----------

If you do not want to copy all files into the same directory you can
use `find` and the Gigamesh CLI tools will work just as well.

    find -name ".ply" -exec gigamesh-featurevectors {} \;

For info you need provide a commandline of all files to be processed.
A well-defined hierarchy helps here. ´-a´ generates all sidecar files.
`-o` generates a CSV for all files in the commandline describing technical
meta-data.

    gigamesh-info -a -o technical.csv Gold_Signet/*/*/*.ply Haghia_Triadha/*/*/*.ply


Parameters
----------

Choosing tiled rendering YES will ensure that images are in frame
and excess borders are cropped. Setting a higher DPI will not zoom the
objects in and crop them. Choosing tiled rendering NO will screenshot the view
of Gigamesh as currently on screen. Useful for computational analysis since
each image will have the exact same width and height. Make sure to maximize
Gigamesh beforehand. Choose a DPI so that each object in the set to be
screenshotted fits into the view.

PDFs
----

For Archaeological documentation.

Enable light, disable vertex color, switch to grayscale,
File -> Screenshot Directory, Choose PDF, DPI: 800, Front view,
Tiled Rendering: Yes, Choose directory with PLYs, Suffix: _light

PNGs
----

For preview and computational analysis.

Enable light, disable vertex color, switch to grayscale,
File -> Screenshot Directory, DPI: 600, Render PNGs, Only front,
Tiled Rendering: No, Choose directory with PLYs, Suffix: _light

PNGs
----

For computational analysis. Ensure that objects have MSII computed.

Disable light, enable feature visualization, switch to grayscale,
_invert colors_, set Minimum to -0.1, set Maximum to 0.25, set Fixed Range
File -> Screenshot Directory, Render PNGs, Only front, DPI: 600,
Tiled Rendering: No, Choose directory with PLYs, Suffix: _msii

PNGs
----

For preview. Ensure that objects have MSII computed.

Enable light, enable feature visualization, switch to grayscale,
_invert colors_, set Minimum to -0.1, set Maximum to 0.25, set Fixed Range
File -> Screenshot Directory, Render PNGs, Only front, DPI: 600,
Tiled Rendering: No, Choose directory with PLYs, Suffix: _msii_light

Arachne
-------

Go to

<https://arachne.uni-koeln.de/browser/index.php?view[layout]=siegel>

and navigate to the desired seal manually. Then, use a link in the right
bar titled "Verlauf". Only these URLs are stable. It should look like this:

<https://arachne.uni-koeln.de/browser/index.php?view[layout]=siegel_item&objektsiegel[jump_to_id]=160113>

References for CMS volumes
--------------------------

<https://www.uni-heidelberg.de/fakultaeten/philosophie/zaw/cms/cmsseries/theseries.html>

Repeated seals on different sealing forms
=========================================

Haghia Triadha
--------------

24 impressions in total

The focus here is on the use of one seal across several sealings,
including sealings of different shapes

How similar/dissimilar are the impressions within a sealing form
and between sealing forms?

Does this suggest the same person made each impression or not?

Is it possible that one person used more than one seal?

The seals chosen are from amongst the sealing leaders at Haghia Triadha
- how do reconstructions of sealing practises need to change if more than
one person can use the same seal?

NB. More impressions are available of seals 1, 2 and 4, if time permits

### Seal 1, 6 impressions

CMS II. 6 no. 70 (HMs 455/3, 455/5, 455/10, 455/15, 456/5, AAPM 10.615)

### Seal 2, 6 impressions

CMS II.6 no. 101 (HMs 452/5, 454/11, 452/25, 17967, 452/30, 452/32)

### Seal 3, 6 impressions

CMS II.6 no. 20 (HMs 434/1, 434/2, 434/4, 434/5, 434/7, 434/28)

### Seal 4, 6 impressions

CMS II.6 no. 11 (HM Disk 78, HMs 441/18, 441/20, 441/5, 441/6, 443/13)

Gold signet rings
-----------------

22 impressions in total

(known as ‘Replica rings’)

Four rings are used on sealings found at multiple sites

How similar/dissimilar are the impressions from each ring on its sealings?

Does the sealing form cause any variation?

Does this suggest the same person made all the impressions of a
single ring or not?

### Seal 1, 7 impressions

Haghia Triadha: CMS II.6 no. 43, 3 x impressions (HMs 497-499)

Gournia: CMS II.6 no. 161, 1 x impression (HMs 101)

Sklavokambos: CMS II.6 no. 259, 2 x impressions (HMs 628-629)

Zakros Palace: CMS II.7 no. 39, 1 x impression (HMs 1051)

### Seal 2, 3 impressions

Haghia Triadha: CMS II.6 no. 44, 1 x impression (RMP 71974)

Gournia: CMS II.6 no. 162, 1 x impression (HMs 102)

Sklavokambos: CMS II.6 no. 255, 1 x impression (HMs 612)

### Seal 3, 5 impressions

Haghia Triadha: CMS II.6 no. 19, 2 x impressions (HMs 591, HMs 516)

Sklavokambos: CMS II.6 no. 260, 3 x impressions (HMs 632-635)

### Seal 4, 7 impressions

Haghia Triadha: CMS II.6 no. 15, 5 x impressions (HMs 526/1-3, HMs 595-596)

Knossos: CMS II.8 no. 279, 2 x impressions (HMs 369, 1275)
