# coding=utf-8

import sys
import json
import subprocess

import numpy

import scipy.spatial.distance

import skimage.filters

import matplotlib.pyplot


def additive_overlay(image1, image2):
    a, b = image1.shape
    c, d = image2.shape
    e, f = min(a, c), min(b, d)
    overlay = numpy.zeros((e, f))
    overlay += image1[0:e, 0:f]
    overlay += image2[0:e, 0:f]
    return overlay


def difference_overlay(image1, image2):
    a, b = image1.shape
    c, d = image2.shape
    e, f = min(a, c), min(b, d)
    overlay = numpy.zeros((e, f))
    overlay += image1[0:e, 0:f]
    overlay -= image2[0:e, 0:f]
    overlay = numpy.abs(overlay)
    return overlay


def binary_overlay(left_nonrigid, right_image):
    threshold = skimage.filters.threshold_niblack(
        right_image, window_size=51)
    right_binary = right_image > threshold
    threshold = skimage.filters.threshold_niblack(
        left_nonrigid, window_size=51)
    left_binary = left_nonrigid > threshold

    binary = numpy.zeros((left_binary.shape[0], left_binary.shape[1]))
    binary[left_binary] = 1
    binary[right_binary] = 2
    binary[numpy.logical_and(left_binary, right_binary)] = 3

    return binary


def main():
    template = {
        "right_filename": "../data/JR635_SM2066-HE5-060_310712001_crop_AO.png",
        "left_filename": "../data/JR632_SM2066-HE5-060_310712001_crop_AO.png",
        "synthetic_deformation": 0,
        "crop": [[0, 0], [0, 0]],
        "pad": [[200, 200], [200, 200]],
        "scale": 0.5,
        "equalize_disk": 100,
        "horizontal_patches": 100,
        "vertical_patches": 100,
        "residual_threshold": 50,
        "method": "autoencoder",
        "daisy_step": 3,
        "daisy_radius": 20,
        "daisy_rings": 3,
        "bovw_words": 1024,
        "bovw_radius": 20
    }

    filenames = [
        "../data/Siegel-03-01_S075_1356_170419_GMxCFO_r1.00_n4_v256.png",
        "../data/Siegel-03-02_S075_1356_100519_GMxCFO_r1.00_n4_v256.png",
        #"../data/Siegel-03-03_S075_1356_170419_GMxCFO_r1.00_n4_v256.png",
        #"../data/Siegel-03-04_S075_1356_230419_GMxCFO_r1.00_n4_v256.png",
        #"../data/Siegel-03-05_S075_1356_230419_GMxCFO_r1.00_n4_v256.png",
        #"../data/Siegel-03-07_S075_1356_230419_GMxCFO_r1.00_n4_v256.png",
        #"../data/Siegel-03-10_S075_1356_230419_GMxCFO_r1.00_n4_v256.png",
        #"../data/Siegel-04-02_S075_1356_240419_GMxCFO_r1.00_n4_v256.png",
        #"../data/Siegel-04-03_S075_1356_290419_GMxCFO_r1.00_n4_v256.png",
        #"../data/Siegel-04-05_S075_1356_290419_GMxCFO_r1.00_n4_v256.png",
        "../data/Siegel-04-08_S075_1356_290419_GMxCFO_r1.00_n4_v256.png",
        "../data/Siegel-04-09_S075_1356_060519_GMxCFO_r1.00_n4_v256.png",
    ]

    #filenames = [
    #    "../data/cube_1.1_msii_r1_f0.png",
    #    "../data/cube_1.2_msii_r1_f0.png",
    #    "../data/cube_2.5_msii_r1_f0.png",
    #    "../data/cube_2.6_msii_r1_f0.png"
    #]

    filenames = [
        "../data/JR591_SM2066-HE5-060_310712001_AO.png",
        "../data/JR632_SM2066-HE5-060_310712001_crop_AO.png",
        "../data/JR634_SM2066-HE5-060_310712001_crop_AO.png",
        "../data/JR635_SM2066-HE5-060_310712001_crop_AO.png"
    ]

    names = ["HR591", "HR632", "HR634", "HR635"]

    comparisons = []

    for i, left_filename in enumerate(filenames):
        for j, right_filename in enumerate(filenames):
            comp = template.copy()
            comp["index"] = (i, j)
            comp["left_filename"] = left_filename
            comp["right_filename"] = right_filename
            comparisons.append(comp)

    figure, axes = matplotlib.pyplot.subplots(5, 5)
    figure.set_size_inches(12, 12)

    for i in range(5):
        axes[i, i].axis("off")

    for i, comp in enumerate(comparisons):
        print("Running comparison {} of {} ...".format(i, len(comparisons)))

        with open("comparison.json", "w") as f:
            json.dump(comp, f)

        subprocess.call(["python3", "01_preprocess.py", "comparison.json"])
        subprocess.call(["python3", "02_extract_features.py", "comparison.json"])
        subprocess.call(["python3", "03_register.py", "comparison.json"])
        subprocess.call(["python3", "04_plot.py", "comparison.json"])

        with open("../computed/01_preprocess.npz", "rb") as f:
            loaded = numpy.load(f)
            left_image = loaded["left_image"]
            right_image = loaded["right_image"]

        with open("../computed/02_extract_features.npz", "rb") as f:
            loaded = numpy.load(f)
            left_samples = loaded["left_samples"]
            right_samples = loaded["right_samples"]
            left_features = loaded["left_features"]
            right_features = loaded["right_features"]

        with open("../computed/03_register.npz", "rb") as f:
            loaded = numpy.load(f)
            nonrigid_transform = loaded["nonrigid_transform"]
            left_rigid = loaded["left_rigid"]
            left_nonrigid = loaded["left_nonrigid"]

        with open("../computed/04_plot.json", "r") as f:
            statistics = json.load(f)

        overlay = additive_overlay(left_nonrigid, right_image)

        a, b = comp["index"]
        if a == b:
            axes[a+1, 0].imshow(left_image, cmap="gray")
            axes[a+1, 0].set_frame_on(False)
            axes[a+1, 0].set_xticks([])
            axes[a+1, 0].set_yticks([])
            axes[a+1, 0].set_xlabel(names[a])

            axes[0, b+1].imshow(right_image, cmap="gray")
            axes[0, b+1].set_frame_on(False)
            axes[0, b+1].set_xticks([])
            axes[0, b+1].set_yticks([])
            axes[0, b+1].set_xlabel(names[b])
        else:
            axes[a+1, b+1].imshow(overlay, cmap="gray")
            axes[a+1, b+1].set_frame_on(False)
            axes[a+1, b+1].set_xticks([])
            axes[a+1, b+1].set_yticks([])
            axes[a+1, b+1].set_xlabel(
                "{:.2f}".format(statistics["differences"]))

    figure.tight_layout()
    figure.savefig("../plots/08_comparison.png", dpi=200)


if __name__ == "__main__":
    main()
