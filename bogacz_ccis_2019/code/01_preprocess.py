# coding=utf-8

import sys
import json

import numpy

import skimage.io
import skimage.util
import skimage.color
import skimage.filters
import skimage.transform
import skimage.morphology

import chuietal


def main():
    with open(sys.argv[1], "r") as f:
        config = json.load(f)

    synthetic_deformation = config["synthetic_deformation"]
    left_filename = config["left_filename"]
    right_filename = config["right_filename"]
    crop = config["crop"]
    pad = config["pad"]
    scale = config["scale"]
    disk = config["equalize_disk"]
    disk = skimage.morphology.disk(disk)

    left_original = skimage.io.imread(left_filename)
    left_original = skimage.color.rgb2gray(left_original)
    left_original = skimage.util.crop(left_original, crop)
    left_original = skimage.util.pad(left_original, pad, mode="maximum")
    left_original = skimage.transform.rescale(
        left_original, scale, mode='reflect')

    left_original = skimage.filters.rank.equalize(left_original, disk)
    left_original = numpy.array(left_original, dtype=float)

    assert numpy.all(0 <= left_original)
    assert numpy.all(left_original <= 255)
    assert numpy.any(left_original > 1)

    if synthetic_deformation > 0:
        points1 = numpy.zeros((25, 2))
        for i, a in enumerate(numpy.linspace(0, left_original.shape[0], num=5)):
            for j, b in enumerate(numpy.linspace(0, left_original.shape[1], num=5)):
                points1[i * 5 + j] = (a, b)
        delta = synthetic_deformation / 2 - \
                numpy.random.random((25, 2)) * synthetic_deformation
        points2 = points1 + delta
        transform = chuietal.fit_tps(points1, points2, 1)
        right_original = chuietal.apply_tps_transform(transform, left_original)

        with open("../computed/01_transform.npz", "wb") as f:
            numpy.savez_compressed(f, transform=transform)
    else:
        right_original = skimage.io.imread(right_filename)
        right_original = skimage.color.rgb2gray(right_original)
        right_original = skimage.util.crop(right_original, crop)
        right_original = skimage.util.pad(right_original, pad, mode="maximum")
        right_original = skimage.transform.rescale(
            right_original, scale, mode='reflect')

        right_original = skimage.filters.rank.equalize(right_original, disk)
        right_original = numpy.array(right_original, dtype=float)

    assert numpy.all(0 <= right_original)
    assert numpy.all(right_original <= 255)
    assert numpy.any(right_original > 1)

    with open("../computed/01_preprocess.npz", "wb") as f:
        numpy.savez_compressed(f,
            left_image=left_original,
            right_image=right_original)

if __name__ == "__main__":
    main()
