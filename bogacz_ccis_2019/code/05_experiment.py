# coding=utf-8

import sys
import json

import subprocess


def main():
    summary = []
    experiments = []

    # Manufactured motif

    filenames = [
        "../data/Siegel-03-01_S075_1356_170419_GMxCFO_r1.00_n4_v256.png",
        "../data/Siegel-03-02_S075_1356_100519_GMxCFO_r1.00_n4_v256.png",
        "../data/Siegel-03-03_S075_1356_170419_GMxCFO_r1.00_n4_v256.png",
        "../data/Siegel-03-04_S075_1356_230419_GMxCFO_r1.00_n4_v256.png",
        "../data/Siegel-03-05_S075_1356_230419_GMxCFO_r1.00_n4_v256.png",
        "../data/Siegel-03-07_S075_1356_230419_GMxCFO_r1.00_n4_v256.png",
        "../data/Siegel-03-10_S075_1356_230419_GMxCFO_r1.00_n4_v256.png",
        "../data/Siegel-04-02_S075_1356_240419_GMxCFO_r1.00_n4_v256.png",
        "../data/Siegel-04-03_S075_1356_290419_GMxCFO_r1.00_n4_v256.png",
        "../data/Siegel-04-05_S075_1356_290419_GMxCFO_r1.00_n4_v256.png",
        "../data/Siegel-04-08_S075_1356_290419_GMxCFO_r1.00_n4_v256.png",
        "../data/Siegel-04-09_S075_1356_060519_GMxCFO_r1.00_n4_v256.png",
    ]

    for left_filename in filenames:
        for right_filename in filenames:
            if left_filename == right_filename:
                continue

            template = {
                "name": "manufactured_motif",
                "left_filename": left_filename,
                "right_filename": right_filename,
                "synthetic_deformation": 0,
                "crop": [[100, 100], [300, 300]],
                "pad": [[0, 0], [0, 0]],
                "scale": 0.5,
                "equalize_disk": 100,
                "horizontal_patches": 100,
                "vertical_patches": 100,
                "residual_threshold": 50,
                "method": "autoencoder",
                "daisy_step": 2,
                "daisy_radius": 20,
                "daisy_rings": 3,
                "bovw_words": 1024,
                "bovw_radius": 20
            }

            experiments.append(template)

            for method in ["daisy", "bovw", "autoencoder"]:
                exp = template.copy()
                exp["name"] = "method"
                exp["method"] = method
                experiments.append(exp)

            for scale in [0.3, 0.5, 0.7]:
                exp = template.copy()
                exp["name"] = "scale"
                exp["scale"] = scale
                experiments.append(exp)

            for n_patches in [70, 100, 130]:
                exp = template.copy()
                exp["name"] = "n_patches"
                exp["horizontal_patches"] = n_patches
                exp["vertical_patches"] = n_patches
                experiments.append(exp)

            for residual_threshold in [30, 50, 100]:
                exp = template.copy()
                exp["name"] = "residual_threshold"
                exp["residual_threshold"] = residual_threshold
                experiments.append(exp)

            for daisy_step in [2, 3, 5, 10]:
                exp = template.copy()
                exp["name"] = "daisy_step"
                exp["method"] = "daisy"
                exp["daisy_step"] = daisy_step
                experiments.append(exp)

            for daisy_radius in [3, 5, 10, 20]:
                exp = template.copy()
                exp["name"] = "daisy_radius"
                exp["method"] = "daisy"
                exp["daisy_radius"] = daisy_radius
                experiments.append(exp)

            for daisy_rings in [1, 2, 3, 5]:
                exp = template.copy()
                exp["name"] = "daisy_rings"
                exp["method"] = "daisy"
                exp["daisy_rings"] = daisy_rings
                experiments.append(exp)

            for bovw_radius in [5, 10, 20]:
                exp = template.copy()
                exp["name"] = "bovw_radius"
                exp["method"] = "bovw"
                exp["bovw_radius"] = bovw_radius
                experiments.append(exp)

            for bovw_words in [128, 256, 512, 1024]:
                exp = template.copy()
                exp["name"] = "bovw_words"
                exp["method"] = "bovw"
                exp["bovw_words"] = bovw_words
                experiments.append(exp)

    # Synthetic deformation

    for left_filename in filenames:
        template = {
            "left_filename": left_filename,
            "right_filename": left_filename,
            "synthetic_deformation": 0,
            "crop": [[100, 100], [300, 300]],
            "pad": [[0, 0], [0, 0]],
            "scale": 0.5,
            "equalize_disk": 100,
            "horizontal_patches": 100,
            "vertical_patches": 100,
            "residual_threshold": 50,
            "method": "autoencoder",
            "daisy_step": 2,
            "daisy_radius": 20,
            "daisy_rings": 3,
            "bovw_words": 1024,
            "bovw_radius": 20
        }

        for synthetic_deformation in [10, 30, 50, 70, 90, 110, 130]:
            exp = template.copy()
            exp["name"] = "synthetic_deformation"
            exp["synthetic_deformation"] = synthetic_deformation
            experiments.append(exp)

    # Manufactured dice

    filenames = [
        "../data/cube_1.1_msii_r1_f0.png",
        "../data/cube_1.2_msii_r1_f0.png",
        "../data/cube_1.3_msii_r1_f0.png",
        "../data/cube_1.4_msii_r1_f0.png",
        "../data/cube_1.5_msii_r1_f0.png",
        "../data/cube_1.6_msii_r1_f0.png",
        "../data/cube_2.1_msii_r1_f0.png",
        "../data/cube_2.2_msii_r1_f0.png",
        "../data/cube_2.5_msii_r1_f0.png",
        "../data/cube_2.6_msii_r1_f0.png",
    ]

    for left_filename in filenames:
        for right_filename in filenames:
            if left_filename == right_filename:
                continue

            template = {
                "name": "manufactured_dice",
                "left_filename": left_filename,
                "right_filename": right_filename,
                "synthetic_deformation": 0,
                "crop": [[200, 200], [300, 300]],
                "pad": [[0, 0], [0, 0]],
                "scale": 0.5,
                "equalize_disk": 100,
                "horizontal_patches": 100,
                "vertical_patches": 100,
                "residual_threshold": 50,
                "method": "autoencoder",
                "daisy_step": 2,
                "daisy_radius": 20,
                "daisy_rings": 3,
                "bovw_words": 1024,
                "bovw_radius": 20
            }

            experiments.append(template)

            for method in ["daisy", "bovw", "autoencoder"]:
                exp = template.copy()
                exp["name"] = "method"
                exp["method"] = method
                experiments.append(exp)

    # Ancient impression

    filenames = [
        "../data/JR591_SM2066-HE5-060_310712001_AO.png",
        "../data/JR632_SM2066-HE5-060_310712001_crop_AO.png",
        "../data/JR634_SM2066-HE5-060_310712001_crop_AO.png",
        "../data/JR635_SM2066-HE5-060_310712001_crop_AO.png",
    ]

    for left_filename in filenames:
        for right_filename in filenames:
            if left_filename == right_filename:
                continue

            template = {
                "name": "original",
                "left_filename": left_filename,
                "right_filename": right_filename,
                "synthetic_deformation": 0,
                "crop": [[0, 0], [0, 0]],
                "pad": [[0, 0], [0, 0]],
                "scale": 0.5,
                "equalize_disk": 100,
                "horizontal_patches": 100,
                "vertical_patches": 100,
                "residual_threshold": 50,
                "method": "autoencoder",
                "daisy_step": 2,
                "daisy_radius": 20,
                "daisy_rings": 3,
                "bovw_words": 1024,
                "bovw_radius": 20
            }

            experiments.append(template)

    # Run each configuration

    experiments = [e for e in experiments if e["name"] == "synthetic_deformation"]

    for i, exp in enumerate(experiments):
        print("Running experiment {} of {} ...".format(i, len(experiments)))

        with open("experiment.json", "w") as f:
            json.dump(exp, f)

        subprocess.call(["python3", "01_preprocess.py", "experiment.json"])
        subprocess.call(["python3", "02_extract_features.py", "experiment.json"])
        subprocess.call(["python3", "03_register.py", "experiment.json"])
        subprocess.call(["python3", "04_plot.py", "experiment.json"])

        with open("../computed/04_plot.json", "r") as f:
            statistics = json.load(f)

        statistics.update(exp)
        summary.append(statistics)

        # Save all results

        with open("../computed/05_experiment.json", "w") as f:
            json.dump(summary, f, indent=4)


if __name__ == "__main__":
    main()
