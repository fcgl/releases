# coding=utf-8

import sys
import json
import collections

import numpy

import scipy.spatial.distance

import skimage.measure
import skimage.feature
import skimage.transform

import matplotlib.pyplot

import chuietal


def main():
    with open(sys.argv[1], "r") as f:
        config = json.load(f)
        residual_threshold = config["residual_threshold"]

    with open("../computed/01_preprocess.npz", "rb") as f:
        loaded = numpy.load(f)
        left_image = loaded["left_image"]
        right_image = loaded["right_image"]

    with open("../computed/02_extract_features.npz", "rb") as f:
        loaded = numpy.load(f)
        left_samples = loaded["left_samples"]
        right_samples = loaded["right_samples"]
        left_features = loaded["left_features"]
        right_features = loaded["right_features"]

    matches = skimage.feature.match_descriptors(
        left_features, right_features, metric='cosine')

    model, inliers = skimage.measure.ransac(
        (left_samples[matches[:, 0]],
         right_samples[matches[:, 1]]),
        skimage.transform.EuclideanTransform,
        min_samples=3, residual_threshold=residual_threshold,
        max_trials=5000)

    figure, axes = matplotlib.pyplot.subplots(1, 1)

    skimage.feature.plot_matches(
        axes, left_image, right_image, left_samples,
        right_samples, matches[inliers], only_matches=True)

    axes.axis("off")

    figure.set_size_inches(20, 10)
    figure.tight_layout()
    figure.savefig("../plots/matches.png", dpi=200)

    left_rigid = skimage.transform.warp(left_image, model, mode="reflect")

    left_matched = left_samples[matches[inliers, 0]]
    right_matched = right_samples[matches[inliers, 1]]

    nonrigid_transform = chuietal.fit_tps(left_matched, right_matched, 1000)
    left_nonrigid = chuietal.apply_tps_transform(nonrigid_transform, left_image)

    with open("../computed/03_register.npz", "wb") as f:
        numpy.savez_compressed(
            f,
            nonrigid_transform=nonrigid_transform,
            left_rigid=left_rigid,
            left_nonrigid=left_nonrigid)


if __name__ == "__main__":
    main()
