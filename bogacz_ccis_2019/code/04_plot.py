# coding=utf-8

import sys
import json

import numpy

import matplotlib.pyplot

import scipy.spatial.distance

import skimage.filters

import chuietal


def additive_overlay(image1, image2):
    a, b = image1.shape
    c, d = image2.shape
    e, f = min(a, c), min(b, d)
    overlay = numpy.zeros((e, f))
    overlay += image1[0:e, 0:f]
    overlay += image2[0:e, 0:f]
    return overlay


def difference_overlay(image1, image2):
    a, b = image1.shape
    c, d = image2.shape
    e, f = min(a, c), min(b, d)
    overlay = numpy.zeros((e, f))
    overlay += image1[0:e, 0:f]
    overlay -= image2[0:e, 0:f]
    overlay = numpy.abs(overlay)
    return overlay


def compute_difference_of_transformations(image, transformation1,
                                          transformation2, resolution):
    pdist = scipy.spatial.distance.pdist
    squareform = scipy.spatial.distance.squareform

    # Create homogeneous coordinates (1, x, y) of a regular
    # grid spanning the size of the input image
    grid = numpy.meshgrid(
        numpy.linspace(0, image.shape[1], resolution),
        numpy.linspace(0, image.shape[0], resolution),
        sparse=False)

    grid = numpy.stack(
        (numpy.ones((resolution * resolution,)),
         numpy.ravel(grid[0]),
         numpy.ravel(grid[1])), axis=1)

    # Transform both grids grid according supplied transformations
    grid1 = chuietal.chui_transform(grid, transformation1)
    grid2 = chuietal.chui_transform(grid, transformation2)

    # We are interested in the the differences between moved
    # grid points. However, we discount difference from far
    # removed points with a gaussian rooted at the respective
    # grid points.
    diff = numpy.linalg.norm(grid2 - grid1, axis=1)

    return grid, grid1, grid2, diff


def show_grid_deformation(axes, image, transformation, resolution):
    pdist = scipy.spatial.distance.pdist
    squareform = scipy.spatial.distance.squareform
    cmap = matplotlib.cm.get_cmap('RdYlBu')

    # Create homogeneous coordinates (1, x, y) of a regular
    # grid spanning the size of the input image
    grid1 = numpy.meshgrid(
        numpy.linspace(0, image.shape[1], resolution),
        numpy.linspace(0, image.shape[0], resolution),
        sparse=False)

    grid1 = numpy.stack(
        (numpy.ones((resolution * resolution,)),
         numpy.ravel(grid1[0]),
         numpy.ravel(grid1[1])), axis=1)
    # Compute pairwise distances between all grid points
    dist1 = squareform(pdist(grid1))
    # We resuse this computation to compute the weighting
    # for the Gaussian kernel used to discount farther away
    # points.
    kernel = dist1 / numpy.max(dist1)
    kernel = numpy.exp(-kernel / 0.01)
    kernel = kernel / numpy.sum(kernel, axis=0)
    # Transform grid according supplied transformation
    grid2 = chuietal.chui_transform(grid1, transformation)
    dist2 = squareform(pdist(grid2))
    # We are interested in the the differences between moved
    # grid points. However, we discount difference from far
    # removed points with a gaussian rooted at the respective
    # grid points.
    diff = (dist2 - dist1) * kernel
    diff = numpy.sum(diff, axis=0)
    # We move the differences into a (-0.5, 0.5) range
    colors = diff / (-numpy.min(diff) + numpy.max(diff))
    # And move these into a (0, 1.0) range with 0.5 meaning
    # no grid point movement.
    colors += 0.5
    colors = cmap(colors)
    colors[:, 3] = 0.8
    axes.imshow(image, cmap="gray", alpha=0.8)
    axes.scatter(grid2[:, 1], grid2[:, 2], color=colors)
    axes.set_ylim([image.shape[0], 0])
    axes.set_xlim([0, image.shape[1]])
    axes.axis("off")


def deformation_energy(image, transform):
    grid1 = numpy.meshgrid(
        numpy.linspace(0, image.shape[1], 100),
        numpy.linspace(0, image.shape[0], 100),
        sparse=False)

    grid1 = numpy.stack(
        (numpy.ones((100 * 100,)),
         numpy.ravel(grid1[0]),
         numpy.ravel(grid1[1])), axis=1)

    grid2 = chuietal.chui_transform(grid1, transform)

    diff = numpy.linalg.norm(grid2[:, [1, 2]] - grid1[:, [1, 2]], axis=1)
    diff = numpy.reshape(diff, (100, 100))

    return numpy.sum(diff)


def main():
    with open(sys.argv[1], "r") as f:
        config = json.load(f)
        residual_threshold = config["residual_threshold"]

    with open("../computed/01_preprocess.npz", "rb") as f:
        loaded = numpy.load(f)
        left_image = loaded["left_image"]
        right_image = loaded["right_image"]

    with open("../computed/02_extract_features.npz", "rb") as f:
        loaded = numpy.load(f)
        left_samples = loaded["left_samples"]
        right_samples = loaded["right_samples"]
        left_features = loaded["left_features"]
        right_features = loaded["right_features"]

    with open("../computed/03_register.npz", "rb") as f:
        loaded = numpy.load(f)
        nonrigid_transform = loaded["nonrigid_transform"]
        left_rigid = loaded["left_rigid"]
        left_nonrigid = loaded["left_nonrigid"]

    figure, axes = matplotlib.pyplot.subplots(2, 3)
    figure.set_size_inches(13, 10)
    figure.tight_layout()

    axes[0, 0].imshow(left_image, cmap="gray")
    axes[0, 0].axis("off")

    axes[1, 0].imshow(right_image, cmap="gray")
    axes[1, 0].axis("off")

    # Deformation

    show_grid_deformation(axes[0, 1], left_nonrigid, nonrigid_transform, 40)

    # Addition

    addition = additive_overlay(left_nonrigid, right_image)
    axes[1, 1].imshow(addition, cmap="gray")
    axes[1, 1].axis("off")

    # Difference

    difference = difference_overlay(left_nonrigid, right_image)
    axes[1, 2].imshow(255 - difference, cmap="gray")
    axes[1, 2].axis("off")

    # Binary

    threshold = skimage.filters.threshold_niblack(
        right_image, window_size=51)
    right_binary = right_image > threshold
    threshold = skimage.filters.threshold_niblack(
        left_nonrigid, window_size=51)
    left_binary = left_nonrigid > threshold

    binary = numpy.zeros((left_binary.shape[0], left_binary.shape[1]))
    binary[left_binary] = 1
    binary[right_binary] = 2
    binary[numpy.logical_and(left_binary, right_binary)] = 3

    axes[0, 2].imshow(binary)
    axes[0, 2].axis("off")

    figure.savefig("../plots/report.png")

    # Quiver

    grid1 = numpy.meshgrid(
        numpy.linspace(0, left_image.shape[1], 30),
        numpy.linspace(0, right_image.shape[0], 30),
        sparse=False)

    grid1 = numpy.stack(
        (numpy.ones((30 * 30,)),
         numpy.ravel(grid1[0]),
         numpy.ravel(grid1[1])), axis=1)

    grid2 = chuietal.chui_transform(grid1, nonrigid_transform)

    figure, axes = matplotlib.pyplot.subplots(1, 1)
    figure.set_size_inches(10, 10)
    figure.tight_layout()
    x, u = grid1, grid2
    axes.imshow(binary)
    axes.quiver(x[:, 1], x[:, 2], u[:, 1] - x[:, 1], u[:, 2] - x[:, 2],
                scale=1000, color='red')
    axes.axis("off")
    figure.savefig("../plots/05_quiver.png")

    # Statistics

    if config["synthetic_deformation"] > 0:
        with open("../computed/01_transform.npz", "rb") as f:
            loaded = numpy.load(f)
            transform = loaded["transform"]

        _, _, _, grid_diff = compute_difference_of_transformations(
            left_image, nonrigid_transform, transform, 50)
        grid_diff = numpy.mean(grid_diff)
    else:
        grid_diff = 0

    differences = difference_overlay(left_nonrigid, right_image)
    differences = numpy.sum(differences) / (differences.shape[0] * differences.shape[1])

    statistics = {
        "difference_of_grids": grid_diff,
        "differences": differences,
        "energy": deformation_energy(left_nonrigid, nonrigid_transform)
    }

    with open("../computed/04_plot.json", "w") as f:
        json.dump(statistics, f)


if __name__ == "__main__":
    main()
