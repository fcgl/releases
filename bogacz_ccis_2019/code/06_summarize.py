# coding=utf-8

import sys
import json

import numpy
import pandas

import matplotlib.pyplot


def main():
    with open("../computed/05_experiment.json", "r") as f:
        experiments = json.load(f)

    experiments = pandas.DataFrame(experiments)

    figure, axes = matplotlib.pyplot.subplots(1, 1)
    figure.set_size_inches(5, 5)

    axes.set_title("Method")
    axes.boxplot((
        experiments.loc[(experiments["name"] == "method")
                        & (experiments["method"] == "daisy")
                        & (experiments["left_filename"].str.contains("Siegel")),
                        "differences"],
        experiments.loc[(experiments["name"] == "method")
                        & (experiments["method"] == "daisy")
                        & (experiments["left_filename"].str.contains("cube")),
                        "differences"],
        experiments.loc[(experiments["name"] == "method")
                        & (experiments["method"] == "bovw")
                        & (experiments["left_filename"].str.contains("Siegel")),
                        "differences"],
        experiments.loc[(experiments["name"] == "method")
                        & (experiments["method"] == "bovw")
                        & (experiments["left_filename"].str.contains("cube")),
                        "differences"],
        experiments.loc[(experiments["name"] == "method")
                        & (experiments["method"] == "autoencoder")
                        & (experiments["left_filename"].str.contains("Siegel")),
                        "differences"],
        experiments.loc[(experiments["name"] == "method")
                        & (experiments["method"] == "autoencoder")
                        & (experiments["left_filename"].str.contains("cube")),
                        "differences"]

    ), labels=["DAISY\nMotif", "DAISY\nDice", "BOVW\nMotif", "BOVW\nDice", "Autoenc.\nMotif", "Autoenc.\nDice"])

    figure.tight_layout()
    figure.savefig("../plots/boxplot_methods.png")

    figure, axes = matplotlib.pyplot.subplots(2, 3)
    figure.set_size_inches(10, 6)

    axes[0, 0].set_title("Patches per side")
    axes[0, 0].boxplot((
        experiments.loc[(experiments["name"] == "n_patches")
                        & (experiments["horizontal_patches"] == 70),
                        "differences"],
        experiments.loc[(experiments["name"] == "n_patches")
                        & (experiments["horizontal_patches"] == 100),
                        "differences"],
        experiments.loc[(experiments["name"] == "n_patches")
                        & (experiments["horizontal_patches"] == 130),
                        "differences"]
    ), labels=["70", "100", "130"])

    axes[0, 1].set_title("Image scale factor")
    axes[0, 1].boxplot((
        experiments.loc[(experiments["name"] == "scale")
                        & (experiments["scale"] == 0.3),
                        "differences"],
        experiments.loc[(experiments["name"] == "scale")
                        & (experiments["scale"] == 0.5),
                        "differences"],
        experiments.loc[(experiments["name"] == "scale")
                        & (experiments["scale"] == 0.7),
                        "differences"]
    ), labels=["0.3", "0.5", "0.7"])

    axes[0, 2].set_title("Rigid alignment error threshold")
    axes[0, 2].boxplot((
        experiments.loc[(experiments["name"] == "residual_threshold")
                        & (experiments["residual_threshold"] == 30),
                        "differences"],
        experiments.loc[(experiments["name"] == "residual_threshold")
                        & (experiments["residual_threshold"] == 50),
                        "differences"],
        experiments.loc[(experiments["name"] == "residual_threshold")
                        & (experiments["residual_threshold"] == 100),
                        "differences"],
    ), labels=["30", "50", "100"])

    axes[1, 0].set_title("Daisy step size")
    axes[1, 0].boxplot((
        experiments.loc[(experiments["name"] == "daisy_step")
                        & (experiments["daisy_step"] == 2),
                        "differences"],
        experiments.loc[(experiments["name"] == "daisy_step")
                        & (experiments["daisy_step"] == 3),
                        "differences"],
        experiments.loc[(experiments["name"] == "daisy_step")
                        & (experiments["daisy_step"] == 5),
                        "differences"],
        experiments.loc[(experiments["name"] == "daisy_step")
                        & (experiments["daisy_step"] == 10),
                        "differences"]
    ), labels=["2", "3", "5", "10"])

    # axes[1, 1].set_title("Daisy ring count")
    # axes[1, 1].boxplot((
    #     experiments.loc[(experiments["name"] == "daisy_rings")
    #                     & (experiments["daisy_rings"] == 1),
    #                     "differences"],
    #     experiments.loc[(experiments["name"] == "daisy_rings")
    #                     & (experiments["daisy_rings"] == 2),
    #                     "differences"],
    #     experiments.loc[(experiments["name"] == "daisy_rings")
    #                     & (experiments["daisy_rings"] == 3),
    #                     "differences"],
    #     experiments.loc[(experiments["name"] == "daisy_rings")
    #                     & (experiments["daisy_rings"] == 5),
    #                     "differences"]
    # ), labels=["1", "2", "3", "5"])

    axes[1, 1].set_title("Daisy radius")
    axes[1, 1].boxplot((
        experiments.loc[(experiments["name"] == "daisy_radius")
                        & (experiments["daisy_radius"] == 3),
                        "differences"],
        experiments.loc[(experiments["name"] == "daisy_radius")
                        & (experiments["daisy_radius"] == 5),
                        "differences"],
        experiments.loc[(experiments["name"] == "daisy_radius")
                        & (experiments["daisy_radius"] == 10),
                        "differences"],
        experiments.loc[(experiments["name"] == "daisy_radius")
                        & (experiments["daisy_radius"] == 20),
                        "differences"]
    ), labels=["3", "5", "10", "20"])

    axes[1, 2].set_title("BOVW radius")
    axes[1, 2].boxplot((
        experiments.loc[(experiments["name"] == "bovw_radius")
                        & (experiments["bovw_radius"] == 5),
                        "differences"],
        experiments.loc[(experiments["name"] == "bovw_radius")
                        & (experiments["bovw_radius"] == 10),
                        "differences"],
        experiments.loc[(experiments["name"] == "bovw_radius")
                        & (experiments["bovw_radius"] == 20),
                        "differences"]
    ), labels=["5", "10", "20"])

    # axes[2, 1].set_title("BOVW word count")
    # axes[2, 1].boxplot((
    #     experiments.loc[(experiments["name"] == "bovw_words")
    #                     & (experiments["bovw_words"] == 128),
    #                     "differences"],
    #     experiments.loc[(experiments["name"] == "bovw_words")
    #                     & (experiments["bovw_words"] == 256),
    #                     "differences"],
    #     experiments.loc[(experiments["name"] == "bovw_words")
    #                     & (experiments["bovw_words"] == 512),
    #                     "differences"],
    #     experiments.loc[(experiments["name"] == "bovw_words")
    #                     & (experiments["bovw_words"] == 1024),
    #                     "differences"]
    # ), labels=["128", "256", "512", "1024"])

    figure.tight_layout()
    figure.savefig("../plots/06_boxplots.png")

    # Synthetic plots

    figure, axes = matplotlib.pyplot.subplots(1, 2)
    figure.set_size_inches(10, 5)

    axes[0].set_xlabel("Deformation Strength")
    axes[0].set_ylabel("Mean Pixel Difference")
    axes[0].boxplot((
        experiments.loc[(experiments["name"] == "synthetic_deformation")
                        & (experiments["synthetic_deformation"] == 10),
                        "differences"],
        experiments.loc[(experiments["name"] == "synthetic_deformation")
                        & (experiments["synthetic_deformation"] == 30),
                        "differences"],
        experiments.loc[(experiments["name"] == "synthetic_deformation")
                        & (experiments["synthetic_deformation"] == 50),
                        "differences"],
        experiments.loc[(experiments["name"] == "synthetic_deformation")
                        & (experiments["synthetic_deformation"] == 70),
                        "differences"],
        experiments.loc[(experiments["name"] == "synthetic_deformation")
                        & (experiments["synthetic_deformation"] == 90),
                        "differences"],
        experiments.loc[(experiments["name"] == "synthetic_deformation")
                        & (experiments["synthetic_deformation"] == 110),
                        "differences"],
        experiments.loc[(experiments["name"] == "synthetic_deformation")
                        & (experiments["synthetic_deformation"] == 130),
                        "differences"]
    ), labels=["10", "30", "50", "70", "90", "110", "130"])

    axes[1].set_xlabel("Deformation Strength")
    axes[1].set_ylabel("Mean Grid Difference")
    axes[1].boxplot((
        experiments.loc[(experiments["name"] == "synthetic_deformation")
                        & (experiments["synthetic_deformation"] == 10),
                        "difference_of_grids"],
        experiments.loc[(experiments["name"] == "synthetic_deformation")
                        & (experiments["synthetic_deformation"] == 30),
                        "difference_of_grids"],
        experiments.loc[(experiments["name"] == "synthetic_deformation")
                        & (experiments["synthetic_deformation"] == 50),
                        "difference_of_grids"],
        experiments.loc[(experiments["name"] == "synthetic_deformation")
                        & (experiments["synthetic_deformation"] == 70),
                        "difference_of_grids"],
        experiments.loc[(experiments["name"] == "synthetic_deformation")
                        & (experiments["synthetic_deformation"] == 90),
                        "difference_of_grids"],
        experiments.loc[(experiments["name"] == "synthetic_deformation")
                        & (experiments["synthetic_deformation"] == 110),
                        "difference_of_grids"],
        experiments.loc[(experiments["name"] == "synthetic_deformation")
                        & (experiments["synthetic_deformation"] == 130),
                        "difference_of_grids"]
    ), labels=["10", "30", "50", "70", "90", "110", "130"])

    figure.tight_layout()
    figure.savefig("../plots/synthetic.png")

    # Show comparison matrices

    figure, axes = matplotlib.pyplot.subplots(2, 3)
    figure.set_size_inches(10, 8)

    default = experiments[experiments["name"] == "original"]
    left_filenames = sorted(set(default["left_filename"].tolist()))
    right_filenames = sorted(set(default["right_filename"].tolist()))
    distances = numpy.zeros((len(left_filenames), len(right_filenames)))
    deformation = numpy.zeros((len(left_filenames), len(right_filenames)))

    print(left_filenames)

    for i, left in enumerate(left_filenames):
        for j, right in enumerate(right_filenames):
            res = default.loc[
                (default["left_filename"] == left)
                & (default["right_filename"] == right),
                "differences"].tolist()
            if len(res) == 1:
                distances[i, j] = res[0]

            res = default.loc[
                (default["left_filename"] == left)
                & (default["right_filename"] == right),
                "energy"].tolist()
            if len(res) == 1:
                deformation[i, j] = res[0]

    axes[0, 0].matshow(distances)
    axes[0, 0].set_xticks([])
    axes[0, 0].set_yticks([0, 1, 2, 3])
    axes[0, 0].set_yticklabels(["HR591", "HR632", "HR634", "HR635"])
    axes[0, 0].set_xlabel("Difference in pixels")
    axes[0, 0].set_title("Original ancient impression")

    axes[1, 0].matshow(deformation)
    axes[1, 0].set_xticks([])
    axes[1, 0].set_yticks([0, 1, 2, 3])
    axes[1, 0].set_yticklabels(["HR591", "HR632", "HR634", "HR635"])
    axes[1, 0].set_xlabel("Deformation energy")
    axes[1, 0].set_title("Original ancient impression")

    default = experiments[experiments["name"] == "manufactured_dice"]
    left_filenames = sorted(set(default["left_filename"].tolist()))
    right_filenames = sorted(set(default["right_filename"].tolist()))
    distances = numpy.zeros((len(left_filenames), len(right_filenames)))
    deformation = numpy.zeros((len(left_filenames), len(right_filenames)))

    for i, left in enumerate(left_filenames):
        for j, right in enumerate(right_filenames):
            res = default.loc[
                (default["left_filename"] == left)
                & (default["right_filename"] == right),
                "differences"].tolist()
            if len(res) == 1:
                distances[i, j] = res[0]

            res = default.loc[
                (default["left_filename"] == left)
                & (default["right_filename"] == right),
                "energy"].tolist()
            if len(res) == 1:
                deformation[i, j] = res[0]

    axes[0, 1].matshow(distances)
    axes[0, 1].set_xticks([])
    axes[0, 1].set_yticks([0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
    axes[0, 1].set_yticklabels(["01.01", "01.02", "01.03", "01.04", "01.05",
                                "01.06", "02.01", "02.02", "02.05", "02.06"])
    axes[0, 1].set_xlabel("Difference in pixels")
    axes[0, 1].set_title("Manufactured dice impression")

    axes[1, 1].matshow(deformation)
    axes[1, 1].set_xticks([])
    axes[1, 1].set_yticks([0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
    axes[1, 1].set_yticklabels(["01.01", "01.02", "01.03", "01.04", "01.05",
                                "01.06", "02.01", "02.02", "02.05", "02.06"])
    axes[1, 1].set_xlabel("Deformation energy")
    axes[1, 1].set_title("Manufactured dice impression")

    default = experiments[experiments["name"] == "manufactured_motif"]
    left_filenames = sorted(set(default["left_filename"].tolist()))
    right_filenames = sorted(set(default["right_filename"].tolist()))
    distances = numpy.zeros((len(left_filenames), len(right_filenames)))
    deformation = numpy.zeros((len(left_filenames), len(right_filenames)))

    for i, left in enumerate(left_filenames):
        for j, right in enumerate(right_filenames):
            res = default.loc[
                (default["left_filename"] == left)
                & (default["right_filename"] == right),
                "differences"].tolist()
            if len(res) == 1:
                distances[i, j] = res[0]

            res = default.loc[
                (default["left_filename"] == left)
                & (default["right_filename"] == right),
                "energy"].tolist()
            if len(res) == 1:
                deformation[i, j] = res[0]

    axes[0, 2].matshow(distances)
    axes[0, 2].set_xticks([])
    axes[0, 2].set_yticks([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11])
    axes[0, 2].set_yticklabels(["03.01", "03.02", "03.03", "03.04", "03.05",
                                "03.07", "03.10", "04.02", "04.03", "04.05",
                                "04.08", "04.09"])
    axes[0, 2].set_xlabel("Difference in pixels")
    axes[0, 2].set_title("Manufactured motif impression")

    axes[1, 2].matshow(deformation)
    axes[1, 2].set_xticks([])
    axes[1, 2].set_yticks([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11])
    axes[1, 2].set_yticklabels(["03.01", "03.02", "03.03", "03.04", "03.05",
                                "03.07", "03.10", "04.02", "04.03", "04.05",
                                "04.08", "04.09"])
    axes[1, 2].set_xlabel("Deformation energy")
    axes[1, 2].set_title("Manufactured motif impression")

    figure.tight_layout()
    figure.savefig("../plots/06_differences.png")


if __name__ == "__main__":
    main()
