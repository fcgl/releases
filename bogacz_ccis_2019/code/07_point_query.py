# coding=utf-8

import sys
import json
import subprocess

import numpy

import scipy.spatial.distance

import matplotlib.pyplot


def visualize_point_query_response(axes1, axes2, reference, *,
                                   image1, image2,
                                   keypoints1, keypoints2,
                                   descriptors1, descriptors2):
    map_colors = matplotlib.cm.get_cmap('plasma')

    distances = scipy.spatial.distance.cdist(descriptors1, descriptors2,
                                             metric="cosine")

    closest1 = numpy.argmin(numpy.linalg.norm(keypoints1 - reference, axis=1))
    closest2 = numpy.argsort(distances[closest1, :])[:500]

    axes1.imshow(image1, cmap="gray", alpha=0.5)
    axes1.scatter(
        keypoints1[closest1, 1],
        keypoints1[closest1, 0],
        s=500, marker=".", color="red")
    axes1.axis("off")

    colors = -distances[closest1, closest2]
    colors -= numpy.min(colors)
    colors /= numpy.max(colors)
    colors = map_colors(colors)

    axes2.imshow(image2, cmap="gray", alpha=0.5)
    axes2.scatter(
        keypoints2[closest2, 1],
        keypoints2[closest2, 0],
        c=colors, s=3, marker="s")
    axes2.axis("off")


def main():
    with open(sys.argv[1], "r") as f:
        config = json.load(f)

    figure, axes = matplotlib.pyplot.subplots(3, 4)
    figure.set_size_inches(15, 10)
    figure.tight_layout()

    for i, query in enumerate([(70, 150), (150, 100), (250, 310)]):
        for j, method in enumerate(["daisy", "bovw", "autoencoder"]):

            config["left_filename"] = \
                "../data/Siegel-03-01" \
                "_S075_1356_170419_GMxCFO_r1.00_n4_v256.png"

            config["right_filename"] = \
                "../data/Siegel-04-02" \
                "_S075_1356_240419_GMxCFO_r1.00_n4_v256.png"

            config["method"] = method

            with open("visualize.json", "w") as f:
                json.dump(config, f)

            subprocess.check_call(
                ["python3", "01_preprocess.py", "visualize.json"])
            subprocess.check_call(
                ["python3", "02_extract_features.py", "visualize.json"])

            with open("../computed/01_preprocess.npz", "rb") as f:
                loaded = numpy.load(f)
                left_image = loaded["left_image"]
                right_image = loaded["right_image"]

            with open("../computed/02_extract_features.npz", "rb") as f:
                loaded = numpy.load(f)
                left_samples = loaded["left_samples"]
                right_samples = loaded["right_samples"]
                left_features = loaded["left_features"]
                right_features = loaded["right_features"]

            visualize_point_query_response(axes[i, 0], axes[i, j+1],
                query, image1=left_image, image2=right_image,
                keypoints1=left_samples, keypoints2=right_samples,
                descriptors1=left_features, descriptors2=right_features)

            axes[i, j+1].set_xlabel(method)

    figure.savefig("../plots/07_point_query.png")


if __name__ == "__main__":
    main()