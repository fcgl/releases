# coding=utf-8

import sys
import json
import math

import numpy

import scipy.spatial.distance

import skimage.feature

import sklearn.cluster


def patches_from_samples(image, samples):
    patches = numpy.zeros((samples.shape[0], 64, 64))

    for i, (y, x) in enumerate(samples):
        patches[i, :, :] = image[y - 64 // 2:y + 64 // 2,
                           x - 64 // 2:x + 64 // 2]

    assert patches.shape[0] == samples.shape[0]

    return patches


def with_autoencoder(left_image, right_image, left_samples, right_samples):
    from keras.layers import Input, Dense, Conv2D, MaxPooling2D, UpSampling2D
    from keras.models import Model
    from keras.optimizers import Adagrad

    # Setup autoencoder

    input_img = Input(shape=(64, 64, 1))

    x = Conv2D(8, (3, 3), activation='relu', padding='same')(input_img)
    x = MaxPooling2D((2, 2), padding='same')(x)
    x = Conv2D(8, (3, 3), activation='relu', padding='same')(x)
    x = MaxPooling2D((2, 2), padding='same')(x)
    x = Conv2D(16, (3, 3), activation='relu', padding='same')(x)
    x = MaxPooling2D((2, 2), padding='same')(x)
    x = Conv2D(16, (3, 3), activation='relu', padding='same')(x)
    encoded = MaxPooling2D((2, 2), padding='same')(x)

    x = Conv2D(16, (3, 3), activation='relu', padding='same')(encoded)
    x = UpSampling2D((2, 2))(x)
    x = Conv2D(16, (3, 3), activation='relu', padding='same')(x)
    x = UpSampling2D((2, 2))(x)
    x = Conv2D(8, (3, 3), activation='relu', padding='same')(x)
    x = UpSampling2D((2, 2))(x)
    x = Conv2D(8, (3, 3), activation='relu', padding='same')(x)
    x = UpSampling2D((2, 2))(x)
    decoded = Conv2D(1, (3, 3), activation='sigmoid', padding='same')(x)

    autoencoder = Model(input_img, decoded)
    encoder = Model(input_img, encoded)

    adagrad = Adagrad(lr=0.01, epsilon=None, decay=0.0)
    autoencoder.compile(optimizer=adagrad, loss='mean_squared_error')

    autoencoder.summary()

    # Extract and reshape descriptors

    left_descriptors = patches_from_samples(
        left_image, left_samples)
    left_descriptors = numpy.reshape(left_descriptors,
        (left_samples.shape[0], 64, 64, 1))
    left_descriptors = left_descriptors / 255

    right_descriptors = patches_from_samples(
        right_image, right_samples)
    right_descriptors = numpy.reshape(right_descriptors,
        (right_samples.shape[0], 64, 64, 1))
    right_descriptors = right_descriptors / 255

    # Fit Autoencoder

    y_train = numpy.concatenate((left_descriptors, right_descriptors))
    x_train = y_train + numpy.random.random((y_train.shape[0], 64, 64, 1)) / 2

    autoencoder.fit(x_train, y_train,
        epochs=5, batch_size=32, shuffle=True,
        validation_split=0.1, verbose=1)

    # Extract feature vectors

    left_features = encoder.predict(left_descriptors)
    left_features = numpy.reshape(
        left_features, (left_descriptors.shape[0], -1))
    right_features = encoder.predict(right_descriptors)
    right_features = numpy.reshape(
        right_features, (right_descriptors.shape[0], -1))

    return left_features, right_features


def sampling_grid(image, n_horizontal, n_vertical):
    points_y, points_x = numpy.meshgrid(
        numpy.linspace(64 // 2, image.shape[0] - 64 // 2,
                       n_horizontal, dtype=int),
        numpy.linspace(64 // 2, image.shape[1] - 64 // 2,
                       n_vertical, dtype=int),
        sparse=False)
    points_y = numpy.ravel(points_y)
    points_x = numpy.ravel(points_x)
    points = numpy.stack((points_y, points_x), axis=1)
    return points


def daisy_sampling_grid(image, *, step, radius):
    # Reverse engineer samples on image at which DAISY
    # descriptors have been extracted.

    # DAISY as implemented by skimage generates keysamples
    # row major, i.e., x y
    samples_x, samples_y = numpy.meshgrid(
        numpy.linspace(radius, image.shape[1] - radius,
                       int(math.ceil((image.shape[1] - radius * 2) / step)),
                       dtype=int),
        numpy.linspace(radius, image.shape[0] - radius,
                       int(math.ceil((image.shape[0] - radius * 2) / step)),
                       dtype=int),
        sparse=False)

    samples_x = numpy.ravel(samples_x)
    samples_y = numpy.ravel(samples_y)
    samples = numpy.stack((samples_y, samples_x), axis=1)

    return samples


def with_daisy(left_image, right_image, *, step, radius, rings):
    left_descriptors = skimage.feature.daisy(
        left_image, step=step, radius=radius, rings=rings)

    left_descriptors = numpy.reshape(
        left_descriptors,
        (left_descriptors.shape[0] * left_descriptors.shape[1],
         left_descriptors.shape[2]))

    left_samples = daisy_sampling_grid(left_image, step=step, radius=radius)

    assert left_samples.shape[0] == left_descriptors.shape[0]

    right_descriptors = skimage.feature.daisy(
        right_image, step=step, radius=radius, rings=rings)

    right_descriptors = numpy.reshape(
        right_descriptors,
        (right_descriptors.shape[0] * right_descriptors.shape[1],
         right_descriptors.shape[2]))

    right_samples = daisy_sampling_grid(right_image, step=step, radius=radius)

    assert right_samples.shape[0] == right_descriptors.shape[0]

    return left_samples, right_samples, left_descriptors, right_descriptors


def with_bovw(left_image, right_image, left_samples, right_samples, *,
              words, bag_radius, step, radius, rings):
    left_descriptors = skimage.feature.daisy(
        left_image, step=step, radius=radius, rings=rings)

    left_descriptors = numpy.reshape(
        left_descriptors,
        (left_descriptors.shape[0] * left_descriptors.shape[1],
         left_descriptors.shape[2]))

    right_descriptors = skimage.feature.daisy(
        right_image, step=step, radius=radius, rings=rings)

    right_descriptors = numpy.reshape(
        right_descriptors,
        (right_descriptors.shape[0] * right_descriptors.shape[1],
         right_descriptors.shape[2]))

    left_descriptor_samples = daisy_sampling_grid(
        left_image, step=step, radius=radius)
    right_descriptor_samples = daisy_sampling_grid(
        right_image, step=step, radius=radius)

    assert left_descriptor_samples.shape[0] == left_descriptors.shape[0]
    assert right_descriptor_samples.shape[0] == right_descriptors.shape[0]

    clustering = sklearn.cluster.KMeans(n_clusters=words)

    #clustering.fit(numpy.concatenate((left_descriptors, right_descriptors)))
    clustering.fit(left_descriptors)

    # Extract

    assert left_descriptor_samples.shape[0] == left_descriptors.shape[0]
    assert right_descriptor_samples.shape[0] == right_descriptors.shape[0]

    # Discretize descriptors into visual words

    left_discretized = clustering.predict(left_descriptors)
    right_discretized = clustering.predict(right_descriptors)

    # Adjacency matrix of descriptors close to samples

    left_distances = scipy.spatial.distance.cdist(
        left_samples, left_descriptor_samples)
    left_distances = numpy.where(left_distances < bag_radius, 1, 0)

    right_distances = scipy.spatial.distance.cdist(
        right_samples, right_descriptor_samples)
    right_distances = numpy.where(right_distances < bag_radius, 1, 0)

    # Fill visual words with counts of nearby words

    left_bovws = numpy.zeros((left_samples.shape[0], words))
    right_bovws = numpy.zeros((right_samples.shape[0], words))

    for i in range(left_samples.shape[0]):
        words = left_discretized[left_distances[i, :] == 1]
        values, counts = numpy.unique(words, return_counts=True)
        left_bovws[i, values] = counts

    for i in range(right_samples.shape[0]):
        words = right_discretized[right_distances[i, :] == 1]
        values, counts = numpy.unique(words, return_counts=True)
        right_bovws[i, values] = counts

    return left_bovws, right_bovws


def main():
    with open(sys.argv[1], "r") as f:
        config = json.load(f)
        method = config["method"]
        daisy_step = config["daisy_step"]
        daisy_radius = config["daisy_radius"]
        daisy_rings = config["daisy_rings"]
        bovw_words = config["bovw_words"]
        bovw_radius = config["bovw_radius"]

    with open("../computed/01_preprocess.npz", "rb") as f:
        loaded = numpy.load(f)
        horizontal_patches = config["horizontal_patches"]
        vertical_patches = config["vertical_patches"]
        left_image = loaded["left_image"]
        right_image = loaded["right_image"]

    if method == "autoencoder":
        left_samples = sampling_grid(
            left_image, horizontal_patches, vertical_patches)
        right_samples = sampling_grid(
            right_image, horizontal_patches, vertical_patches)

        left_features, right_features = with_autoencoder(
            left_image, right_image, left_samples, right_samples)
    elif method == "daisy":
        left_samples, right_samples, left_features, right_features = \
            with_daisy(left_image, right_image,
                       step=daisy_step, radius=daisy_radius,
                       rings=daisy_rings)
    elif method == "bovw":
        left_samples = sampling_grid(
            left_image, horizontal_patches, vertical_patches)
        right_samples = sampling_grid(
            right_image, horizontal_patches, vertical_patches)

        left_features, right_features = \
            with_bovw(left_image, right_image, left_samples, right_samples,
                      words=bovw_radius, bag_radius=bovw_radius,
                      step=daisy_step, radius=daisy_radius, rings=daisy_rings)
    else:
        assert False

    with open("../computed/02_extract_features.npz", "wb") as f:
        numpy.savez_compressed(
            f,
            left_samples=left_samples,
            right_samples=right_samples,
            left_features=left_features,
            right_features=right_features)


if __name__ == "__main__":
    main()
