# coding=utf-8

# TODO: Change module name to something sensible

import collections

import numpy
import numpy.linalg

import scipy.spatial

import skimage.transform

# TODO: Remove namedtuple and pass parameters one by one
# A set of asserts can check validity of passed parameters
Transform = collections.namedtuple("Transform", "control, affine, warping")

# TODO: Rename these TPS functions and the homogenous to euclidean
# wrappers below to more sensible names.


def chui_transform(points, parameters):
    # FIXME: Rename this function to something more sensible
    assert numpy.allclose(points[:, 0], 1)

    control, affine, warping = parameters

    assert affine.shape[0] == affine.shape[1]
    assert warping.shape == control.shape

    kernel = scipy.spatial.distance.cdist(points, control)
    kernel = numpy.power(kernel, 2) * numpy.log(kernel, where=kernel > 0)
    kernel[numpy.where(numpy.isnan(kernel))] = 0

    assert numpy.all(numpy.isfinite(kernel))

    points_1 = numpy.dot(points, affine) + numpy.dot(kernel, warping)

    assert numpy.all(numpy.isfinite(points_1))

    return points_1


def transform_tps_euclidean(points, parameters):
    homogeneous = numpy.ones((points.shape[0], points.shape[1] + 1))
    homogeneous[:, 1:] = points
    homogeneous = chui_transform(homogeneous, parameters)
    return homogeneous[:, 1:]


def chui_minimize(V, Y, l):
    # FIXME: Rename this function to something more sensible
    # TODO: Document lacking regularization on affine transformation.

    assert l >= 0
    assert V.shape == Y.shape
    assert numpy.allclose(V[:, 0], 1)
    assert numpy.allclose(Y[:, 0], 1)

    k, m = V.shape

    I = numpy.identity(k - m)

    phi = scipy.spatial.distance.pdist(V)
    phi = scipy.spatial.distance.squareform(phi)
    phi = numpy.power(phi, 2) * numpy.log(phi, where=phi > 0)
    phi[numpy.where(numpy.isnan(phi))] = 0

    assert phi.shape == (k, k)
    assert numpy.all(numpy.isfinite(phi))

    # If R is left untouched, it cannot be inverted later.
    # In Chui's .m code, however, R is sliced down to m,
    # i.e. the point's dimensions. This is not explained in the paper,
    # the formula shows R to be in the upper left of a matrix expression.

    Q, R = numpy.linalg.qr(V, mode="complete")
    R = R[:m, :m]
    Q1, Q2 = Q[:, :m], Q[:, m:]

    assert Q1.shape == (k, m)
    assert Q2.shape == (k, k - m)

    w_hat = Q2 @ numpy.linalg.inv(Q2.T @ phi @ Q2 + l * I) @ Q2.T @ Y

    assert w_hat.shape == (k, m)

    # In the paper it is stated R^-1 (Q1' V - K w), however the dimensions
    # of the matrices, i.e. (k, m) (m, k) - (k, k) (k, m),
    # are not compatible this way. Further, the weighted assignment Y has
    # no influence on the rigid transformation.
    # In his code, Chui uses a similar formulation
    # where the parenthesis is moved once left and Y is used instead of V
    # Thus, I assume the parenthesis placement and the usage of V
    # in the paper are typos and incorrect.

    R_inv = numpy.linalg.inv(R)
    d_hat = R_inv @ Q1.T @ (Y - phi @ w_hat)

    assert d_hat.shape == (m, m)

    assert numpy.all(numpy.isfinite(d_hat))
    assert numpy.all(numpy.isfinite(w_hat))

    return Transform(V, d_hat, w_hat)


def fit_tps(points1, points2, rigidity):
    # FIXME: Rename this function to something more sensible
    ones1 = numpy.ones((points1.shape[0],))
    homog1 = numpy.stack((ones1, points1[:, 1], points1[:, 0]), axis=1)

    ones2 = numpy.ones((points2.shape[0],))
    homog2 = numpy.stack((ones2, points2[:, 1], points2[:, 0]), axis=1)

    transform = chui_minimize(homog1, homog2, rigidity)

    return transform


def apply_tps_transform(transform, image):
    # FIXME: Rename this function to something more sensible
    # FIXME: Grid border extents are not dependant on image shape
    # FIXME: Grid is very coarse
    vertical = numpy.linspace(-500, image.shape[0] + 500, 100)
    horizontal = numpy.linspace(-500, image.shape[1] + 500, 100)
    from_grid = numpy.meshgrid(vertical, horizontal, sparse=False)
    from_grid = numpy.stack((numpy.ones((100 * 100,)),
                             numpy.ravel(from_grid[0]),
                             numpy.ravel(from_grid[1])), axis=1)

    to_grid = chui_transform(from_grid, transform)

    piecewise = skimage.transform.PiecewiseAffineTransform()

    assert piecewise.estimate(to_grid[:, [1, 2]], from_grid[:, [1, 2]])

    # TODO: Add piecewise model to pipeline to transform originals

    image = skimage.transform.warp(image, piecewise, mode='reflect')

    return image
