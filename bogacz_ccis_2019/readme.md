Cite as:
```
@inproceedings {Bogacz:CCIS2020,
    booktitle = {Communications in Computer and Information Science},
    title = {{Quantifying Deformation in Aegean Sealing Practices}},
    author = {Bogacz, Bartosz and Finlayson, Sarah and Panagiotopolous, Diamantis and Mara, Hubert},
    year = {2020},
}
```
