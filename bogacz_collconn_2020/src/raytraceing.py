import struct
import os.path

import numpy
import matplotlib.pyplot


def load_ply(filename):
    with open(filename, "rb") as f:

        line = f.readline()
        assert line == b'ply\n'

        line = f.readline()
        assert line == b'format binary_little_endian 1.0\n'

        group = None
        parts = None
        elements = []

        while True:
            line = f.readline()

            if line == b'end_header\n':
                break

            if line.startswith(b'comment'):
                continue

            if line.startswith(b'element vertex '):
                if group is not None:
                    elements.append((group, parts, count))
                group, parts = "vertex", []
                count = int(line[len(b'element vertex '):])

            if line.startswith(b'element face '):
                if group is not None:
                    elements.append((group, parts, count))
                group, parts = "face", []
                count = int(line[len(b'element face '):])

            if line.startswith(b'property float '):
                ident = line[len(b'property float '):-1].decode()
                parts.append((ident, "<f", None))

            if line.startswith(b'property int '):
                ident = line[len(b'property int '):-1].decode()
                parts.append((ident, "<i", None))

            if line.startswith(b'property uint8 '):
                ident = line[len(b'property uint8 '):-1].decode()
                parts.append((ident, "<B", None))

            if line.startswith(b'property uint32 '):
                ident = line[len(b'property uint32 '):-1].decode()
                parts.append((ident, "<I", None))

            if line.startswith(b'property list uint8 float '):
                ident = line[len(b'property list uint8 float '):-1].decode()
                parts.append((ident, "<B", "<f"))

            if line.startswith(b'property list uchar int32 '):
                ident = line[len(b'property list uchar int32 '):-1].decode()
                parts.append((ident, "<B", "<i"))

        if group is not None:
            elements.append((group, parts, count))

        result = {part[0]: [] for group, parts, _ in elements for part in parts}

        for group, parts, count in elements:
            for n in range(count):
                for ident, fst, snd in parts:
                    if snd is None:
                        buf = f.read(struct.calcsize(fst))
                        val = struct.unpack(fst, buf)[0]
                        result[ident].append(val)
                    else:
                        buf = f.read(struct.calcsize(fst))
                        count = struct.unpack(fst, buf)[0]
                        buf = f.read(struct.calcsize(snd) * count)
                        val = [x[0] for x in struct.iter_unpack(snd, buf)]
                        result[ident].append(val)

        return result


def ortho_proj(height, width, vertices):
    origins = numpy.meshgrid(
        numpy.linspace(vertices[:, 0].min(), vertices[:, 0].max(), num=height),
        numpy.linspace(vertices[:, 1].max(), vertices[:, 1].min(), num=width))
    origins = numpy.column_stack((
        origins[0].ravel(), origins[1].ravel(),
        numpy.ones(height * width) * vertices[:, 2].min()))
    directions = numpy.zeros((height * width, 3))
    directions[:, 2] = 1

    return origins, directions


def viz_ortho_proj(axes, vertices, faces):
    triangles = vertices[faces].reshape(-1, 3)
    origins, directions = ortho_proj(100, 100, triangles)

    axes.scatter(origins[:, 0], origins[:, 1], c="lightgray", s=1)
    axes.plot(
        vertices[faces[:, [0, 1, 2, 0]], 0].transpose(),
        vertices[faces[:, [0, 1, 2, 0]], 1].transpose())

    return origins, directions


def moller_trumbore(origins, directions, triangles):
    assert triangles.shape[0] == origins.shape[0]
    assert triangles.shape[1] == 3
    assert triangles.shape[2] == origins.shape[1]
    assert origins.shape[0] == directions.shape[0]

    eps = 0.0000001
    edges1 = triangles[:, 1, :] - triangles[:, 0, :]
    edges2 = triangles[:, 2, :] - triangles[:, 0, :]
    h = numpy.cross(directions, edges2, axis=1)
    a = numpy.einsum("ab,ab->a", edges1, h)
    mask = numpy.logical_or(a < -eps, a > eps)
    f = 1 / a
    s = origins - triangles[:, 0]
    u = f * numpy.einsum("ab,ab->a", s, h)
    mask = numpy.logical_and(mask, numpy.logical_and(u > 0, u < 1))
    q = numpy.cross(s, edges1)
    v = f * numpy.einsum("ab,ab->a", directions, q)
    mask = numpy.logical_and(mask, numpy.logical_and(v > 0, (u + v) < 1))
    t = f * numpy.einsum("ab,ab->a", edges2, q)
    mask = numpy.logical_and(mask, t > eps)
    coords = numpy.column_stack((u, v, 1 - u - v))

    return mask, coords, t


def viz_moller_trumbore(axes, vertices, faces, origins, directions):
    triangles_rep = numpy.repeat(vertices[faces], origins.shape[0], axis=0)
    origins_rep = numpy.tile(origins, (faces.shape[0], 1))
    directions_rep = numpy.tile(directions, (faces.shape[0], 1))

    mask, uvw, t = moller_trumbore(origins_rep, directions_rep, triangles_rep)

    HEIGHT, WIDTH = 100, 100
    ii, jj, kk = numpy.nonzero(mask.reshape(faces.shape[0], HEIGHT, WIDTH))
    tt = t.reshape(faces.shape[0], HEIGHT, WIDTH)[ii, jj, kk]
    zz = numpy.argsort(tt)
    ii, jj, kk = ii[zz], jj[zz], kk[zz]
    image = numpy.zeros((HEIGHT, WIDTH, 3))
    image[jj, kk] = uvw.reshape(faces.shape[0], HEIGHT, WIDTH, 3)[ii, jj, kk]
    #image = numpy.zeros((HEIGHT, WIDTH))
    #image[jj, kk] = tt[zz]
    axes.imshow(image)


def build_bvh(vertices, faces):
    centers = numpy.mean(vertices[faces], axis=1)
    levels = int(numpy.ceil(numpy.log2(centers.shape[0])))
    valid = numpy.ones((2 ** levels,), dtype=bool)
    indices = numpy.arange(2 ** levels, dtype=int)
    valid[indices >= centers.shape[0]] = False
    indices[indices >= centers.shape[0]] -= centers.shape[0]
    tree = []

    indices = indices.reshape((1, 2 ** levels))

    for _ in range(levels):
        print(indices.shape)

        splits = centers[indices]
        boxes = numpy.array([
            [splits[:, :, 0].min(axis=1), splits[:, :, 0].max(axis=1)],
            [splits[:, :, 1].min(axis=1), splits[:, :, 1].max(axis=1)],
            [splits[:, :, 2].min(axis=1), splits[:, :, 2].max(axis=1)]])

        extents = boxes[:, 1, :] - boxes[:, 0, :]
        axes = extents.argmax(axis=0)
        join = numpy.arange(axes.shape[0])
        indices2 = numpy.argsort(splits[join, :, axes], axis=1)
        join2 = numpy.tile(join, (indices2.shape[1], 1)).transpose()
        indices = indices[join2, indices2].reshape(
            (indices.shape[0] * 2,
             indices.shape[1] // 2))

        triangle_boxes = numpy.array([
            vertices[faces[indices], :].min(axis=2).min(axis=1),
            vertices[faces[indices], :].max(axis=2).max(axis=1)])
        triangle_boxes = numpy.transpose(triangle_boxes, (1, 0, 2))
        tree.append(triangle_boxes)

    return tree, indices, valid


def viz_build_bhv(axes, vertices, faces):
    tree, indices, valid = build_bvh(vertices, faces)
    boxes = tree[-1]
    axes.plot(
        vertices[faces[:, [0, 1, 2, 0]], 0].transpose(),
        vertices[faces[:, [0, 1, 2, 0]], 1].transpose())
    axes.plot(boxes[:, [0, 1, 1, 0, 0], 0].transpose(),
              boxes[:, [0, 0, 1, 1, 0], 1].transpose())
    return tree, indices, valid


def traverse_bvh(tree, origins, directions):
    oo_rep = numpy.arange(origins.shape[0])
    bb_rep = numpy.arange(tree[0].shape[0])

    oo_rep = numpy.repeat(oo_rep, tree[0].shape[0], axis=0)
    bb_rep = numpy.tile(bb_rep, (origins.shape[0],))

    for boxes in tree:
        oo = oo_rep
        bb = bb_rep

        dx = directions[oo, 0]
        dy = directions[oo, 1]
        dz = directions[oo, 2]

        xz, xnz = dx == 0, dx != 0
        yz, ynz = dy == 0, dy != 0
        zz, znz = dz == 0, dz != 0

        txmin = boxes[bb, 0, 0] - origins[oo, 0]
        txmin[xz] = numpy.where(txmin[xz] > 0, -numpy.inf, +numpy.inf)
        numpy.divide(txmin, dx, out=txmin, where=xnz)
        txmax = boxes[bb, 1, 0] - origins[oo, 0]
        txmax[xz] = numpy.where(txmax[xz] > 0, -numpy.inf, +numpy.inf)
        numpy.divide(txmax, dx, out=txmax, where=xnz)
        tmin = numpy.minimum(txmin, txmax)
        tmax = numpy.maximum(txmin, txmax)

        tymin = boxes[bb, 0, 1] - origins[oo, 1]
        tymin[yz] = numpy.where(tymin[yz] > 0, -numpy.inf, +numpy.inf)
        numpy.divide(tymin, dy, out=tymin, where=ynz)
        tymax = boxes[bb, 1, 1] - origins[oo, 1]
        tymax[yz] = numpy.where(tymax[yz] > 0, -numpy.inf, +numpy.inf)
        numpy.divide(tymax, dy, out=tymax, where=xnz)
        tmin = numpy.maximum(tmin, numpy.minimum(tymin, tymax))
        tmax = numpy.minimum(tmax, numpy.maximum(tymin, tymax))

        tzmin = boxes[bb, 0, 2] - origins[oo, 2]
        tzmin[zz] = numpy.where(tzmin[zz] > 0, -numpy.inf, +numpy.inf)
        numpy.divide(tzmin, dz, out=tzmin, where=znz)
        tzmax = boxes[bb, 1, 2] - origins[oo, 2]
        tzmax[zz] = numpy.where(tzmax[zz] > 0, -numpy.inf, +numpy.inf)
        numpy.divide(tzmax, dz, out=tzmax, where=znz)
        tmin = numpy.maximum(tmin, numpy.minimum(tzmin, tzmax))
        tmax = numpy.minimum(tmax, numpy.maximum(tzmin, tzmax))

        hits = tmin < tmax

        oo_rep = numpy.repeat(oo[hits], 2, axis=0)
        bb_rep = numpy.column_stack((bb[hits] * 2, bb[hits] * 2 + 1)).ravel()

        print(oo.shape)

    return oo[hits], bb[hits]


def viz_traverse_bhv(axes, vertices, faces, tree, origins, directions):
    oo, bb = traverse_bvh(tree, origins, directions)
    axes.plot(tree[-1][:, [0, 1, 1, 0, 0], 0].transpose(),
              tree[-1][:, [0, 0, 1, 1, 0], 1].transpose())
    axes.scatter(origins[:, 0],
                 origins[:, 1],
                 c="lightgray", s=1)
    axes.scatter(origins[oo, 0],
                 origins[oo, 1],
                 c="red", s=1)
    axes.plot(
        vertices[faces[:, [0, 1, 2, 0]], 0].transpose(),
        vertices[faces[:, [0, 1, 2, 0]], 1].transpose())

    return oo, bb


def render(vertices, faces, msii, height, width):
    triangles = vertices[faces].reshape(-1, 3)
    origins, directions = ortho_proj(height, width, triangles)
    tree, indices, valid = build_bvh(vertices, faces)
    oo, bb = traverse_bvh(tree, origins, directions)
    indices = indices.reshape(tree[-1].shape[0], -1)
    oo_rep = numpy.repeat(oo, indices.shape[1], axis=0)
    ff = faces[indices[bb]].reshape(oo_rep.shape[0], 3)
    mask, uvw, t = moller_trumbore(
        origins[oo_rep], directions[oo_rep], vertices[ff])
    oo_rep, ff, uvw, t = oo_rep[mask], ff[mask], uvw[mask], t[mask]
    jj, kk = numpy.unravel_index(oo_rep, (height, width))
    zz = numpy.argsort(t)
    image = numpy.zeros((height, width))
    image[jj[zz], kk[zz]] = \
        numpy.einsum("ab,ab->a", msii[ff[zz], 0], uvw[zz])
    return image


def viz_render(axes, vertices, faces, msii, height, width):
    image = render(vertices, faces, msii, height, width)
    axes.imshow(image)


def main():
    PLY_FILENAME = "data/CMS-II6-no101_HM-452-30_2019-05-18" \
        "_GMCFO_r1.00_n4_v256.volume.ply"

    ply = load_ply(os.path.expanduser(PLY_FILENAME))
    print(list(ply.keys()))

    vertices_viz = numpy.column_stack((ply["x"], ply["y"], ply["z"]))
    faces_viz = numpy.array(ply["vertex_indices"])[:3, :]
    msii_viz = numpy.array(ply["feature_vector"])
    print("vertices_viz.shape =", vertices_viz.shape,
          "faces_viz.shape =", faces_viz.shape,
          "msii_viz.shape =", msii_viz.shape)

    figure, axes = matplotlib.pyplot.subplots(1, 1, figsize=(10, 10), dpi=100)
    origins_viz, directions_viz = viz_ortho_proj(axes, vertices_viz, faces_viz)
    print("origins_viz.shape =", origins_viz.shape,
          "directions_viz.shape =", directions_viz.shape)
    figure.savefig("plots/orthogonal_projection.png")

    figure, axes = matplotlib.pyplot.subplots(1, 1, figsize=(10, 10), dpi=100)
    viz_moller_trumbore(
        axes, vertices_viz, faces_viz, origins_viz, directions_viz)
    figure.savefig("plots/moller_trumbore.png")

    figure, axes = matplotlib.pyplot.subplots(1, 1, figsize=(10, 10), dpi=100)
    tree_viz, indices_viz, valid_viz = viz_build_bhv(axes, vertices_viz, faces_viz)
    print("len(tree_viz) =", len(tree_viz),
          "indices_viz.shape =", indices_viz.shape,
          "valid_viz.sum() =", valid_viz.sum())
    figure.savefig("plots/build_bounding_volume_hierarchy.png")

    figure, axes = matplotlib.pyplot.subplots(1, 1, figsize=(10, 10), dpi=100)
    oo_viz, bb_viz = viz_traverse_bhv(
        axes, vertices_viz, faces_viz, tree_viz, origins_viz, directions_viz)
    print("oo_viz.shape =", oo_viz.shape)
    figure.savefig("plots/traverse_bounding_volume_hierarchy.png")

    figure, axes = matplotlib.pyplot.subplots(1, 1, figsize=(10, 10), dpi=100)
    viz_render(axes, vertices_viz, faces_viz, msii_viz, 1000, 1000)
    figure.savefig("plots/test_rendering_1.png")

    ply = load_ply(os.path.expanduser(PLY_FILENAME))
    vertices = numpy.column_stack((ply["x"], ply["y"], ply["z"]))
    faces = numpy.array(ply["vertex_indices"])
    msii = numpy.array(ply["feature_vector"])

    image = render(vertices, faces, msii, 1000, 1000)

    figure, axes = matplotlib.pyplot.subplots(1, 1, figsize=(15, 15), dpi=100)
    axes.imshow(image)
    figure.savefig("plots/test_rendering_2.png")


if __name__ == "__main__":
    main()
