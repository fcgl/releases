import numpy

import scipy.spatial.distance

import skimage.io
import skimage.util
import skimage.color
import skimage.feature
import skimage.transform

import sklearn.svm
import sklearn.neighbors
import sklearn.decomposition

import matplotlib.pyplot


class Experiment:
    def __init__(self, names, files):
        self.names = names
        self.files = files

    def load_images(self, *, cut=0.2):
        images = []

        for fn in self.files:
            im = skimage.io.imread(fn)
            im = skimage.color.rgb2gray(skimage.color.rgba2rgb(im))
            im = im[
                int(im.shape[0] * cut):int(im.shape[0] * (1 - cut)),
                int(im.shape[1] * cut):int(im.shape[1] * (1 - cut))]
            images.append(im)

        self.images = images

    def show_images(self):
        figure, axes = matplotlib.pyplot.subplots(
            1, len(self.images), figsize=(15, 5), dpi=90)

        for ax, nm, im in zip(axes, self.names, self.images):
            ax.imshow(im)
            ax.set_title(nm)

        figure.tight_layout()
        return figure

    # For some visualizations its much easier to see a difference if
    # we split foreground from background. We use a simple threshold,
    # a smarter would be to use adaptive binarization methods.

    def whiten_images(self, *, above=0.3):
        binaries = []
        for im in self.images:
            wt = numpy.copy(im)
            wt -= numpy.mean(wt)
            wt /= numpy.std(wt)
            bi = wt > above
            binaries.append(bi)

        self.binaries = binaries

    def show_threshold(self):
        figure, axes = matplotlib.pyplot.subplots(
            1, len(self.images), figsize=(15, 5), dpi=90)

        for ax, nm, bi in zip(axes, self.names, self.binaries):
            ax.imshow(bi)
            ax.set_title(nm)

        figure.tight_layout()
        return figure

    # We use a keypoint based alignment method. That is, we align
    # visually similar keypoints (descriptors) from both images
    # to each other. For that, we need to extract these keypoint,
    # or visual descriptors. Our method of choice is DAISY.

    def extract_daisy(self, *, step=5, radius=30, rings=5):
        descriptors = []
        shapes = []
        coordinates = []

        for im in self.images:
            desc = skimage.feature.daisy(im,
                step=step, radius=radius, rings=rings)

            shp = (desc.shape[0], desc.shape[1])

            desc = desc.reshape(
                desc.shape[0] * desc.shape[1], desc.shape[2])


            # Reverse engineer samples on image at which DAISY
            # descriptors have been extracted.

            # DAISY as implemented by skimage generates keysamples
            # row major, i.e., x y
            coord_x, coord_y = numpy.meshgrid(
                numpy.linspace(radius, im.shape[1] - radius,
                   int(numpy.ceil((im.shape[1] - radius * 2) / step)),
                   dtype=int),
                numpy.linspace(radius, im.shape[0] - radius,
                   int(numpy.ceil((im.shape[0] - radius * 2) / step)),
                   dtype=int),
                sparse=False)

            coord_x = numpy.ravel(coord_x)
            coord_y = numpy.ravel(coord_y)

            coord = numpy.stack((coord_x, coord_y), axis=1)

            assert coord.shape[0] == desc.shape[0]

            descriptors.append(desc)
            shapes.append(shp)
            coordinates.append(coord)

        self.descriptors = descriptors
        self.shapes = shapes
        self.coordinates = coordinates

    # The descriptors themselves may be large.
    # We show the points were the descriprtors were extracrted from
    # the images to get an idea how much border we "lose".

    def show_daisy(self):
        figure, axes = matplotlib.pyplot.subplots(
            1, len(self.images), figsize=(15, 5), dpi=90)

        for ax, nm, im, cd in \
                zip(axes, self.names, self.images, self.coordinates):
            ax.imshow(im)
            ax.scatter(cd[:, 0], cd[:, 1], s=1, color="white")
            ax.set_title(nm)

        figure.tight_layout()
        return figure

    # Visual features (descriptors) that repeat often are not very descriptive.
    # They will be often sources and targets of others since they are so
    # common. Therefore, we compute the probability of an descriptor occuring
    # and remove any descriptors that are _more_ common than some threshold.
    # Effectively removing non-informative keypoints.

    # Also it severly reduces the computational impact of the method.
    # Consider that the assignment computation is `O(n^2)` with `n` the
    # count of remaining features. Reducing these here by a factor of
    # two reduced the count of assignments by four.

    def estimate_density(self, *, below=50):
        joined = []
        for desc in self.descriptors:
            subset = numpy.random.randint(0, desc.shape[0], size=(2000,))
            joined.append(desc[subset])
        joined = numpy.concatenate(joined, axis=0)

        embedding = sklearn.decomposition.PCA(n_components=16)
        embedding.fit(joined)

        density = sklearn.neighbors.KernelDensity(bandwidth=0.1)
        density.fit(embedding.transform(joined))

        features = []
        uncommon = []

        for desc in self.descriptors:
            feat = embedding.transform(desc)
            prob = density.score_samples(feat)
            uniq = prob < numpy.percentile(prob, below)

            features.append(feat)
            uncommon.append(uniq)

        self.features = features
        self.uncommon = uncommon

    # The removal will be seen as holes in the images. We want the remaining
    # descriptors to stay close to semantically meaningful visuals, e.g.
    # motifs on sealing images or borders of signs. Here, SVG files would make
    # our life much easier, as we would have ground-truth outlines.

    def show_density(self):
        figure, axes = matplotlib.pyplot.subplots(
            1, len(self.images), figsize=(15, 5), dpi=90)

        for ax, nm, im, cd, uc in \
                zip(axes, self.names, self.images,
                    self.coordinates, self.uncommon):
            ax.imshow(im)
            ax.scatter(cd[uc, 0], cd[uc, 1], s=1, color="white")
            ax.set_title(nm)

        figure.tight_layout()
        return figure

    # Given un-common keypoints on two images, we find connections (assignments)
    # between both sides that match descriptors of similar visual content.
    # We also make sure that the best target for a source is also the best
    # source for the target. That is, that finding assignments backwards
    # results in the same connections. We also penalize connections that
    # are especially long, those might be flukes.

    # We define two parameters, a weight how much distance should be
    # penalized `w` and the radius of pixel that a backwards connection is
    # allowed to miss the source `r`.

    def assign_features(self, *, w=0.3, r=2):
        assert 0 < r < 1000
        assert 0 < w < 1

        self.feat_hists = {}
        self.spat_hists = {}
        self.penalty_hists = {}
        self.close_hists = {}

        assignments = {}
        values = {}

        for i in range(len(self.features)):
            for j in range(len(self.features)):
                if i == j:
                    continue

                ui, uj = self.uncommon[i], self.uncommon[j]
                ci, cj = self.coordinates[i][ui], self.coordinates[j][uj]
                fi, fj = self.features[i][ui], self.features[j][uj]

                feat = fi[:, None, :] - fj[None, :, :]
                feat = numpy.linalg.norm(feat, axis=2)

                self.feat_hists[(i, j)] = (feat.min(), feat.mean(), feat.max())

                spat = ci[:, None, :] - cj[None, :, :]
                spat = numpy.linalg.norm(spat, axis=2)

                self.spat_hists[(i, j)] = (spat.min(), spat.mean(), spat.max())

                feat /= feat.max()
                spat /= spat.max()

                penalty = (1 - w) * feat + w * spat

                self.penalty_hists[(i, j)] = \
                    (penalty.min(), penalty.mean(), penalty.max())

                ii = numpy.arange(0, penalty.shape[0], dtype=int)
                ij = numpy.argmin(penalty, axis=1)
                ji = numpy.argmin(penalty, axis=0)

                close = numpy.linalg.norm(ci[ii] - ci[ji[ij]], axis=1)

                self.close_hists[(i, j)] = \
                    (close.min(), close.mean(), close.max())

                close = close < r
                ii = ii[close]
                jj = ij[close]

                assig = numpy.concatenate((ci[ii], cj[jj]), axis=1)
                val = penalty[ii, jj]

                assignments[(i, j)] = assig
                values[(i, j)] = val

        self.assignments = assignments
        self.values = values

    # The back-alignment require culls allowable connections further,
    # in addition to the density requirement. We show the alignments
    # that passed our requirements. Even though we require alignments
    # to work both ways, the process still is not symmetric, e.g.
    # because of our allowed back-alignment miss. Therefore alignments
    # are shown in a matrix, with the alignment process being from column
    # to row. That is the images in the column header are aligned to
    # the images in the row header.

    def show_assignments(self):
        figure, axes = matplotlib.pyplot.subplots(
            len(self.images) + 1, len(self.images) + 1,
            figsize=(20, 20), dpi=90)

        axes[0, 0].set_axis_off()

        for i in range(len(self.names)):
            for j in range(len(self.names)):
                if i == j:
                    axes[i+1, j+1].set_axis_off()
                    continue

                ci, ui = self.coordinates[i], self.uncommon[i]
                cj, uj = self.coordinates[j], self.uncommon[j]
                assig = self.assignments[(i, j)]

                axes[i+1, j+1].imshow(self.images[i])
                axes[i+1, j+1].quiver(
                    assig[:, 0], assig[:, 1],
                    assig[:, 2] - assig[:, 0],
                    assig[:, 3] - assig[:, 1],
                    angles="xy", scale_units="xy",
                    scale=1, width=0.005, color="white")
                axes[i+1, j+1].set_axis_off()

        for i in range(len(self.names)):
            axes[i+1, 0].set_title(self.names[i])
            axes[i+1, 0].set_axis_off()
            axes[i+1, 0].imshow(self.images[i])
            axes[0, i+1].set_title(self.names[i])
            axes[0, i+1].set_axis_off()
            axes[0, i+1].imshow(self.images[i])

        figure.tight_layout()
        return figure

    # The assignments contain some amount of outliers and are spotty,
    # i.e. not equally distributed over the image domain. We consider
    # the alignments `x, y, x', y'` as a manifold in four dimensional
    # space. If we smooth this manifold we remove outliers and we
    # can generate new assignments by interpolation. For this we
    # use two SVRs each predicting `x, y -> x'` and `x, y -> y'`.

    def regress_manifold(self, *, c=50, gamma=0.00001):
        regressors = {}

        for i in range(len(self.names)):
            for j in range(len(self.names)):
                if i == j:
                    continue

                assig = self.assignments[(i, j)]

                reg0 = sklearn.svm.SVR(C=c, gamma=gamma, epsilon=0)
                reg0.fit(assig[:, [0, 1]], assig[:, 2])

                reg1 = sklearn.svm.SVR(C=c, gamma=gamma, epsilon=0)
                reg1.fit(assig[:, [0, 1]], assig[:, 3])

                regressors[(i, j)] = (reg0, reg1)

        self.regressors = regressors

    # Then, we can easily regress or interpolate new smoothed
    # assignments from the predicted manifold.

    # Blue dots and arrows depict original assignments, yellow dots and
    # arrows depict regressed assignments.

    def show_regression(self, *, i=0, j=1):
        assig = self.assignments[(i, j)]
        nm = self.names[i]
        im = self.images[i]
        reg0, reg1 = self.regressors[(i, j)]

        figure, axes = matplotlib.pyplot.subplots(
            2, 2, figsize=(10, 10), dpi=90)

        vertical = numpy.linspace(
            im.shape[0] * 0.1, im.shape[0] * 0.9, 20)
        horizontal = numpy.linspace(
            im.shape[1] * 0.1, im.shape[1] * 0.9, 20)
        grid = numpy.meshgrid(horizontal, vertical, sparse=False)
        grid = numpy.stack(
            (numpy.ravel(grid[0]), numpy.ravel(grid[1])), axis=1)

        axes[0, 0].imshow(im, cmap="gray")
        axes[0, 0].quiver(
            assig[:, 0], assig[:, 1],
            assig[:, 2] - assig[:, 0],
            assig[:, 3] - assig[:, 1],
            angles="xy", scale_units="xy",
            scale=1, width=0.003, color="royalblue")
        axes[0, 0].set_title(nm)

        axes[0, 1].imshow(im, cmap="gray")
        axes[0, 1].quiver(
            grid[:, 0], grid[:, 1],
            reg0.predict(grid) - grid[:, 0],
            reg1.predict(grid) - grid[:, 1],
            angles="xy", scale_units="xy",
            scale=1, width=0.005, color="darkorange")
        axes[0, 1].set_title(nm)

        axes[1, 0].scatter(assig[:, 0], assig[:, 2],
                           s=5, color="royalblue")
        axes[1, 0].scatter(grid[:, 0], reg0.predict(grid),
                           s=10, color="darkorange")
        axes[1, 0].set_title("Regression of X' given X and Y")
        axes[1, 0].set_ylabel("Source X")
        axes[1, 0].set_xlabel("Target X'")

        axes[1, 1].scatter(assig[:, 1], assig[:, 3],
                           s=5, color="royalblue")
        axes[1, 1].scatter(grid[:, 1], reg1.predict(grid),
                           s=10, color="darkorange")
        axes[1, 1].set_title("Regression of Y' given X and Y")
        axes[1, 1].set_ylabel("Source Y")
        axes[1, 1].set_xlabel("Target Y'")

        figure.tight_layout()
        return figure

    # Finally, what is left is to actually transform the images given
    # our computed manifold. For this, we sample a `20 x 20` regular
    # grid on the manifold for the image domain and use a piecewise
    # linear interpolation for the pixels.

    def warp_images(self):
        grids_orig = {}
        grids_affine = {}
        images_affine = {}
        binaries_affine = {}
        grids_warped = {}
        images_warped = {}
        binaries_warped = {}

        for i in range(len(self.images)):
            for j in range(len(self.images)):
                if i == j:
                    continue

                vertical = numpy.linspace(
                    self.images[i].shape[0] * 0.01,
                    self.images[i].shape[0] * 0.99, 30)
                horizontal = numpy.linspace(
                    self.images[i].shape[1] * 0.01,
                    self.images[i].shape[1] * 0.99, 30)
                from_grid = numpy.meshgrid(
                    horizontal, vertical, sparse=False)
                from_grid = numpy.stack((
                    numpy.ravel(from_grid[0]),
                    numpy.ravel(from_grid[1])),
                    axis=1)

                reg0, reg1 = self.regressors[(i, j)]

                to_grid = numpy.stack((
                    reg0.predict(from_grid),
                    reg1.predict(from_grid)),
                    axis=1)

                grids_orig[(i, j)] = from_grid

                affine = skimage.transform.AffineTransform()
                assert affine.estimate(from_grid, to_grid)
                grids_affine[(i, j)] = affine(from_grid)
                samples = skimage.transform.warp_coords(
                    affine.inverse, self.images[j].shape)
                images_affine[(i, j)] = scipy.ndimage.map_coordinates(
                    self.images[i], samples, order=0)
                binaries_affine[(i, j)] = scipy.ndimage.map_coordinates(
                    self.binaries[i], samples, order=0)

                piecewise = skimage.transform.PiecewiseAffineTransform()
                assert piecewise.estimate(from_grid, to_grid)
                grids_warped[(i, j)] = piecewise(from_grid)
                samples = skimage.transform.warp_coords(
                    piecewise.inverse, self.images[j].shape)
                images_warped[(i, j)] = scipy.ndimage.map_coordinates(
                    self.images[i], samples, order=0)
                binaries_warped[(i, j)] = scipy.ndimage.map_coordinates(
                    self.binaries[i], samples, order=0)

        self.grids_orig = grids_orig
        self.grids_affine = grids_affine
        self.grids_warped = grids_warped
        self.images_affine = images_affine
        self.images_warped = images_warped
        self.binaries_affine = binaries_affine
        self.binaries_warped = binaries_warped

    # We display the applied transformation to a source image
    # by overlaying it atop the un-transformed target image. Therefore,
    # one can instantly compare how similar they are. Additionaly, we also
    # show the applied non-rigid deformation to the source image.

    # This performed by first computing a affine transform, given the
    # samples from manifold, and comparing it to the warped transform.
    # Then, we draw arrows to show this difference.

    # By overlaying the foreground of both source and target we
    # easily compare any differences.

    def show_overlay(self, *, binaries=True, quiver=True):
        figure, axes = matplotlib.pyplot.subplots(
            len(self.images) + 1, len(self.images) + 1,
            figsize=(20, 20), dpi=90)

        axes[0, 0].set_axis_off()

        for i in range(len(self.images)):
            for j in range(len(self.images)):
                if i == j:
                    axes[i+1, j+1].set_axis_off()
                    continue

                ga = self.grids_affine[(i, j)]
                gw = self.grids_warped[(i, j)]

                if binaries:
                    overlay = self.binaries_warped[(i, j)].astype(int) + \
                        self.binaries[j].astype(int)
                else:
                    overlay = self.images_warped[(i, j)] + self.images[j]

                axes[i+1, j+1].set_axis_off()
                axes[i+1, j+1].set_ylim(overlay.shape[0], 0)
                axes[i+1, j+1].set_xlim(0, overlay.shape[1])
                axes[i+1, j+1].imshow(overlay)

                if quiver:
                    axes[i+1, j+1].quiver(
                        ga[:, 0], ga[:, 1],
                        gw[:, 0] - ga[:, 0],
                        gw[:, 1] - ga[:, 1],
                        angles="xy", scale_units="xy",
                        scale=1, width=0.005, color="white")

        for i in range(len(self.names)):
            axes[i+1, 0].set_title(self.names[i])
            axes[i+1, 0].set_axis_off()
            axes[i+1, 0].imshow(self.images[i])
            axes[0, i+1].set_title(self.names[i])
            axes[0, i+1].set_axis_off()
            axes[0, i+1].imshow(self.images[i])

        figure.tight_layout()
        return figure

    # The transformation of the source to match the target can be performed both
    # rigidly and non-rigidly. Here we show the difference, summing binary
    # images of transformed sources that match a single target, for both affine
    # and warping transformations.

    # Additionally, this view allows us to find a prototype. That is an image
    # that all other images align particularly well to.

    def show_alignment(self, *, binaries=True):
        figure, axes = matplotlib.pyplot.subplots(
            2, len(self.names),
            figsize=(15, 7), dpi=90)

        for j in range(len(self.names)):
            if binaries:
                acc_a = self.binaries[j].copy().astype(float)
                acc_w = self.binaries[j].copy().astype(float)
            else:
                acc_a = self.images[j].copy().astype(float)
                acc_w = self.images[j].copy().astype(float)

            for i in range(len(self.names)):
                if i == j:
                    continue

                if binaries:
                    acc_a += self.binaries_affine[(i, j)]
                    acc_w += self.binaries_warped[(i, j)]
                else:
                    acc_a += self.images_affine[(i, j)]
                    acc_w += self.images_warped[(i, j)]

            axes[0, j].imshow(acc_a)
            axes[0, j].set_axis_off()
            axes[0, j].set_title(self.names[j])
            axes[1, j].imshow(acc_w)
            axes[1, j].set_axis_off()

        figure.suptitle("Affine (top) vs warped (bottom)")
        figure.tight_layout()
        return figure

    # Similarity Matrix

    # Finally, a quantitative plot gives us the similarity between all studied
    # images. Here, we can find subsets that very similar to each other or
    # find outliers that are dissimilar to all others.

    def show_matrix(self):
        figure, axes = matplotlib.pyplot.subplots(
            1, 1, figsize=(7, 7), dpi=90)

        agreement = numpy.zeros((len(self.names), len(self.names)))

        for i in range(len(self.names)):
            for j in range(len(self.names)):
                if i == j:
                    continue

                imi = self.images_warped[(i, j)]
                imj = self.binaries_warped[(i, j)]

                a, b = imi.shape
                c, d = imj.shape
                e, f = max(a, c), max(b, d)
                overlay = numpy.zeros((e, f))
                overlay[0:imi.shape[0], 0:imi.shape[1]] += imi
                overlay[0:imj.shape[0], 0:imj.shape[1]] += imj

                agreement[i, j] = (numpy.sum(overlay == 2) * 2) \
                    / (numpy.sum(imi == 1) + numpy.sum(imj == 1))

        normalize = matplotlib.colors.Normalize(
            agreement[agreement > 0].min(),
            agreement[agreement < 1].max())
        mappable = matplotlib.cm.ScalarMappable(normalize)

        axes.matshow(agreement, norm=normalize)

        axes.set_xticks(numpy.arange(len(self.names)))
        axes.set_xticklabels(self.names, rotation=45, ha="left")
        axes.set_xlim([-0.5, len(self.names) - 0.5])

        axes.set_yticks(numpy.arange(len(self.names)))
        axes.set_yticklabels(self.names)
        axes.set_ylim([-0.5, len(self.names) - 0.5])

        matplotlib.pyplot.colorbar(mappable, ax=axes, shrink=0.79)

        figure.tight_layout()
        return figure


def search_parameters():
    names = ["CMS II.6 no.70 HM 455/10", "CMS II.6 no.70 HM 455/15"]

    images = [
        "data/CMS-II6-070-455-10_S075_1356_180519_GMCFO_r1.00_n4_v256_f00.png",
        "data/CMS-II6-070-455-15_S075_1356_080519_GMCFO_r1.00_n4_v256_f00.png"]

    exp = Experiment(names, images)

    exp.load_images()
    exp.whiten_images()
    exp.extract_daisy()
    exp.estimate_density()

    for r in [1, 2, 3, 4, 5]:
        exp.assign_features(r=r)
        exp.show_assignments().savefig(
            "plots/parameters/Ar{}.png".format(r))

        exp.regress_manifold()
        exp.show_regression().savefig(
            "plots/parameters/Rr{}.png".format(r))

        exp.warp_images()
        exp.show_alignment().savefig(
            "plots/parameters/Jr{}.png".format(r))

    for w in [0.1, 0.3, 0.4, 0.5, 0.6, 0.7, 0.9]:
        exp.assign_features(w=w)
        exp.show_assignments().savefig(
            "plots/parameters/Aw{}.png".format(w))

        exp.regress_manifold()
        exp.show_regression().savefig(
            "plots/parameters/Rw{}.png".format(w))

        exp.warp_images()
        exp.show_alignment().savefig(
            "plots/parameters/Jw{}.png".format(w))

    for c in [1, 10, 50, 100, 500, 1000, 5000]:
        exp.assign_features()
        exp.show_assignments().savefig(
            "plots/parameters/Ac{}.png".format(c))

        exp.regress_manifold(c=c)
        exp.show_regression().savefig(
            "plots/parameters/Rc{}.png".format(c))

        exp.warp_images()
        exp.show_alignment().savefig(
            "plots/parameters/Jc{}.png".format(c))

# On remote host
# eval "$(/export/home/bbogacz/anaconda3/bin/conda shell.bash hook)"
# cd projects/erkon3d
# python3 src/registration.py

# On local host
# cd projects/erkon3d
# scp .\src\*.py namru:projects/erkon3d/src
# scp -r namru:projects/erkon3d/plots/ plots/

if __name__ == "__main__":
    search_parameters()
