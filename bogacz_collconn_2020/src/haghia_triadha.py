import registration

# Haghia Triadha - 24 impressions in total

# The focus here is on the use of one seal across several sealings,
# including sealings of different shapes.

# How similar/dissimilar are the impressions within a sealing form and
# between  sealing forms? Does this suggest the same person made each
# impression or not? Is it possible that one person used more than one seal?

# The seals chosen are from amongst the sealing leaders at Haghia Triadha -
# how do reconstructions of sealing practises need to change if more than
# one person can use the same seal?

def seal1():
    """Seal 1, 6 impressions
    CMS II. 6 no. 70 (HMs 455/3, 455/5, 455/10, 455/15, 456/5, AAPM 10.615)"""

    names = [
        "CMS II.6 no.70 HM 455/3",
        "CMS II.6 no.70 HM 455/5",
        "CMS II.6 no.70 HM 455/10",
        "CMS II.6 no.70 HM 455/15",
        "CMS II.6 no.70 HM 456/5",
        # Very noisy "CMS II.6 no.70 AAPM 10.615",
    ]

    images = [
        "data/CMS-II6-070-455-3_S075_1356_080519_GMCFO_r1.00_n4_v256_f00.png",
        "data/CMS-II6-070-455-5_S075_1356_180519_GMCFO_r1.00_n4_v256_f00.png",
        "data/CMS-II6-070-455-10_S075_1356_180519_GMCFO_r1.00_n4_v256_f00.png",
        "data/CMS-II6-070-455-15_S075_1356_080519_GMCFO_r1.00_n4_v256_f00.png",
        "data/CMS-II6-070-456-5_S075_1356_070519_CFO_r1.00_n4_v256_f00.png",
        # Very noisy: "data/CMS-II6-070-AAPM-10-615_S075_1356_080519" \
        # "_CFO_r1.00_n4_v256_f00.png",
    ]

    exp = registration.Experiment(names, images)

    exp.load_images()
    exp.show_images().savefig(
        "plots/Haghia-Triadha-Seal-1/images.png")

    exp.whiten_images()
    exp.show_threshold().savefig(
        "plots/Haghia-Triadha-Seal-1/threshold.png")

    exp.extract_daisy()
    exp.show_daisy().savefig(
        "plots/Haghia-Triadha-Seal-1/daisy.png")

    exp.estimate_density()
    exp.show_density().savefig(
        "plots/Haghia-Triadha-Seal-1/density.png")

    exp.assign_features()
    exp.show_assignments().savefig(
        "plots/Haghia-Triadha-Seal-1/assignments.png")

    exp.regress_manifold()
    exp.show_regression().savefig(
        "plots/Haghia-Triadha-Seal-1/regression.png")

    exp.warp_images()

    exp.show_overlay().savefig(
        "plots/Haghia-Triadha-Seal-1/overlay.png")
    exp.show_alignment().savefig(
        "plots/Haghia-Triadha-Seal-1/alignment.png")
    exp.show_matrix().savefig(
        "plots/Haghia-Triadha-Seal-1/matrix.png")


def paper1_figure4():
    names = [
        "CMS II.6 no.70 HM 455/10",
        "CMS II.6 no.70 HM 455/15",
    ]

    images = [
        "data/CMS-II6-070-455-10_S075_1356_180519_GMCFO_r1.00_n4_v256_f00.png",
        "data/CMS-II6-070-455-15_S075_1356_080519_GMCFO_r1.00_n4_v256_f00.png",
    ]

    exp = registration.Experiment(names, images)
    exp.load_images()
    exp.whiten_images()
    exp.extract_daisy()
    exp.estimate_density()
    exp.assign_features()
    exp.regress_manifold()
    exp.warp_images()
    exp.show_alignment().savefig(
        "plots/paper1/figure4.png")


def paper1_figure8a():
    names = [
        "CMS II.6 no.20 HM 434/1",
        "CMS II.6 no.20 HM 434/2",
    ]

    images = [
        "data/CMS-II6-020-434-1_S075_1356_190619_CFO_r1.00_n4_v256_f00.png",
        "data/CMS-II6-020-434-2_S075_S050_061119_GMCFO_r1.00_n4_v256_f00.png",
    ]

    exp = registration.Experiment(names, images)
    exp.load_images()
    exp.whiten_images()
    exp.extract_daisy()
    exp.estimate_density()
    exp.assign_features()
    exp.regress_manifold()
    exp.warp_images()
    exp.show_alignment().savefig(
        "plots/paper1/figure8a.png")


def paper1_figure8b():
    names = [
        "CMS II.6 no.20 HM 434/2",
        "CMS II.6 no.20 HM 434/4",
    ]

    images = [
        "data/CMS-II6-020-434-2_S075_S050_061119_GMCFO_r1.00_n4_v256_f00.png",
        "data/CMS-II6-020-434-4_S075_S050_061119_GMCFO_r1.00_n4_v256_f00.png",
    ]

    exp = registration.Experiment(names, images)
    exp.load_images()
    exp.whiten_images()
    exp.extract_daisy()
    exp.estimate_density()
    exp.assign_features()
    exp.regress_manifold()
    exp.warp_images()
    exp.show_alignment().savefig(
        "plots/paper1/figure8b.png")


def paper1_figure10():
    names = [
        "CMS II.6 no.20 HM 434/2",
        "CMS II.6 no.20 HM 434/4",
    ]

    images = [
        "data/CMS-II6-011-441-05_S075_S050_081119_GMCFO_r1.00_n4_v256_f00.png",
        "data/CMS-II6-011-443-13_S075_S050_081119_GMCFO_r1.00_n4_v256_f00.png",
    ]

    exp = registration.Experiment(names, images)
    exp.load_images()
    exp.whiten_images()
    exp.extract_daisy()
    exp.estimate_density()
    exp.assign_features()
    exp.regress_manifold()
    exp.warp_images()
    exp.show_alignment().savefig(
        "plots/paper1/figure10.png")


# On remote host
# ssh namru
# eval "$(/export/home/bbogacz/anaconda3/bin/conda shell.bash hook)"
# cd projects/erkon3d
# python3 src/haghia_triadha.py

# On local host
# cd projects/erkon3d
# scp .\src\*.py namru:projects/erkon3d/src
# scp -r namru:projects/erkon3d/plots/* plots

if __name__ == "__main__":
    seal1()
    paper1_figure4()
    paper1_figure8a()
    paper1_figure8b()
    paper1_figure10()
