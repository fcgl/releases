The related publication for reference (in BibTeX format):
```
@inproceedings {CollConn20,
    booktitle = {Proc. of Collect \& Connect Archives and Collections in a Digital Age},
    title = {{Visualizing and Contextualizing Outliers in Aegean Seal Collections}},
    author = {Bogacz, Bartosz and Finlayson, Sarah and Panagiotopolous, Diamantis and Mara, Hubert},
    year = {2020},
}
```
Pre-print [available on ResearchGate](https://www.researchgate.net/publication/331324346_Recovering_and_Visualizing_Deformation_in_3D_Aegean_Sealings).
