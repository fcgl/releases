#import requests
#import json
import csv
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d, Axes3D

#computes center of gravity
def computeCOG(vertex_1_x, vertex_1_y, vertex_1_z, vertex_2_x, vertex_2_y, vertex_2_z, vertex_3_x, vertex_3_y, vertex_3_z, vertex_4_x, vertex_4_y, vertex_4_z,):
	gravityX = (vertex_1_x + vertex_2_x + vertex_3_x + vertex_4_x)/4
	gravityY = (vertex_1_y + vertex_2_y + vertex_3_y + vertex_4_y)/4
	gravityZ = (vertex_1_z + vertex_2_z + vertex_3_z + vertex_4_z)/4
	return np.array([gravityX, gravityY, gravityZ])


def main():
	
	fig = plt.figure()
	ax = fig.add_subplot(111, projection='3d')
	ax.view_init(70,80)
	#ax = Axes3D(fig)
	
	#filenamePlaceholder = 'HS_0270c_GMOCF_r1.50_n4_v512.csv'
	filenamePlaceholder = 'HS_0550_GMOCF_r1.50_n4_v512.csv'
	
	
	with open(filenamePlaceholder, 'rt') as csvfile:
		customReader = csv.reader(csvfile, delimiter=',')
		#customReader = csv.reader(csvfile)
		for row in customReader:
			#for column in customReader:
			#print(type(row)) #is of type list
			#print(len(row)) #should be 14
			if len(row) == 14: # to kick out empty lines that may show up in the beginning and end of the file
				#print(type(row[2])) is a string
				currentCenterOfGravity = computeCOG(float(row[2]),float(row[3]),float(row[4]),float(row[5]),float(row[6]),float(row[7]),float(row[8]),float(row[9]),float(row[10]),float(row[11]),float(row[12]),float(row[13]))
				#print(currentCenterOfGravity)
				ax.scatter(currentCenterOfGravity[0],currentCenterOfGravity[1],currentCenterOfGravity[2])
	
	
	plt.show
	return 0



if __name__== "__main__":
	main()


