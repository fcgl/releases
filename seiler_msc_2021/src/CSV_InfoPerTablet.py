#for file traversal
import os
import glob

#for csv file opening
import csv

#for json parsing
#import json

#for structures
import numpy as np

#for distance calculation
#from numpy import linalg as LA
#for plotting
import matplotlib.pyplot as plt

def calculateDeterminant(m):
    return (m[0][0] * (m[1][1] * m[2][2] - m[1][2] * m[2][1]) -
            m[1][0] * (m[0][1] * m[2][2] - m[0][2] * m[2][1]) +
            m[2][0] * (m[0][1] * m[1][2] - m[0][2] * m[1][1]))


def subtract(a, b):
    return (a[0] - b[0],
            a[1] - b[1],
            a[2] - b[2])

def calculateTetraederVolume(a, b, c, d):
    return (abs(calculateDeterminant((subtract(a, b), subtract(b, c), subtract(c, d)))) / 6.0)

def calculateEuclidianDistance3D(po1x,po1y,po1z, po2x,po2y,po2z):
	pointA = np.array((po1x,po1y,po1z))
	pointB = np.array((po2x,po2y,po2z))
	return np.linalg.norm(pointA-pointB)

def openCSVFile(csvFilePath):
	
	#opens the csv file
	#write all content into numpyArray
	
	CSVFileContent = np.zeros((1,15), dtype='float')
	
	quiverData = np.zeros((1,2), dtype='float')
	
	with open(csvFilePath, 'rt') as csvfile:
		customReader = csv.reader(csvfile, delimiter=',')
		for row in customReader:
			if len(row) == 14: # to kick out empty lines that may show up in the beginning and end of the file
				
				#calculate the tetraeder volume
				point1 = [float(row[2]), float(row[3]), float(row[4])]
				point2 = [float(row[5]), float(row[6]), float(row[7])]
				point3 = [float(row[8]), float(row[9]), float(row[10])]
				point4 = [float(row[11]), float(row[12]), float(row[13])]
				
				CSVFileContent = np.append(
						CSVFileContent,
						[[float(row[0]),
						float(row[1]),
						float(row[2]),
						float(row[3]),
						float(row[4]),
						float(row[5]),
						float(row[6]),
						float(row[7]),
						float(row[8]),
						float(row[9]),
						float(row[10]),
						float(row[11]),
						float(row[12]),
						float(row[13]),
						calculateTetraederVolume(point1, point2, point3, point4)]], axis=0)
	
				distanceTopPo1 = calculateEuclidianDistance3D(float(row[2]),float(row[3]),float(row[4]),float(row[5]),float(row[6]),float(row[7])),
				distanceTopPo2 = calculateEuclidianDistance3D(float(row[2]),float(row[3]),float(row[4]),float(row[8]),float(row[9]),float(row[10])),
				distanceTopPo3 = calculateEuclidianDistance3D(float(row[2]),float(row[3]),float(row[4]),float(row[11]),float(row[12]),float(row[13]))
				
				#find out, how wedge oriented
				otherVerticesAverageZ = (float(row[7]) + float(row[10]) + float(row[13]))/ 3.0
				
				distanceList = [distanceTopPo1, distanceTopPo2, distanceTopPo3]
				maxValue = max(distanceList)
				max_index = distanceList.index(maxValue)
				#if the first distance is the largest, look at the vector
				#between tetraeder top and vertex 1
				#etc.
				
				
				
				
				#the tetraeder top is deeper
				if(otherVerticesAverageZ > float(row[4])):
					if(max_index==0):
						vectorCompx = float(row[5]) - float(row[2])
						vectorCompy = float(row[6]) - float(row[3])
						quiverData = np.append(quiverData, [[vectorCompx, vectorCompy]], axis=0)
					elif(max_index==1):
						vectorCompx = float(row[8]) - float(row[2])
						vectorCompy = float(row[9]) - float(row[3])
						quiverData = np.append(quiverData, [[vectorCompx, vectorCompy]], axis=0)
					elif(max_index==2):
						vectorCompx = float(row[11]) - float(row[2])
						vectorCompy = float(row[12]) - float(row[3])
						quiverData = np.append(quiverData, [[vectorCompx, vectorCompy]], axis=0)
					
				else:
					#the wedge is at the backside of the tablet, the x value needs to be switched
					if(max_index==0):
						vectorCompx = (float(row[5]) - float(row[2]))*(-1.0)
						vectorCompy = float(row[6]) - float(row[3])
						quiverData = np.append(quiverData, [[vectorCompx, vectorCompy]], axis=0)
					if(max_index==1):
						vectorCompx = (float(row[8]) - float(row[2]))*(-1.0)
						vectorCompy = float(row[9]) - float(row[3])
						quiverData = np.append(quiverData, [[vectorCompx, vectorCompy]], axis=0)
					if(max_index==2):
						vectorCompx = float(row[11]) - float(row[2])*(-1.0)
						vectorCompy = float(row[12]) - float(row[3])
						quiverData = np.append(quiverData, [[vectorCompx, vectorCompy]], axis=0)
					
	
	
	
	
	
	#delete the first row, it only holds zeros
	CSVFileContent = np.delete(CSVFileContent, 0,0)
	quiverData = np.delete(quiverData, 0,0)
	
	return CSVFileContent, quiverData

def main():
	
	print("Script was started")
	
	CSVsLocationString = "/home/damian/Desktop/extractedCSVs/*"
	#JSONsLocationString = "/home/damian/Desktop/JSONCollection/"
	
	
	CSVFilesOpened = 0
	correspondingCSVExists = 0
	#noEntry = 0
	
	
	#avHei = np.array([], dtype=float)
	
	#vVol = np.array([], dtype=float)
	
	#for simple scatter plots
	#languages = np.array([], dtype=object)
	#periods = np.array([], dtype=object)
	#genres = np.array([], dtype=object)
	#proveniences = np.array([], dtype=object)
	#materials = np.array([], dtype=object)
	
	#for boxplots
	#languages = np.array([], dtype=int)
	#periods = np.array([], dtype=int)
	#genres = np.array([], dtype=int)
	#proveniences = np.array([], dtype=int)
	#materials = np.array([], dtype=int)
	
	rotationN = 80
	commonDPI = 200

	for name in glob.glob(CSVsLocationString):
		#print(name)
		#print(os.path.basename(name))
		
		

		filename = os.fsdecode(name)
		#if filename.endswith(".csv"):
		if filename.endswith("0550_GMOCF_r1.50_n4_v512.csv"):
		
			
			#print(filename)
			
			#CSVFileContent = openCSVFile(os.path.basename(name))
			CSVFileContent, quiverData = openCSVFile(filename)
			
			
			
			CSVFilesOpened += 1
			
			
			#CSVFileContent holds these floats
			# 1 RANSAC Quality
			# 1 Tetraeder Height
			# 12 = 4 * 3(=x,y,z coordinates, first set is tetraeder top)
			
			#look at Tetraeder Height
			heightPerTablet = CSVFileContent[:,1]
			
			#look at volume
			volumePerTablet = CSVFileContent[:,14]
			
			#print(heightPerTablet.shape)
			averagedTetraederHeight = np.mean(heightPerTablet)
			#print(type(averagedTetraederHeight)) # a numpy float
			
			averagedTetraederVolume = np.mean(volumePerTablet)
			
			#print(os.path.basename(name))
			#print(averagedTetraederHeight)
			
			#avHei = np.append(avHei, averagedTetraederHeight)
			#avVol = np.append(avVol, averagedTetraederVolume)
			
			
			#the following code was successfully run 2020.11.19 0:03 
			
			volumeHistogramTitle = 'Wedge Volume Histogram of ' + os.path.basename(name)[0:len(os.path.basename(name))-4]
			volumeHistogramXLabel = 'Wedge Volume in cubic mm'
			volumeHistogramYLabel = 'Number of Wedges'
			volumeHistogramFileName = os.path.basename(name)[0:len(os.path.basename(name))-4] + '_VolumeHistogram.png'
			
			heightHistogramTitle = 'Wedge Height Histogram of ' + os.path.basename(name)[0:len(os.path.basename(name))-4]
			heightHistogramXLabel = 'Wedge Height in mm'
			heightHistogramYLabel = 'Number of Wedges'
			heightHistogramFileName = os.path.basename(name)[0:len(os.path.basename(name))-4] + '_HeightHistogram.png'
			
			#volume histogram
			plt.figure()
			
			plt.hist(volumePerTablet, bins=40)
			
			plt.title(volumeHistogramTitle)
			plt.xlabel(volumeHistogramXLabel)
			plt.ylabel(volumeHistogramYLabel)
			plt.yscale('log')
			#plt.xscale('log')
			plt.savefig(volumeHistogramFileName, dpi=commonDPI)
			
			
			#height histogram
			plt.figure()
			
			plt.hist(heightPerTablet, bins=40)
			
			plt.title(heightHistogramTitle)
			plt.xlabel(heightHistogramXLabel)
			plt.ylabel(heightHistogramYLabel)
			plt.yscale('log')
			#plt.xscale('log')
			plt.savefig(heightHistogramFileName, dpi=commonDPI)
			
			data1 = quiverData[:,0]
			data2 = quiverData[:,1]
			
			fig1, ax1 = plt.subplots()
			#plt.xticks(rotation=rotationN)
			ax1.scatter(data1, data2)
			fig1Name = 'Plot showing the inner edge direction'
			ax1.set_title(fig1Name)
			ax1.set_xlabel('x coordinate')
			ax1.set_ylabel('y coordinate')
			fig1.set_size_inches(8, 10)
			plt.savefig(fig1Name, dpi=commonDPI)
			
			"""
			quiverPlotTitle = 'Quiver Plot of longest inner edge direction of ' + os.path.basename(name)[0:len(os.path.basename(name))-4]
			quiverPlotXLabel = 'x coordinate in mm'
			quiverPlotYLabel = 'y coordinate in mm'
			quiverPlotFileName = os.path.basename(name)[0:len(os.path.basename(name))-4] + '_QuiverPlot.png'
			
			
			
			#longest inner edge direction
			origin = np.array([[0,0,0],[0,0,0]])
			if(CSVFilesOpened < 10):
				plt.figure()
				
				plt.quiver(*origin, quiverData[:,0], quiverData[:,1])
				
				plt.title(quiverPlotTitle)
				plt.xlabel(quiverPlotXLabel)
				plt.ylabel(quiverPlotYLabel)
				plt.savefig(quiverPlotFileName, dpi=commonDPI)
			"""
		
		
	
	
	
	
	
	print("script has ended")
	print('   ', CSVFilesOpened, ' CSV files were looked at') 
	
	
	
	#scatterplots
	"""
	fig1, ax1 = plt.subplots()
	plt.xticks(rotation=rotationN)
	
	
	#print(languages)
	
	#LHData = [languages.astype(int), avHei]
	
	#ax1.boxplot(LHData)
	#ax1.plot(languages, avHei)
	
	ax1.scatter(languages, avHei)
	fig1Name = 'ScatterPlot showing the relationship between language and averaged wedge height'
	ax1.set_title(fig1Name)
	ax1.set_xlabel('Language')
	ax1.set_ylabel('Averaged Wedge Height')
	fig1.set_size_inches(8, 10)
	plt.savefig(fig1Name, dpi=commonDPI)
	
	
	fig2, ax2 = plt.subplots()
	plt.xticks(rotation=rotationN)
	
	ax2.scatter(periods, avHei)
	fig2Name = 'ScatterPlot showing the relationship between period and averaged wedge height'
	ax2.set_title(fig2Name)
	ax2.set_xlabel('Period')
	ax2.set_ylabel('Averaged Wedge Height')
	fig2.set_size_inches(8, 18)
	plt.savefig(fig2Name, dpi=commonDPI)
	
	
	fig3, ax3 = plt.subplots()
	plt.xticks(rotation=rotationN)
	
	ax3.scatter(genres, avHei)
	fig3Name = 'ScatterPlot showing the relationship between genre and averaged wedge height'
	ax3.set_title(fig3Name)
	ax3.set_xlabel('Genre')
	ax3.set_ylabel('Averaged Wedge Height')
	fig3.set_size_inches(8, 14)
	plt.savefig(fig3Name, dpi=commonDPI)
	
	
	fig4, ax4 = plt.subplots()
	plt.xticks(rotation=rotationN)
	
	ax4.scatter(proveniences, avHei)
	fig4Name = 'ScatterPlot showing the relationship between provenience and averaged wedge height'
	ax4.set_title(fig4Name)
	ax4.set_xlabel('Provenience')
	ax4.set_ylabel('Averaged Wedge Height')
	fig4.set_size_inches(8, 18)
	plt.savefig(fig4Name, dpi=commonDPI)
	
	fig5, ax5 = plt.subplots()
	plt.xticks(rotation=rotationN)
	
	ax5.scatter(materials, avHei)
	fig5Name = 'ScatterPlot showing the relationship between material and averaged wedge height'
	ax5.set_title(fig5Name)
	ax5.set_xlabel('Material')
	ax5.set_ylabel('Averaged Wedge Height')
	fig5.set_size_inches(8, 10)
	plt.savefig(fig5Name, dpi=commonDPI)
	
	
	fig6, ax6 = plt.subplots()
	plt.xticks(rotation=rotationN)
		
	ax6.scatter(languages, avVol)
	fig6Name = 'ScatterPlot showing the relationship between language and averaged wedge volume'
	ax6.set_title(fig6Name)
	ax6.set_xlabel('Language')
	ax6.set_ylabel('Averaged Wedge Volume')
	fig6.set_size_inches(8, 10)
	plt.savefig(fig6Name, dpi=commonDPI)
	
	
	fig7, ax7 = plt.subplots()
	plt.xticks(rotation=rotationN)
	
	ax7.scatter(periods, avVol)
	fig7Name = 'ScatterPlot showing the relationship between period and averaged wedge volume'
	ax7.set_title(fig7Name)
	ax7.set_xlabel('Period')
	ax7.set_ylabel('Averaged Wedge Volume')
	fig7.set_size_inches(8, 18)
	plt.savefig(fig7Name, dpi=commonDPI)
	
	
	fig8, ax8 = plt.subplots()
	plt.xticks(rotation=rotationN)
	
	ax8.scatter(genres, avVol)
	fig8Name = 'ScatterPlot showing the relationship between genre and averaged wedge volume'
	ax8.set_title(fig8Name)
	ax8.set_xlabel('Genre')
	ax8.set_ylabel('Averaged Wedge Volume')
	fig8.set_size_inches(8, 14)
	plt.savefig(fig8Name, dpi=commonDPI)
	
	
	fig9, ax9 = plt.subplots()
	plt.xticks(rotation=rotationN)
	
	ax9.scatter(proveniences, avVol)
	fig9Name = 'ScatterPlot showing the relationship between provenience and averaged wedge volume'
	ax9.set_title(fig9Name)
	ax9.set_xlabel('Provenience')
	ax9.set_ylabel('Averaged Wedge Volume')
	fig9.set_size_inches(8, 18)
	plt.savefig(fig9Name, dpi=commonDPI)
	
	
	fig10, ax10 = plt.subplots()
	plt.xticks(rotation=rotationN)
	
	ax10.scatter(materials, avVol)
	fig10Name = 'ScatterPlot showing the relationship between material and averaged wedge volume'
	ax10.set_title(fig10Name)
	ax10.set_xlabel('Material')
	ax10.set_ylabel('Averaged Wedge Volume')
	fig10.set_size_inches(8, 10)
	plt.savefig(fig10Name, dpi=commonDPI)
	"""
	#boxplots
	"""
	fig1, ax1 = plt.subplots()
	plt.xticks(rotation=rotationN)
	
	LHData = [languages, avHei]
	
	ax1.boxplot(LHData)
	
	
	fig2, ax2 = plt.subplots()
	plt.xticks(rotation=rotationN)
	
	PHData = [periods, avHei]
	
	ax2.boxplot(PHData)
	
	
	fig3, ax3 = plt.subplots()
	plt.xticks(rotation=rotationN)
	
	GHData = [genres, avHei]
	
	ax3.boxplot(GHData)
	
	
	fig4, ax4 = plt.subplots()
	plt.xticks(rotation=rotationN)
	
	PHData = [proveniences, avHei]
	
	ax4.boxplot(PHData)
	
	
	fig5, ax5 = plt.subplots()
	plt.xticks(rotation=rotationN)
	
	MHData = [materials, avHei]
	
	ax5.boxplot(MHData)
	"""
	
	
	return 0



if __name__== "__main__":
	main()






#create a struct or class
#string Filename of csv
#averaged tetraeder height
#check if the computation of the tetraeder height is correct
#averaged tetraeder volume
#JSonInfo
#string language
#int era
#string type administrative and so on
#Schreibrichtung usw.


#go through all csv data

#go through json file

#PCA and language

#PCA and genre

#PCA 