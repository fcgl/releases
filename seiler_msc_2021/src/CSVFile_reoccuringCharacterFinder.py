#import requests
#import json
import csv
import numpy as np
from scipy import spatial
from sklearn import decomposition
from sklearn import cluster
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
#import mpl_toolkits.mplot3d.art3d as a3
import matplotlib.colors as colors
import pylab as pl
import scipy as sp

import matplotlib.pyplot as plt
#from mpl_toolkits.mplot3d import axes3d, Axes3D


#computes center of gravity
def computeCOG(vertex_1_x, vertex_1_y, vertex_1_z, vertex_2_x, vertex_2_y, vertex_2_z, vertex_3_x, vertex_3_y, vertex_3_z, vertex_4_x, vertex_4_y, vertex_4_z,):
	gravityX = (vertex_1_x + vertex_2_x + vertex_3_x + vertex_4_x)/4
	gravityY = (vertex_1_y + vertex_2_y + vertex_3_y + vertex_4_y)/4
	gravityZ = (vertex_1_z + vertex_2_z + vertex_3_z + vertex_4_z)/4
	return np.array([gravityX, gravityY, gravityZ])
	
#def computeDistance():
	#return 0


def main():
	
	#how many nearest neighbours
	nNK = 5
	
	#how many clusters does the user want
	#AKA: how many different characters shall be recognized on the tablet
	clusterNumber = 8
	
	#fig = plt.figure()
	#ax = fig.add_subplot(111, projection='3d')
	#ax = Axes3D(fig)
	
	#Center of Gravity Coordinates
	xValues = []
	yValues = []
	zValues = []
	
	#Pure Vertex Coordinates
	#We only care about the coordinates of triangles opposite of the triangle top
	#This is position 5 till 13
	#pureX = []
	#pureY = []
	#pureZ = []
	#will be collected in numpy array
	triangleVertices = np.zeros((1,9))
	#print(triangleVertices)
	
	#filenamePlaceholder = 'HS_0270c_GMOCF_r1.50_n4_v512.csv'
	filenamePlaceholder = 'HS_0550_GMOCF_r1.50_n4_v512.csv'
	with open(filenamePlaceholder, 'rt') as csvfile:
		customReader = csv.reader(csvfile, delimiter=',')
		#customReader = csv.reader(csvfile)
		for row in customReader:
			#for column in customReader:
			#print(type(row)) #is of type list
			#print(len(row)) #should be 14
			if len(row) == 14: # to kick out empty lines that may show up in the beginning and end of the file
				#print(type(row[2])) is a string
				currentCenterOfGravity = computeCOG(float(row[2]),float(row[3]),float(row[4]),float(row[5]),float(row[6]),float(row[7]),float(row[8]),float(row[9]),float(row[10]),float(row[11]),float(row[12]),float(row[13]))
				#print(currentCenterOfGravity)
				#ax.scatter(currentCenterOfGravity[0],currentCenterOfGravity[1],currentCenterOfGravity[2])
				xValues.append(currentCenterOfGravity[0])
				yValues.append(currentCenterOfGravity[1])
				zValues.append(currentCenterOfGravity[2])
				
				#pureX.append(float(row[5]))
				#pureX.append(float(row[8]))
				#pureX.append(float(row[11]))
				#pureY.append(float(row[6]))
				#pureY.append(float(row[9]))
				#pureY.append(float(row[12]))
				#pureZ.append(float(row[7]))
				#pureZ.append(float(row[10]))
				#pureZ.append(float(row[13]))
				
				triangleVertices = np.append(triangleVertices,[[float(row[5]),float(row[6]),float(row[7]),float(row[8]),float(row[9]),float(row[10]),float(row[11]),float(row[12]),float(row[13])]], axis=0)
	
	#print(len(xValues))
	
	#delete the first row, it only holds zeros
	triangleVertices = np.delete(triangleVertices, 0,0)
	#print(len(triangleVertices))
	
	
	#numberIntraDifferences = 6
	#intra distances do not need to be computed per hand as pdist does this
	
	tree = spatial.KDTree(list(zip(xValues,yValues,zValues)))
	distances, locations = tree.query(list(zip(xValues,yValues,zValues)),k=nNK,p=2)
	# 2 stands for euclidean distance
	
	#print(distances.shape)
	#print(locations.shape)
	
	#print(distances)
	#print(locations)
	
	#compute the distances of the elements between eachother
	#elements means those COF close to eachother
	
	#small test
	#somepo = []
	#point1 = [0.0,0.0,0.0]
	#point2 = [0.0,1.0,0.0]
	#point3 = [0.0,0.0,3.0]
	#somepo.append(point1)
	#somepo.append(point2)
	#somepo.append(point3)
	
	#zwischenA = spatial.distance.pdist(somepo, 'euclidean')
	#print(zwischenA)
	
	
	
	
	
	
	intraDistances = []
	
	for row in locations:
		
		#build array to get intra distances
		arrayOfPoints = []
		for entry in row:
			pointCoordinates = [xValues[entry],yValues[entry],zValues[entry]]
			arrayOfPoints.append(pointCoordinates)
		
		
		#spatial.distance.pdist works this way:
		#imagine 3 points, then pdist will give the distance between
		#point 1 and point 2
		#point 1 and point 3
		#point 2 and point 3
		intraDistanceArray = spatial.distance.pdist(arrayOfPoints, 'euclidean')
		
		#intraDistanceArray.sort()
		#sort in reverse order
		sortedHighToLow = np.sort(intraDistanceArray)[::-1]
		intraDistances.append(sortedHighToLow)
		
	#print(intraDistances)
	
	#print(numberOfIntradistances)
	
	#features = np.zeros((len(intraDistances),len(intraDistances[0])))
	
	#we will only care for two principle components
	PCAResults = decomposition.PCA(n_components=2).fit_transform(intraDistances)
	#PCA first entry per row is Principal component no. 1, PCA second entry per row is PC no. 2
	#print(type(PCAresults)) #is a numpy array
	#print(PCAresults.shape) #length of elements of intraDistances row Number, times n_components, here 2
	
	assert PCAResults.shape == (len(intraDistances), 2)
	#assert helps checking, if conditions are met
	
	#plt.scatter(PCAResults[:,0], PCAResults[:,1])
	
	
	clustering = cluster.KMeans(n_clusters=clusterNumber).fit(PCAResults)
	#print(type(clustering)) #ist vom Typ her sklearn.cluster.k_means_.KMeans
	
	
	labeledElements = clustering.labels_
	#print(type(labels)) #numpy array
	#print(labels.ndim) #one dimension, 
	#labels = clustering.transform(PCAResults)
	#print(type(labels)) #numpy array
	#print(labels.shape) #currently 1085, 8 
	#print(labels[1084])
	
	fig1 = plt.figure(figsize=(10,9))
	
	ax1 = fig1.add_subplot(111)
	ax1.scatter(PCAResults[:,0], PCAResults[:,1], c=labeledElements.astype(np.float))
	
	title1String = 'K-Means clustering of PCA into ' + str(clusterNumber) + ' clusters. Based on ' + str(nNK) + ' nearest neighbours.'
	ax1.set_title(title1String)
	ax1.set_xlabel('Principal component no. 1')
	ax1.set_ylabel('Principal component no. 2')
	
	PNGName1 = filenamePlaceholder[0:len(filenamePlaceholder)-4] + '_KMeans_Cluster' + '.png'
	
	plt.savefig(PNGName1, dpi=200)
	
	
	#triangles = np.zeros((5,3,2))
	#plt.plot(triangles[:,[0,1,2,3],0], triangles[:,[0,1,2,3],1])
	
	
	#fig = plt.figure()
	#ax = fig.add_subplot(111, projection='3d')
	
	#do a pseudo PCA
	#shorten the array
	#for row in intraDistances:
		#PCA = row[0:3]
		#PCAValues.append(PCA)
		#ax.scatter(PCA[0],PCA[1],PCA[2])
	
	
	
	#print(PCAValues)
	
	
	#plt.show
	
	#Find clusters
	
	#guess how many similar structures will occur
	#nSimilarStructures = 10
	
	
	#PCAtree = spatial.KDTree(PCAValues)
	#locations, distances = PCAtree.query(PCAValues,k=nSimilarStructures,p=2)
	
	
	#ax.view_init(0,90)
	
	#plt.show
	
	
	
	
	
		
	
	
	
	#in square form:
	#squareform(spatial.distance.pdist(...))
	#someResult = spatial.distance.pdist(something, 'euclidian', )
	
	#intraDistances = np.zeros((len(xValues), numberIntraDifferences))
	
	#print(len(intraDistances)) #175
	
	#print(intraDistances)
	#print(type(distances)) #is a numpy.ndarray
	#print(distances.shape) #for example (175,5)
	#print(distances)
	#scheint schon sortiert zu sein
	
	#print(distances[0, :])
	
	#TODO Sort the intraDistances
	
	#sortedDistances = distances
	#sortedDistances.sort(axis=1)
	
	#print(sortedDistances[0, :])
	
	#print(tree.data)
	
	#print(locations)
	
	#plt.show
	
	
	
	#Every center of gravity belongs to a pure Vertex 
	#Every locations row gives the centers of gravity
	#Every intradistances row corresponds to a locations row.
	#Every PCAresults row corresponds to a intradistances row
	#Every Labels Element says the label of the PCAresults row
	
	#triangleVertices = triangleVertices[1083:]
	#print(triangleVertices)
	
	#fig = plt.figure(figsize=(4,4), dpi=300)
	#fig = plt.figure()
	
	fig2 = plt.figure(figsize=(10,9))
	ax2 = fig2.add_subplot(111, projection='3d')
	
	#fc = ["crimson" if i%2 else "gold" for i in range(triangleVertices.shape[0])]
	
	#fc = [np.random.rand(1,3)]
	
	#a dummy face color
	fc = [0.0,0.0,0.0]
	
	#x=colors.rgb2hex(sp.rand(3))
	#print(x)
	#print(type(x))
	
	
	for i in range(clusterNumber):
		
		currentColor = colors.rgb2hex([np.random.uniform(),np.random.uniform(),np.random.uniform()])
		
		#belongToCurrentCluster = np.zeros((1,9))
		for labelAtPosition in range(len(labeledElements)):
			#use .item() to convert from numpy int to normal int
			#print(labelAtPosition)
			if i==labeledElements[labelAtPosition]:
				
				poly3d = [[triangleVertices[labelAtPosition, j*3:j*3+3] for j in range(3)]]
				
				#poly3d = [[triangleVertices[labi, j*3:j*3+3] for j in range(3)] for labi in range(10)]
				#print(poly3d)
				wedge = Poly3DCollection(poly3d, facecolors=fc, linewidths=1)
				wedge.set_color(currentColor)
				
				ax2.add_collection3d(wedge)
				
	
	
	
	#poly3d = [[triangleVertices[i, j*3:j*3+3] for j in range(3)] for i in range(triangleVertices.shape[0])]
	
	#wedges = Poly3DCollection(poly3d, facecolors=fc, linewidths=1)
	#wedges.set_color(colors.rgb2hex(sp.rand(3)))
	#wedges.set_color(if(1==1): (0.1,0.1,0.1))
	
	#ax.add_collection3d(wedges)
	
	ax2.set_xlim(-25,25)
	ax2.set_ylim(-10,50)
	ax2.set_zlim(-15,15)
		
	ax2.view_init(elev=60.0, azim=10.0)
	
	ax2.set_title('Tablet with wedges given the same color, if they belong to a very similar character')
	ax2.set_xlabel('x component')
	ax2.set_ylabel('y component')
	ax2.set_zlabel('z component')
	
	PNGName2 = filenamePlaceholder[0:len(filenamePlaceholder)-4] + '_clustered_characters' + '.png'
	
	plt.savefig(PNGName2, dpi=200)
	
	plt.show()
	
	
	return 0



if __name__== "__main__":
	main()


