#for file traversal
import os
import glob

#for csv file opening
import csv

#for json parsing
import json

#for structures
import numpy as np

#for plotting
import matplotlib.pyplot as plt

def calculateDeterminant(m):
    return (m[0][0] * (m[1][1] * m[2][2] - m[1][2] * m[2][1]) -
            m[1][0] * (m[0][1] * m[2][2] - m[0][2] * m[2][1]) +
            m[2][0] * (m[0][1] * m[1][2] - m[0][2] * m[1][1]))


def subtract(a, b):
    return (a[0] - b[0],
            a[1] - b[1],
            a[2] - b[2])

def calculateTetraederVolume(a, b, c, d):
    return (abs(calculateDeterminant((subtract(a, b), subtract(b, c), subtract(c, d)))) / 6.0)

def calculateEuclidianDistance3D(po1x,po1y,po1z, po2x,po2y,po2z):
	pointA = np.array((po1x,po1y,po1z))
	pointB = np.array((po2x,po2y,po2z))
	return np.linalg.norm(pointA-pointB)

def openCSVFile(csvFilePath):
	
	#opens the csv file
	#write all content into numpyArray
	
	CSVFileContent = np.zeros((1,15), dtype='float')
	
	angleProxy = np.zeros((1,2), dtype='float')
	
	with open(csvFilePath, 'rt') as csvfile:
		customReader = csv.reader(csvfile, delimiter=',')
		for row in customReader:
			if len(row) == 14: # to kick out empty lines that may show up in the beginning and end of the file
				
				#calculate the tetraeder volume
				point1 = [float(row[2]), float(row[3]), float(row[4])]
				point2 = [float(row[5]), float(row[6]), float(row[7])]
				point3 = [float(row[8]), float(row[9]), float(row[10])]
				point4 = [float(row[11]), float(row[12]), float(row[13])]
				
				CSVFileContent = np.append(CSVFileContent,[[float(row[0]),float(row[1]),float(row[2]),float(row[3]),float(row[4]),float(row[5]),float(row[6]),float(row[7]),float(row[8]),float(row[9]),float(row[10]),float(row[11]),float(row[12]),float(row[13]), calculateTetraederVolume(point1, point2, point3, point4)]], axis=0)
				
				distanceTopPo1 = calculateEuclidianDistance3D(float(row[2]),float(row[3]),float(row[4]),float(row[5]),float(row[6]),float(row[7])),
				distanceTopPo2 = calculateEuclidianDistance3D(float(row[2]),float(row[3]),float(row[4]),float(row[8]),float(row[9]),float(row[10])),
				distanceTopPo3 = calculateEuclidianDistance3D(float(row[2]),float(row[3]),float(row[4]),float(row[11]),float(row[12]),float(row[13]))
				
				distanceList = [distanceTopPo1, distanceTopPo2, distanceTopPo3]
				maxValue = max(distanceList)
				
				angleProxy = np.append(angleProxy, maxValue)
	
	
	
	
	
	#delete the first row, it only holds zeros
	CSVFileContent = np.delete(CSVFileContent, 0,0)
	angleProxy = np.delete(angleProxy, 0,0)
	
	return CSVFileContent, angleProxy

def retrieveJSONInformation(JSONsLocationString, tabletName):
	
	shortenedName = tabletName[0:len(tabletName)-23] + 'HeiCuBeDa.json'
	
	JSONFileLocation = JSONsLocationString + shortenedName
	
	with open(JSONFileLocation) as json_file:
		data = json.load(json_file)
		#print(type(data)) #is a dictionary class
		
		#these are the key names
		#for x in data:
			#print(x)
		
		#these are the values
		#for x in data:
			#print(data[x])
		
		if "from_cdli_archival_view" in data:
			#try extracting the following
			#language = "tbd"
			#period = "tbd"
			#genre = "tbd"
			#provenience = "tbd"
			#material = "tbd"
			#print(type(data["from_cdli_archival_view"]))
			if "language" in data["from_cdli_archival_view"]:
				language = data["from_cdli_archival_view"]["language"]
				#in case of empty entries
				if not language:
					language = "none"
			else:
				language = "none"
				
			if "period" in data["from_cdli_archival_view"]:
				period = data["from_cdli_archival_view"]["period"]
				#in case of empty entries
				if not period:
					period = "none"
			else:
				period = "none"
				
			if "genre" in data["from_cdli_archival_view"]:
				genre = data["from_cdli_archival_view"]["genre"]
				#in case of empty entries
				if not genre:
					genre = "none"
			else:
				genre = "none"
				
			if "provenience" in data["from_cdli_archival_view"]:
				provenience = data["from_cdli_archival_view"]["provenience"]
				#in case of empty entries
				if not provenience:
					provenience = "none"
			else:
				provenience = "none"
				
			if "material" in data["from_cdli_archival_view"]:
				material = data["from_cdli_archival_view"]["material"]
				#in case of empty entries
				if not material:
					material = "none"
			else:
				material = "none"
			#for x,y in data["from_cdli_archival_view"].items():
				
				"""
				print("hi")
				try:
					language = p['language']
				except:
					language = "none"
				try:
					period = p['period']
				except:
					period = "none"
				try:
					genre = p['genre']
				except:
					genre = "none"
				try:
					provenience = p['provenience']
				except:
					provenience = "none"
				try:
					material = p['material']
				except:
					material = "none"
				"""
			#return "so","so","so","so","so"
			return language, period, genre, provenience, material
				
		else:
			#print("the cdli archival view does not exist")
			#for x in data:
				#print(x)
			return "none", "none", "none", "none", "none"
		#for p in data["from_cdli_archival_view"]:
			#print(p)
	#return "none", "none", "none", "none", "none"
	#return language, period, genre, provenience, material
	
def boxplotdatapreparation(language, period, genre, provenience, material):
	if(language=="none"):
		translatedLanguage = 0
	elif(language=="Sumerian"):
		translatedLanguage = 1
	elif(language=="Akkadian"):
		translatedLanguage = 2
	elif(language=="Hittite"):
		translatedLanguage = 3
	else:
		translatedLanguage = 42
	#Period should be ordered chronologically
	if(period=="none"):
		translatedPeriod = 0
	elif(period=="Ur III (ca. 2100-2000 BC)"):
		translatedPeriod = 1
	elif(period=="Old Akkadian (ca. 2340-2200 BC)"):
		translatedPeriod = 2
	elif(period=="Old Babylonian (ca. 1900-1600 BC)"):
		translatedPeriod = 3
	elif(period=="ED IIIb (ca. 2500-2340 BC)"):
		translatedPeriod = 4
	elif(period=="Middle Babylonian (ca. 1400-1100 BC)"):
		translatedPeriod = 5
	elif(period=="Old Assyrian (ca. 1950-1850 BC)"):
		translatedPeriod = 6
	elif(period=="Middle Hittite (ca. 1500-1100 BC)"):
		translatedPeriod = 7
	else:
		translatedPeriod = 42
	if(genre=="none"):
		translatedGenre = 0
	elif(genre=="Administrative"):
		translatedGenre = 1
	elif(genre=="Lexical; Mathematical"):
		translatedGenre = 2
	elif(genre=="Mathematical"):
		translatedGenre = 3
	elif(genre=="Prayer/Incantation"):
		translatedGenre = 4
	elif(genre=="Lexical"):
		translatedGenre = 5
	elif(genre=="Mathematical ?"):
		translatedGenre = 6
	elif(genre=="Literary"):
		translatedGenre = 7
	elif(genre=="Legal"):
		translatedGenre = 8
	elif(genre=="Letter"):
		translatedGenre = 9
	else:
		translatedGenre = 42
	if(provenience=="none"):
		translatedProvenience = 0	
	elif(provenience=="Nippur (mod. Nuffar)"):
		translatedProvenience = 1
	elif(provenience=="Nippur (mod. Nuffar) ?"):
		translatedProvenience = 2
	elif(provenience=="Puzris-Dagan (mod. Drehem)"):
		translatedProvenience = 3
	elif(provenience=="Puzris-Dagan (mod. Drehem) ?"):
		translatedProvenience = 4
	elif(provenience=="Kanesh (mod. Kütepe)"):
		translatedProvenience = 5
	elif(provenience=="Girsu (mod. Tello)"):
		translatedProvenience = 6
	elif(provenience=="Hattusa (mod. Bogazkale)"):
		translatedProvenience = 7
	elif(provenience=="uncertain (mod. uncertain)"):
		translatedProvenience = 8
	else:
		translatedProvenience = 42
	if(material=="none"):
		translatedMaterial = 0
	elif(material=="clay"):
		translatedMaterial = 1
	else:
		translatedMaterial = 42
	#print(translatedLanguage, translatedPeriod, translatedGenre, translatedProvenience, translatedMaterial)
	return translatedLanguage, translatedPeriod, translatedGenre, translatedProvenience, translatedMaterial




def main():
	
	print("Script was started")
	
	CSVsLocationString = "/home/damian/Desktop/extractedCSVs/*"
	JSONsLocationString = "/home/damian/Desktop/JSONCollection/"
	
	
	CSVFilesOpened = 0
	correspondingCSVExists = 0
	noEntry = 0
	
	
	
	
	grandHe = np.array([], dtype=float)
	grandVo = np.array([], dtype=float)
	
	grandLa = np.array([], dtype=object)
	grandPe = np.array([], dtype=object)
	grandGe = np.array([], dtype=object)
	grandPr = np.array([], dtype=object)
	grandMa = np.array([], dtype=object)
	
	angleProxyAllTablets = np.array([], dtype=float)
	
	['none', 'Mathematical', 'Mathematical ?', 'Lexical; Mathematical', 'Lexical', 'Literary', 'Prayer/Incantation', 'Legal', 'Letter', 'Administrative']
	#angleProxyInvestigation for genre
	angleProxyNone = np.array([], dtype=float)
	angleProxyMath = np.array([], dtype=float)
	angleProxyMathQ = np.array([], dtype=float)
	angleProxyLexMath = np.array([], dtype=float)
	angleProxyLex = np.array([], dtype=float)
	angleProxyLiter = np.array([], dtype=float)
	angleProxyPray = np.array([], dtype=float)
	angleProxyLegal = np.array([], dtype=float)
	angleProxyLett = np.array([], dtype=float)
	angleProxyAdmi = np.array([], dtype=float)
	angleProxyLexSch = np.array([], dtype=float)
	
	avHei = np.array([], dtype=float)
	
	
	avVol = np.array([], dtype=float)
	
	
	
	#for simple scatter plots
	languages = np.array([], dtype=object)
	periods = np.array([], dtype=object)
	genres = np.array([], dtype=object)
	proveniences = np.array([], dtype=object)
	materials = np.array([], dtype=object)
	
	#for boxplots
	"""
	languages = np.array([], dtype=int)
	periods = np.array([], dtype=int)
	genres = np.array([], dtype=int)
	proveniences = np.array([], dtype=int)
	materials = np.array([], dtype=int)
	"""

	for name in glob.glob(CSVsLocationString):
		#print(name)
		#print(os.path.basename(name))
		
		#a removable controll step for testing on smaller subsets
		if(CSVFilesOpened<20000):
			
			
	
			filename = os.fsdecode(name)
			if filename.endswith(".csv"):
				
				#print(filename)
				
				#CSVFileContent = openCSVFile(os.path.basename(name))
				CSVFileContent, angleProxy = openCSVFile(filename)
				
				CSVFilesOpened += 1
				
				language, period, genre, provenience, material = retrieveJSONInformation(JSONsLocationString, os.path.basename(name))
				correspondingCSVExists += 1
				
				if (language == 'none' and period == 'none' and genre == 'none' and provenience == 'none' and material == 'none'):
					noEntry += 1
				
				#languageBP, periodBP, genreBP, provenienceBP, materialBP = boxplotdatapreparation(language, period, genre, provenience, material)
				#print(language)
				languages = np.append(languages, language)
				periods = np.append(periods, period)
				genres = np.append(genres, genre)
				proveniences = np.append(proveniences, provenience)
				materials = np.append(materials, material)
				"""
				languages = np.append(languages, languageBP)
				periods = np.append(periods, periodBP)
				genres = np.append(genres, genreBP)
				proveniences = np.append(proveniences, provenienceBP)
				materials = np.append(materials, materialBP)
				"""
				
				angleProxyAllTablets = np.append(angleProxyAllTablets, angleProxy)
				
				#for genre specific proxy investigation
				
				if(genre=="none"):
					angleProxyNone = np.append(angleProxyNone, angleProxy)
				elif(genre=="Administrative"):
					angleProxyAdmi = np.append(angleProxyAdmi, angleProxy)
				elif(genre=="Lexical; Mathematical"):
					angleProxyLexMath = np.append(angleProxyLexMath, angleProxy)
				elif(genre=="Mathematical"):
					angleProxyMath = np.append(angleProxyMath, angleProxy)
				elif(genre=="Prayer/Incantation"):
					angleProxyPray = np.append(angleProxyPray, angleProxy)
				elif(genre=="Lexical"):
					angleProxyLex = np.append(angleProxyLex, angleProxy)
				elif(genre=="Lexical; School"):
					angleProxyLexSch = np.append(angleProxyLexSch, angleProxy)
				elif(genre=="Mathematical ?"):
					angleProxyMathQ = np.append(angleProxyMathQ, angleProxy)
				elif(genre=="Literary"):
					angleProxyLiter = np.append(angleProxyLiter, angleProxy)
				elif(genre=="Legal"):
					angleProxyLegal = np.append(angleProxyLegal, angleProxy)
				elif(genre=="Letter"):
					angleProxyLett = np.append(angleProxyLett, angleProxy)
				else:
					print("Some Genres were present in the data but were not planned")
					#print(genre)
				
				
				
				#CSVFileContent holds these floats
				# 1 RANSAC Quality
				# 1 Tetraeder Height
				# 12 = 4 * 3(=x,y,z coordinates, first set is tetraeder top)
				
				#look at Tetraeder Height
				heightPerTablet = CSVFileContent[:,1]
				
				#look at volume
				volumePerTablet = CSVFileContent[:,14]
				
				#print(heightPerTablet.shape)
				averagedTetraederHeight = np.mean(heightPerTablet)
				#print(type(averagedTetraederHeight)) # a numpy float
				
				averagedTetraederVolume = np.mean(volumePerTablet)
				
				#print(os.path.basename(name))
				#print(averagedTetraederHeight)
				
				avHei = np.append(avHei, averagedTetraederHeight)
				avVol = np.append(avVol, averagedTetraederVolume)
				
				#print(grandHe.shape)
				grandHe = np.concatenate((grandHe, heightPerTablet), axis=0)
				#print(grandHe.shape)
				grandVo = np.concatenate((grandVo, volumePerTablet), axis=0)
				
				#for i in range(heightPerTablet)
				
				languageFiller = np.full((heightPerTablet.size), language, dtype=object )
				grandLa = np.concatenate((grandLa, languageFiller), axis=0)
				periodFiller = np.full((heightPerTablet.size), period, dtype=object )
				grandPe = np.concatenate((grandPe, periodFiller), axis=0)
				genreFiller = np.full((heightPerTablet.size), genre, dtype=object )
				grandGe = np.concatenate((grandGe, genreFiller), axis=0)
				provenienceFiller = np.full((heightPerTablet.size), provenience, dtype=object )
				grandPr = np.concatenate((grandPr, provenienceFiller), axis=0)
				materialFiller = np.full((heightPerTablet.size), material, dtype=object )
				grandMa = np.concatenate((grandMa, materialFiller), axis=0)
			
		
		
	
	
	
	#print(grandLa.shape)
	#print(grandHe.shape)
	
	print("script has ended")
	print('   ', CSVFilesOpened, ' CSV files were looked at,\n   ', correspondingCSVExists, ' corresponding JSON files existed,\n   ', noEntry, ' JSON files did not have any relevant entry.') 
	
	rotationN = 80
	commonDPI = 200
	
	#scatterplots
	"""
	fig1, ax1 = plt.subplots()
	plt.xticks(rotation=rotationN)
	
	
	#print(languages)
	
	#LHData = [languages.astype(int), avHei]
	
	#ax1.boxplot(LHData)
	#ax1.plot(languages, avHei)
		
	ax1.scatter(languages, avHei)
	fig1Name = 'ScatterPlot showing the relationship between language and averaged wedge height'
	ax1.set_title(fig1Name)
	ax1.set_xlabel('Language')
	ax1.set_ylabel('Averaged Wedge Height')
	fig1.set_size_inches(8, 10)
	plt.savefig(fig1Name, dpi=commonDPI)
	
	
	fig2, ax2 = plt.subplots()
	plt.xticks(rotation=rotationN)
	
	ax2.scatter(periods, avHei)
	fig2Name = 'ScatterPlot showing the relationship between period and averaged wedge height'
	ax2.set_title(fig2Name)
	ax2.set_xlabel('Period')
	ax2.set_ylabel('Averaged Wedge Height')
	fig2.set_size_inches(8, 18)
	plt.savefig(fig2Name, dpi=commonDPI)
	
	
	fig3, ax3 = plt.subplots()
	plt.xticks(rotation=rotationN)
	
	ax3.scatter(genres, avHei)
	fig3Name = 'ScatterPlot showing the relationship between genre and averaged wedge height'
	ax3.set_title(fig3Name)
	ax3.set_xlabel('Genre')
	ax3.set_ylabel('Averaged Wedge Height')
	fig3.set_size_inches(8, 14)
	plt.savefig(fig3Name, dpi=commonDPI)
	
	
	fig4, ax4 = plt.subplots()
	plt.xticks(rotation=rotationN)
	
	ax4.scatter(proveniences, avHei)
	fig4Name = 'ScatterPlot showing the relationship between provenience and averaged wedge height'
	ax4.set_title(fig4Name)
	ax4.set_xlabel('Provenience')
	ax4.set_ylabel('Averaged Wedge Height')
	fig4.set_size_inches(8, 18)
	plt.savefig(fig4Name, dpi=commonDPI)
	
	fig5, ax5 = plt.subplots()
	plt.xticks(rotation=rotationN)
	
	ax5.scatter(materials, avHei)
	fig5Name = 'ScatterPlot showing the relationship between material and averaged wedge height'
	ax5.set_title(fig5Name)
	ax5.set_xlabel('Material')
	ax5.set_ylabel('Averaged Wedge Height')
	fig5.set_size_inches(8, 10)
	plt.savefig(fig5Name, dpi=commonDPI)
	
	
	fig6, ax6 = plt.subplots()
	plt.xticks(rotation=rotationN)
		
	ax6.scatter(languages, avVol)
	fig6Name = 'ScatterPlot showing the relationship between language and averaged wedge volume'
	ax6.set_title(fig6Name)
	ax6.set_xlabel('Language')
	ax6.set_ylabel('Averaged Wedge Volume')
	fig6.set_size_inches(8, 10)
	plt.savefig(fig6Name, dpi=commonDPI)
	
	
	fig7, ax7 = plt.subplots()
	plt.xticks(rotation=rotationN)
	
	ax7.scatter(periods, avVol)
	fig7Name = 'ScatterPlot showing the relationship between period and averaged wedge volume'
	ax7.set_title(fig7Name)
	ax7.set_xlabel('Period')
	ax7.set_ylabel('Averaged Wedge Volume')
	fig7.set_size_inches(8, 18)
	plt.savefig(fig7Name, dpi=commonDPI)
	
	
	fig8, ax8 = plt.subplots()
	plt.xticks(rotation=rotationN)
	
	ax8.scatter(genres, avVol)
	fig8Name = 'ScatterPlot showing the relationship between genre and averaged wedge volume'
	ax8.set_title(fig8Name)
	ax8.set_xlabel('Genre')
	ax8.set_ylabel('Averaged Wedge Volume')
	fig8.set_size_inches(8, 14)
	plt.savefig(fig8Name, dpi=commonDPI)
	
	
	fig9, ax9 = plt.subplots()
	plt.xticks(rotation=rotationN)
	
	ax9.scatter(proveniences, avVol)
	fig9Name = 'ScatterPlot showing the relationship between provenience and averaged wedge volume'
	ax9.set_title(fig9Name)
	ax9.set_xlabel('Provenience')
	ax9.set_ylabel('Averaged Wedge Volume')
	fig9.set_size_inches(8, 18)
	plt.savefig(fig9Name, dpi=commonDPI)
	
	
	fig10, ax10 = plt.subplots()
	plt.xticks(rotation=rotationN)
	
	ax10.scatter(materials, avVol)
	fig10Name = 'ScatterPlot showing the relationship between material and averaged wedge volume'
	ax10.set_title(fig10Name)
	ax10.set_xlabel('Material')
	ax10.set_ylabel('Averaged Wedge Volume')
	fig10.set_size_inches(8, 10)
	plt.savefig(fig10Name, dpi=commonDPI)
	"""
	#boxplots
	
	fig1, ax1 = plt.subplots()
	plt.xticks(rotation=rotationN)
	
	xTicks = ['none','Sumerian','Akkadian','Hittite']
	plt.boxplot((grandHe[grandLa=="none"],grandHe[grandLa=="Sumerian"],grandHe[grandLa=="Akkadian"],grandHe[grandLa=="Hittite"]))
	fig1Name = 'Box-Whisker-Plot showing the Relationship between Language and Wedge Depth'
	ax1.set_title(fig1Name)
	ax1.set_xlabel('Language')
	ax1.set_ylabel('Wedge Height in mm')
	#ax1.set_yscale('log')
	ax1.set_xticklabels(xTicks)
	fig1.set_size_inches(8, 10)
	plt.savefig(fig1Name, dpi=commonDPI)
	
	
	fig2, ax2 = plt.subplots()
	plt.xticks(rotation=rotationN)
	xTicks = ['none', 'ED IIIb (ca. 2500-2340 BC)', 'Old Akkadian (ca. 2340-2200 BC)', 'Ur III (ca. 2100-2000 BC)', 'Old Assyrian (ca. 1950-1850 BC)', 'Old Babylonian (ca. 1900-1600 BC)', 'Middle Hittite (ca. 1500-1100 BC)', 'Middle Babylonian (ca. 1400-1100 BC)']
	plt.boxplot((grandHe[grandPe=="none"],grandHe[grandPe=="ED IIIb (ca. 2500-2340 BC)"],grandHe[grandPe=="Old Akkadian (ca. 2340-2200 BC)"],grandHe[grandPe=="Ur III (ca. 2100-2000 BC)"],grandHe[grandPe=="Old Assyrian (ca. 1950-1850 BC)"],grandHe[grandPe=="Old Babylonian (ca. 1900-1600 BC)"],grandHe[grandPe=="Middle Hittite (ca. 1500-1100 BC)"],grandHe[grandPe=="Middle Babylonian (ca. 1400-1100 BC)"]))
	fig2Name = 'Box-Whisker-Plot showing the Relationship between Period and Wedge Depth'
	ax2.set_title(fig2Name)
	ax2.set_xlabel('Period')
	ax2.set_ylabel('Wedge Height in mm')
	#ax2.set_yscale('log')
	ax2.set_xticklabels(xTicks)
	fig2.set_size_inches(8, 24)
	plt.savefig(fig2Name, dpi=commonDPI)
	
	
	fig3, ax3 = plt.subplots()
	plt.xticks(rotation=rotationN)
	xTicks = ['none', 'Mathematical', 'Mathematical ?', 'Lexical; Mathematical', 'Lexical', 'Literary', 'Prayer/Incantation', 'Legal', 'Letter', 'Administrative']
	plt.boxplot((grandHe[grandGe=='none'],grandHe[grandGe=='Mathematical'],grandHe[grandGe=='Mathematical ?'],grandHe[grandGe=='Lexical; Mathematical'],grandHe[grandGe=='Lexical'],grandHe[grandGe=='Literary'],grandHe[grandGe=='Prayer/Incantation'],grandHe[grandGe=='Legal'],grandHe[grandGe=='Letter'],grandHe[grandGe=='Administrative']))
	fig3Name = 'Box-Whisker-Plot showing the Relationship between Genre and Wedge Depth'
	ax3.set_title(fig3Name)
	ax3.set_xlabel('Genre')
	ax3.set_ylabel('Wedge Height in mm')
	#ax3.set_yscale('log')
	ax3.set_xticklabels(xTicks)
	fig3.set_size_inches(8, 16)
	plt.savefig(fig3Name, dpi=commonDPI)
	
	
	fig4, ax4 = plt.subplots()
	plt.xticks(rotation=rotationN)
	xTicks = ['none', 'Puzriš-Dagan (mod. Drehem) ?', 'uncertain (mod. uncertain)', 'Puzriš-Dagan (mod. Drehem)', 'Girsu (mod. Tello)', 'Kanesh (mod. Kültepe)', 'Ḫattusa (mod. Boğazkale)', 'Nippur (mod. Nuffar) ?', 'Nippur (mod. Nuffar)']
	plt.boxplot((grandHe[grandPr=='none'],grandHe[grandPr=='Puzriš-Dagan (mod. Drehem) ?'],grandHe[grandPr=='uncertain (mod. uncertain)'],grandHe[grandPr=='Puzriš-Dagan (mod. Drehem)'],grandHe[grandPr=='Girsu (mod. Tello)'],grandHe[grandPr=='Kanesh (mod. Kültepe)'],grandHe[grandPr=='Ḫattusa (mod. Boğazkale)'],grandHe[grandPr=='Nippur (mod. Nuffar) ?'],grandHe[grandPr=='Nippur (mod. Nuffar)']))
	fig4Name = 'Box-Whisker-Plot showing the Relationship between Provenience and Wedge Depth'
	ax4.set_title(fig4Name)
	ax4.set_xlabel('Provenience')
	ax4.set_ylabel('Wedge Height in mm')
	#ax4.set_yscale('log')
	ax4.set_xticklabels(xTicks)
	fig4.set_size_inches(8, 20)
	plt.savefig(fig4Name, dpi=commonDPI)
	
	fig5, ax5 = plt.subplots()
	plt.xticks(rotation=rotationN)
	xTicks = ['none','clay']
	plt.boxplot((grandHe[grandMa=='none'],grandHe[grandMa=='clay']))
	fig5Name = 'Box-Whisker-Plot showing the Relationship between Material and Wedge Depth'
	ax5.set_title(fig5Name)
	ax5.set_xlabel('Material')
	ax5.set_ylabel('Wedge Height in mm')
	#ax5.set_yscale('log')
	ax5.set_xticklabels(xTicks)
	fig5.set_size_inches(8, 10)
	plt.savefig(fig5Name, dpi=commonDPI)
	
	
	fig6, ax6 = plt.subplots()
	plt.xticks(rotation=rotationN)
	xTicks = ['none','Sumerian','Akkadian','Hittite']
	plt.boxplot((grandVo[grandLa=="none"],grandVo[grandLa=="Sumerian"],grandVo[grandLa=="Akkadian"],grandVo[grandLa=="Hittite"]))
	fig6Name = 'Box-Whisker-Plot showing the Relationship between Language and wedge Volume'
	ax6.set_title(fig6Name)
	ax6.set_xlabel('Language')
	ax6.set_ylabel('Wedge Volume in cubig mm')
	#ax6.set_yscale('log')
	ax6.set_xticklabels(xTicks)
	fig6.set_size_inches(8, 10)
	plt.savefig(fig6Name, dpi=commonDPI)
	
	
	fig7, ax7 = plt.subplots()
	plt.xticks(rotation=rotationN)
	xTicks = ['none', 'ED IIIb (ca. 2500-2340 BC)', 'Old Akkadian (ca. 2340-2200 BC)', 'Ur III (ca. 2100-2000 BC)', 'Old Assyrian (ca. 1950-1850 BC)', 'Old Babylonian (ca. 1900-1600 BC)', 'Middle Hittite (ca. 1500-1100 BC)', 'Middle Babylonian (ca. 1400-1100 BC)']
	plt.boxplot((grandVo[grandPe=="none"],grandVo[grandPe=="ED IIIb (ca. 2500-2340 BC)"],grandVo[grandPe=="Old Akkadian (ca. 2340-2200 BC)"],grandVo[grandPe=="Ur III (ca. 2100-2000 BC)"],grandVo[grandPe=="Old Assyrian (ca. 1950-1850 BC)"],grandVo[grandPe=="Old Babylonian (ca. 1900-1600 BC)"],grandVo[grandPe=="Middle Hittite (ca. 1500-1100 BC)"],grandVo[grandPe=="Middle Babylonian (ca. 1400-1100 BC)"]))
	fig7Name = 'Box-Whisker-Plot showing the Relationship between Period and Wedge Volume'
	ax7.set_title(fig7Name)
	ax7.set_xlabel('Period')
	ax7.set_ylabel('Wedge Volume in cubic mm')
	#ax7.set_yscale('log')
	ax7.set_xticklabels(xTicks)
	fig7.set_size_inches(8, 24)
	plt.savefig(fig7Name, dpi=commonDPI)
	
	
	fig8, ax8 = plt.subplots()
	plt.xticks(rotation=rotationN)
	xTicks = ['none', 'Mathematical', 'Mathematical ?', 'Lexical; Mathematical', 'Lexical', 'Literary', 'Prayer/Incantation', 'Legal', 'Letter', 'Administrative']
	plt.boxplot((grandVo[grandGe=='none'],grandVo[grandGe=='Mathematical'],grandVo[grandGe=='Mathematical ?'],grandVo[grandGe=='Lexical; Mathematical'],grandVo[grandGe=='Lexical'],grandVo[grandGe=='Literary'],grandVo[grandGe=='Prayer/Incantation'],grandVo[grandGe=='Legal'],grandVo[grandGe=='Letter'],grandVo[grandGe=='Administrative']))
	fig8Name = 'Box-Whisker-Plot showing the Relationship between Genre and Wedge Volume'
	ax8.set_title(fig8Name)
	ax8.set_xlabel('Genre')
	ax8.set_ylabel('Wedge Volume in cubic mm')
	#ax8.set_yscale('log')
	ax8.set_xticklabels(xTicks)
	fig8.set_size_inches(8, 16)
	plt.savefig(fig8Name, dpi=commonDPI)
	
	
	fig9, ax9 = plt.subplots()
	plt.xticks(rotation=rotationN)
	xTicks = ['none', 'Puzriš-Dagan (mod. Drehem) ?', 'uncertain (mod. uncertain)', 'Puzriš-Dagan (mod. Drehem)', 'Girsu (mod. Tello)', 'Kanesh (mod. Kültepe)', 'Ḫattusa (mod. Boğazkale)', 'Nippur (mod. Nuffar) ?', 'Nippur (mod. Nuffar)']
	plt.boxplot((grandVo[grandPr=='none'],grandVo[grandPr=='Puzriš-Dagan (mod. Drehem) ?'],grandVo[grandPr=='uncertain (mod. uncertain)'],grandVo[grandPr=='Puzriš-Dagan (mod. Drehem)'],grandVo[grandPr=='Girsu (mod. Tello)'],grandVo[grandPr=='Kanesh (mod. Kültepe)'],grandVo[grandPr=='Ḫattusa (mod. Boğazkale)'],grandVo[grandPr=='Nippur (mod. Nuffar) ?'],grandVo[grandPr=='Nippur (mod. Nuffar)']))
	fig9Name = 'Box-Whisker-Plot showing the Relationship between Provenience and Wedge Volume'
	ax9.set_title(fig9Name)
	ax9.set_xlabel('Provenience')
	ax9.set_ylabel('Wedge Volume in cubic mm')
	#ax9.set_yscale('log')
	ax9.set_xticklabels(xTicks)
	fig9.set_size_inches(8, 20)
	plt.savefig(fig9Name, dpi=commonDPI)
	
	
	fig10, ax10 = plt.subplots()
	plt.xticks(rotation=rotationN)
	xTicks = ['none','clay']
	plt.boxplot((grandVo[grandMa=='none'],grandVo[grandMa=='clay']))
	fig10Name = 'Box-Whisker-Plot showing the Relationship between Material and Wedge Volume'
	ax10.set_title(fig10Name)
	ax10.set_xlabel('Material')
	ax10.set_ylabel('Wedge Volume in cubic mm')
	#ax10.set_yscale('log')
	ax10.set_xticklabels(xTicks)
	fig10.set_size_inches(8, 10)
	plt.savefig(fig10Name, dpi=commonDPI)
	"""
	#histograms
	
	#figure width and height
	figBr = 9
	figHe = 6
	
	#histogram of the angleProxy for all tablets
	plt.figure(figsize=(figBr, figHe))
	
	plt.hist(angleProxyAllTablets, bins=40)
	
	plt.title("Histogram of the Angle Proxy for all Tablets")
	plt.xlabel("Angle Proxy in mm")
	plt.ylabel("Number of Wedges")
	plt.yscale('log')
	#plt.xscale('log')
	plt.savefig("AngleProxyHistogramAllTablets.png", dpi=commonDPI)
	
	#separate histograms per genre
	plt.figure(figsize=(figBr, figHe))
	plt.hist(angleProxyNone, bins=40)
	plt.title("Histogram of the Angle Proxy for Tablets with no Genre")
	plt.xlabel("Angle Proxy in mm")
	plt.ylabel("Number of Wedges")
	plt.yscale('log')
	#plt.xscale('log')
	plt.savefig("AngleProxyHistogramGenreNone.png", dpi=commonDPI)
	
	plt.figure(figsize=(figBr, figHe))
	plt.hist(angleProxyMath, bins=40)
	plt.title("Histogram of the Angle Proxy for Tablets with Genre Mathematical")
	plt.xlabel("Angle Proxy in mm")
	plt.ylabel("Number of Wedges")
	plt.yscale('log')
	#plt.xscale('log')
	plt.savefig("AngleProxyHistogramGenreMath.png", dpi=commonDPI)
	
	plt.figure(figsize=(figBr, figHe))
	plt.hist(angleProxyMathQ, bins=40)
	plt.title("Histogram of the Angle Proxy for Tablets with Genre possibly Mathematical")
	plt.xlabel("Angle Proxy in mm")
	plt.ylabel("Number of Wedges")
	plt.yscale('log')
	#plt.xscale('log')
	plt.savefig("AngleProxyHistogramGenreMathQ.png", dpi=commonDPI)
	
	plt.figure(figsize=(figBr, figHe))
	plt.hist(angleProxyLexMath, bins=40)
	plt.title("Histogram of the Angle Proxy for Tablets with Genre Mathematical and Lexical")
	plt.xlabel("Angle Proxy in mm")
	plt.ylabel("Number of Wedges")
	plt.yscale('log')
	#plt.xscale('log')
	plt.savefig("AngleProxyHistogramGenreLexMath.png", dpi=commonDPI)
	
	plt.figure(figsize=(figBr, figHe))
	plt.hist(angleProxyLex, bins=40)
	plt.title("Histogram of the Angle Proxy for Tablets with Genre Lexical")
	plt.xlabel("Angle Proxy in mm")
	plt.ylabel("Number of Wedges")
	plt.yscale('log')
	#plt.xscale('log')
	plt.savefig("AngleProxyHistogramGenreLex.png", dpi=commonDPI)
	
	plt.figure(figsize=(figBr, figHe))
	plt.hist(angleProxyLexSch, bins=40)
	plt.title("Histogram of the Angle Proxy for Tablets with Genre Lexical and School")
	plt.xlabel("Angle Proxy in mm")
	plt.ylabel("Number of Wedges")
	plt.yscale('log')
	#plt.xscale('log')
	plt.savefig("AngleProxyHistogramGenreLexSch.png", dpi=commonDPI)
	
	plt.figure(figsize=(figBr, figHe))
	plt.hist(angleProxyLiter, bins=40)
	plt.title("Histogram of the Angle Proxy for Tablets with Genre Literary")
	plt.xlabel("Angle Proxy in mm")
	plt.ylabel("Number of Wedges")
	plt.yscale('log')
	#plt.xscale('log')
	plt.savefig("AngleProxyHistogramGenreLiter.png", dpi=commonDPI)
	
	plt.figure(figsize=(figBr, figHe))
	plt.hist(angleProxyPray, bins=40)
	plt.title("Histogram of the Angle Proxy for Tablets with Genre Prayer")
	plt.xlabel("Angle Proxy in mm")
	plt.ylabel("Number of Wedges")
	plt.yscale('log')
	#plt.xscale('log')
	plt.savefig("AngleProxyHistogramGenrePray.png", dpi=commonDPI)
	
	plt.figure(figsize=(figBr, figHe))
	plt.hist(angleProxyLegal, bins=40)
	plt.title("Histogram of the Angle Proxy for Tablets with Genre Legal")
	plt.xlabel("Angle Proxy in mm")
	plt.ylabel("Number of Wedges")
	plt.yscale('log')
	#plt.xscale('log')
	plt.savefig("AngleProxyHistogramGenreLegal.png", dpi=commonDPI)
	
	plt.figure(figsize=(figBr, figHe))
	plt.hist(angleProxyLett, bins=40)
	plt.title("Histogram of the Angle Proxy for Tablets with Genre Letter")
	plt.xlabel("Angle Proxy in mm")
	plt.ylabel("Number of Wedges")
	plt.yscale('log')
	#plt.xscale('log')
	plt.savefig("AngleProxyHistogramGenreLetter.png", dpi=commonDPI)
	
	plt.figure(figsize=(figBr, figHe))
	plt.hist(angleProxyAdmi, bins=40)
	plt.title("Histogram of the Angle Proxy for Tablets with Genre Administrative")
	plt.xlabel("Angle Proxy in mm")
	plt.ylabel("Number of Wedges")
	plt.yscale('log')
	#plt.xscale('log')
	plt.savefig("AngleProxyHistogramGenreAdmi.png", dpi=commonDPI)
	
	"""
	
	return 0



if __name__== "__main__":
	main()






#create a struct or class
#string Filename of csv
#averaged tetraeder height
#check if the computation of the tetraeder height is correct
#averaged tetraeder volume
#JSonInfo
#string language
#int era
#string type administrative and so on
#Schreibrichtung usw.


#go through all csv data

#go through json file

#PCA and language

#PCA and genre

#PCA 