This repository contains the programming code for the paper
"Period Classification of 3D Cuneiform Tablets with Geometric Neural Networks"
published at the ICFHR 2020 conference.

### Dataset

This project depends on the HeiCuBeDa open-access dataset that can be
retrieved from <https://doi.org/10.11588/data/IE8CCN>.
If you have the necessary hard disk space (2TB for the complete dataset)
its easiest to just download all the files.

Otherwise, you will need all
or a subset of the PLY files with the "\_GMOCF_r1.50_n4_v512.ply"
suffix to make sure that MSII feature vectors (the F in GMOCF) are baked
into the PLYs (as feature_vector properties per vertex).
For the baseline method, the easiest to start experimenting with,
you will need all or a subset of the PNG files ending with "\_03_front.png".
You will also need the "database.json" file from the the dataset that
holds metadata scraped from [CDLI](https://cdli.ucla.edu/) and connects it
to the PLY files.

You can visualize the dataset and get the indices for the training and test
data by running:

    $ python3 dataset.py visualize

### Running the baseline

Currently, the dataset paths, figure paths, logging paths, and result paths
are all hard coded to work on a specific machine at our lab.

The baseline is easiest to run. It expects the metadata at
`../data/database.json` and images at `/export/home/bbogacz/vNGG/tmp/Hilprecht_Sammlung/08_FINAL/HeiDATA/Images_MSII_Filter/`. It will write logs to `../logs/` and figures
to `../figures`. You _have_ to create these directories beforehand.

The script depends on:

    PIL, numpy, sklearn, matplotlib, pytorch, pytorch-vision

Invoke it with:

    $ python3 baseline.py

There are no command line switches.

### Preparing the geometric NN run

Running the methods in `relatedwork.py` and `main.py` requires preparation
using the `dataset.py` script. It will collect all eligible PLY files, extract
its vertices, faces, and feature vectors into dense numpy arrays and peform
a train/test split.

PLY files are exptected to be in any subdirectories below
`/export/home/bbogacz/vNGG/tmp/Hilprecht_Sammlung/08_FINAL/HeiDATA/`. Only
PLY files mentioned in `../data/database.json` will be accepted.
Results will be written to `../computed/`. Please note that for all PLYs,
around 1TB of data will be written.
This script takes around 14h to complete on my machine.

You will need in addition to the aforementioned packages:

    vtk, tqdm

Invoke the script with

    $ python3 dataset.py extract

Then, you can visualize stats of the extracted data with

    $ python3 dataset.py visualize

### Running the proposed method

Upon successful preparation of the data by, `main.py` can be run directly.
It only depends on the paths written to be `dataset.py`, i.e.
`../computed` for the dataset. The `main.py` writes to `../computed` to save
inferred model paramters, `../logs` to record train progress, and `../figures`
if invoked to visualize.

The script can be invoked to train with

    $ python3 main.py train

The run-time on my machine for 2TB is around 80h - 160h and requires 16GB
of video ram. Visualizations can be generated upon a successful run with

    $ python3 main.py visualize

### Running the related work methods

Similarily to the `main.py` script, `relatedwork.py` depends only on the
output of `dataset.py`.

You will need the following direct dependency

    torch_geometric

Afterwards, the script can be invoked with

    $ python3 relatedwork.py MiniSplineNet

for experiments with Fey et al. SplineNet [1], and

    $ python3 relatedwork.py MiniPointNet2

for experiments with Qi et al. PointNet++ [2].

### Refrences

[1] M. Fey, J. E. Lenssen, F. Weichert, and H. Müller, “SplineCNN: Fast
Geometric Deep Learning with Continous B-Spline Kernels,” Computer
Vision and Pattern Recognition, 2018.

[2] C. R. Qi, L. Yi, H. Su, and L. J. Guibas, “PointNet++: Deep Hierarchical
Feature Learning on Point Sets in a Metric Space,” Neural Information
Processing Systems, 2017.
