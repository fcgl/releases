#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys

import time
import json
import random

import numpy

import scipy.spatial

import sklearn.preprocessing
import sklearn.metrics

import torch
import torch_geometric

log_filename = None


def log(s):
    assert log_filename is not None
    with open(log_filename, "a") as f:
        print(s, file=f)


def render_histogram(description, tensor):
    array = tensor.detach().cpu().numpy()
    h = numpy.max(numpy.abs(array))
    v, m = numpy.var(array), numpy.mean(array)
    values, _ = numpy.histogram(array, bins=30, range=(-h, +h))
    bins = numpy.linspace(0, numpy.max(values), num=8)
    values = numpy.digitize(values, bins=bins)
    chars = numpy.array(list("  _.-+*=#"))
    output = "{:>17} {:> 7.3f}{:>+7.3f}: [{}] {:7.3f}"
    output = output.format(
        description, m, v, "".join(chars[values]), h)
    return output


def estimate_density(points, radius):
    kdtree = scipy.spatial.cKDTree(points)
    counts = numpy.zeros((10,))
    for i in range(10):
        j = random.randrange(0, points.shape[0])
        n = kdtree.query_ball_point(points[j], radius)
        counts[i] = len(n)
    return counts


def subsample_to(count, radius, points):
    upper = numpy.max(estimate_density(points, radius))
    factor = count / upper

    subset = numpy.random.random((points.shape[0],)) < factor
    subset = numpy.nonzero(subset)[0]
    subset = numpy.random.permutation(subset)

    log("Decimated {} by {:.3f} from {} to {} @{} giving {}.".format(
        points.shape[0], factor, upper, count, radius, subset.shape[0]))

    return subset


class Dataset(torch_geometric.data.Dataset):
    def __init__(self, metapath):
        super().__init__(root="/tmp/vNGG_pytorch_geometric")

        self._face2edge = torch_geometric.transforms.FaceToEdge()
        self._spherical = torch_geometric.transforms.Spherical()

        with open(metapath, "r") as f:
            metadata = json.load(f)

        self._number_cats = metadata["categories_number"]
        self._numbers = torch.tensor(metadata["numbers"], dtype=torch.long)
        self._label_cats = metadata["categories_label"]
        self._labels = torch.tensor(metadata["labels"], dtype=torch.long)
        self._hashes = metadata["hashes"]
        self.num_classes = self._labels.max().item() + 1

    @property
    def raw_file_names(self):
        return []

    @property
    def processed_file_names(self):
        return []

    def download(self):
        pass

    def process(self):
        pass

    def __len__(self):
        return self._labels.shape[0]

    def get(self, index):
        points = torch.tensor(numpy.load(
            "/export/home/bbogacz/computed/" +
            self._hashes[index] + "_points.npy"), dtype=torch.float)
        faces = torch.tensor(numpy.load(
            "/export/home/bbogacz/computed/" +
            self._hashes[index] + "_faces.npy"), dtype=torch.long)
        features = torch.tensor(numpy.load(
            "/export/home/bbogacz/computed/" +
            self._hashes[index] + "_features.npy"), dtype=torch.float)

        assert features.shape[1] == 1

        # Generate pytorch_geometric data object

        data = torch_geometric.data.Data(
            x=features, y=self._labels[index, None],
            pos=points, face=faces.transpose(0, 1))

        data = self._face2edge(data)

        # Return

        return data


class MiniSplineNet(torch.nn.Module):
    def __init__(self, num_classes):
        super().__init__()

        self._spherical = torch_geometric.transforms.Spherical(
            norm=True, cat=False)

        self.conv1 = torch_geometric.nn.SplineConv(
            in_channels=1,
            out_channels=8,
            dim=3,
            kernel_size=(4, 4, 4))

        self.conv2 = torch_geometric.nn.SplineConv(
            in_channels=8,
            out_channels=16,
            dim=3,
            kernel_size=(4, 4, 4))

        self.fc1 = torch.nn.Linear(
            in_features=16,
            out_features=64)

        self.fc2 = torch.nn.Linear(
            in_features=64,
            out_features=num_classes)

    def forward(self, data):

        # Convolve

        data = self._spherical(data)

        data.edge_attr[torch.isnan(data.edge_attr)] = 0

        data.x = self.conv1(data.x, data.edge_index, data.edge_attr)

        data.x = torch.nn.functional.elu(data.x)

        # Local Pool

        row, col = data.edge_index

        edge_attr = torch.norm(data.pos[row] - data.pos[col], p=2, dim=1)

        weight = torch_geometric.utils.normalized_cut(
            data.edge_index, edge_attr, num_nodes=data.pos.size(0))

        cluster = torch_geometric.nn.graclus(
            data.edge_index, weight, data.x.size(0))

        data = torch_geometric.nn.avg_pool(cluster, data)

        # Convolve

        data = self._spherical(data)

        data.edge_attr[torch.isnan(data.edge_attr)] = 0

        data.x = self.conv2(data.x, data.edge_index, data.edge_attr)

        data.x = torch.nn.functional.elu(data.x)

        # Local Pool

        row, col = data.edge_index

        edge_attr = torch.norm(data.pos[row] - data.pos[col], p=2, dim=1)

        weight = torch_geometric.utils.normalized_cut(
            data.edge_index, edge_attr, num_nodes=data.pos.size(0))

        cluster = torch_geometric.nn.graclus(
            data.edge_index, weight, data.x.size(0))

        data = torch_geometric.nn.avg_pool(cluster, data)

        # Global pool

        data.x = torch_geometric.nn.global_mean_pool(data.x, data.batch)

        # Classify

        data.x = self.fc1(data.x)

        data.x = torch.nn.functional.elu(data.x)

        data.x = self.fc2(data.x)

        return data.x


class MiniPointNet2(torch.nn.Module):
    def __init__(self,
                 num_classes,
                 sample1_radius, sample1_count,
                 sample2_radius, sample2_count):
        super().__init__()

        self._sample1_radius = sample1_radius
        self._sample1_count = sample1_count

        self._sample2_radius = sample2_radius
        self._sample2_count = sample2_count

        self.mlp1 = torch.nn.Sequential(
            torch.nn.Linear(1 + 3, 64),
            torch.nn.ELU(),
            torch.nn.BatchNorm1d(64)
        )

        self.mlp2 = torch.nn.Sequential(
            torch.nn.Linear(64 + 3, 128),
            torch.nn.ELU(),
           torch.nn.BatchNorm1d(128)
        )

        self.mlp3 = torch.nn.Sequential(
            torch.nn.Linear(128 + 3, 256),
            torch.nn.ELU(),
            torch.nn.BatchNorm1d(256)
        )

        self.pointconv1 = torch_geometric.nn.PointConv(self.mlp1)

        self.pointconv2 = torch_geometric.nn.PointConv(self.mlp2)

        self.fc1 = torch.nn.Sequential(
            torch.nn.Linear(256, 256),
            torch.nn.Dropout(p=0.5),
            torch.nn.ELU())
        # BatchNorm not applicable as only one sample per batch
        # torch.BatchNorm1d(512)

        self.fc3 = torch.nn.Linear(256, num_classes)

    def forward(self, data):
        x = data.x

        log(render_histogram("Input", x))

        # Farthest point sampling, or poisson sampling, as implemented here,
        # is too expensive to perform on meshes as large as the
        # HeiCuBeDa dataset.

        # idx = torch_geometric.nn.fps(x, data.batch, 0.01)

        # Instead, use simple random sampling to a fixed count of samples.
        # This allows us to predict the memory used and avoid going out of
        # memory.

        # Reduce document to a specific density

        subset1 = subsample_to(
            self._sample1_count,
            self._sample1_radius,
            data.pos.detach().cpu().numpy())
        subset1 = torch.tensor(subset1, dtype=torch.long, device=x.device)

        # Sample point clouds within a perimeter

        row, col = torch_geometric.nn.radius(
            data.pos, data.pos[subset1], self._sample1_radius,
            data.batch, data.batch[subset1],
            max_num_neighbors=self._sample1_count)

        edge_index = torch.stack([col, row], dim=0)

        # Compute PointNet convolution on sampled point clouds

        x = self.pointconv1(x, (data.pos, data.pos[subset1]), edge_index)

        log(render_histogram("Pointconv1", x))

        # Perform second stage of sub-sampling and convolution

        subset2 = subsample_to(
            self._sample2_count,
            self._sample2_radius,
            data.pos[subset1].detach().cpu().numpy())
        subset2 = torch.tensor(subset2, dtype=torch.long, device=x.device)

        # Convert from an index into subset1 to an index into data.pos
        subset2 = subset1[subset2]

        row, col = torch_geometric.nn.radius(
            data.pos[subset1], data.pos[subset2], self._sample2_radius,
            data.batch[subset1], data.batch[subset2],
            max_num_neighbors=self._sample2_count)

        edge_index = torch.stack([col, row], dim=0)

        x = self.pointconv2(
            x, (data.pos[subset1], data.pos[subset2]), edge_index)

        log(render_histogram("Pointconv2", x))

        # Perform global convolution and global pooling operation

        x = self.mlp3(torch.cat([x, data.pos[subset2]], dim=1))

        log(render_histogram("MLP3", x))

        x = torch_geometric.nn.global_max_pool(x, data.batch[subset2])

        log(render_histogram("Max Pool", x))

        # Final fully connected layer into set of classes

        x = self.fc1(x)

        log(render_histogram("FC1", x))

        x = self.fc3(x)

        log(render_histogram("FC3", x))

        return x


def summarize_dataset(dataset):
    log("Dataset initialized with {} samples and {} classes.".format(
        dataset._labels.shape[0], dataset.num_classes))


def summarize_train_step(epoch, dataset, index, loss, predicted, target):
    log("{} Epoch: {:>3} Mesh: {:<13} "
        "Loss: {:>7.4f} Target: {:>3} Predicted: {:>3}".format(
            time.strftime("%H:%M"),
            epoch,
            dataset._number_cats[dataset._numbers[index]],
            loss.item(),
            target,
            predicted))


def summarize_evaluation_epoch(epoch, target, predicted):
    report = sklearn.metrics.classification_report(
        target, predicted)

    confusion = sklearn.metrics.confusion_matrix(
        target, predicted)

    accuracy = numpy.sum(target == predicted) / target.shape[0]

    desc = "{} Epoch {} Evaluation accuracy {:.3f}".format(
        time.strftime("%H:%M"), epoch, accuracy)

    log(desc)
    log(report)
    log(confusion)

    print(desc)


def summarize_training_epoch(epoch, accumulated_loss, target, predicted):
    loss = accumulated_loss / target.shape[0]
    accuracy = numpy.sum(target == predicted) / target.shape[0]

    desc = "{} Epoch {} Training loss {:.4f} Training accuracy {:.3f}".format(
        time.strftime("%H:%M"), epoch, loss, accuracy)

    log(desc)

    print(desc)


def summarize_experiment_configuration(model, optimizer):
    start = time.strftime("%c")

    description = "Experiment started on {} with\n{}\n{}".format(
        start, str(model), str(optimizer))

    log(description)


def main():
    assert len(sys.argv) == 2 and \
           sys.argv[1] in ["MiniPointNet2", "MiniSplineNet"],\
        "Specify one of related.py [MiniPointNet2|MiniSplineNet]"
    approach = sys.argv[1]

    global log_filename

    log_filename = "../logs/" + time.strftime("%Y %m %d %H %M %S ") + \
                   approach + ".log"

    torch_geometric.set_debug(True)
    torch.autograd.set_detect_anomaly(True)

    # Initialize dataset and model for either approach

    train_set = Dataset(
        "/export/home/bbogacz/computed/heicubeda_train.json")
    test_set = Dataset(
        "/export/home/bbogacz/computed/heicubeda_test.json")

    summarize_dataset(train_set)
    summarize_dataset(test_set)

    if approach == "MiniPointNet2":
        learning_rate = 0.001
        device = torch.device("cpu")
        model = MiniPointNet2(train_set.num_classes, 3.0, 100, 6.0, 100).to(device)
    else:
        learning_rate = 0.01
        device = torch.device("cuda")
        model = MiniSplineNet(train_set.num_classes).to(device)

    # Initialize data loaders and optimizer

    train_loader = torch_geometric.data.DataLoader(train_set, shuffle=True)
    test_loader = torch_geometric.data.DataLoader(test_set, shuffle=False)

    criterion = torch.nn.CrossEntropyLoss()
    criterion.to(device)

    optimizer = torch.optim.Adam(
        model.parameters(), lr=learning_rate)

    summarize_experiment_configuration(model, optimizer)

    # Begin iterative training

    for epoch in range(200):

        # Train

        model.train()

        accumulated_loss = 0
        predicted = []
        target = []

        for n, data in enumerate(train_loader):

            data = data.to(device)

            optimizer.zero_grad()
            out = model(data)
            loss = criterion(out, data.y)
            loss.backward()
            optimizer.step()

            prd = out.argmax().item()
            tgt = data.y.item()

            accumulated_loss += loss.item()
            predicted.append(prd)
            target.append(tgt)

            summarize_train_step(epoch, train_set, n, loss, prd, tgt)

        predicted = numpy.array(predicted)
        target = numpy.array(target)

        summarize_training_epoch(epoch, accumulated_loss, target, predicted)

        # Test

        model.eval()

        predicted = []
        target = []

        for n, data in enumerate(test_loader):

            data = data.to(device)

            with torch.no_grad():
                out = model(data)

            prd = numpy.argmax(out.detach().cpu().numpy(), axis=1)
            tgt = data.y.detach().cpu().numpy()

            predicted.append(prd)
            target.append(tgt)

        predicted = numpy.array(predicted)
        target = numpy.array(target)

        summarize_evaluation_epoch(epoch, target, predicted)


if __name__ == "__main__":
    main()
