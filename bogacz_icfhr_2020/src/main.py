#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import math
import time
import json
import random
import pickle

import numpy
import numpy.linalg

import scipy.spatial
import scipy.ndimage
import scipy.ndimage.filters

import sklearn.metrics
import sklearn.cluster
import sklearn.mixture
import sklearn.decomposition

import matplotlib.pyplot
import matplotlib.cm

import vtk
import vtk.util.numpy_support

import torch
import torch.utils.checkpoint
import torch.autograd
import torch.nn.functional as tnnf
import torch.nn.init as tnni

log_filename = None


def log(s):
    assert log_filename is not None
    with open(log_filename, "a") as f:
        print(s, file=f)


def concentric_probes(begin, end, p_count, d_count):
    probes = numpy.meshgrid(
        numpy.linspace(0, numpy.pi * 2, num=p_count, endpoint=False),
        numpy.linspace(begin, end, num=d_count), sparse=False, indexing="ij")
    probes = numpy.stack((probes[0].ravel(), probes[1].ravel()), axis=1)
    return probes


def local_tangent_planes(points, normals):
    means = torch.mean(points, dim=1)

    bases = torch.zeros(
        (points.shape[0], 3, 3),
        dtype=points.dtype,
        device=points.device)

    bases[:, 0, :] = points[:, 0, :] - means[:, :]
    bases[:, 2, :] = torch.mean(normals, dim=1)

    bases[:, 1, :] = torch.cross(bases[:, 2, :], bases[:, 0, :])
    bases[:, 0, :] = torch.cross(bases[:, 2, :], bases[:, 1, :])

    bases[:, 0, :] = bases[:, 0, :] / torch.norm(bases[:, 0, :], dim=1, keepdim=True)
    bases[:, 1, :] = bases[:, 1, :] / torch.norm(bases[:, 1, :], dim=1, keepdim=True)
    bases[:, 2, :] = bases[:, 2, :] / torch.norm(bases[:, 2, :], dim=1, keepdim=True)

    return bases


def to_polar(cartesian, normals):
    assert len(cartesian.shape) == 3
    assert normals.shape == cartesian.shape

    # Local tangent plane

    means = torch.mean(cartesian, dim=1)
    bases = torch.zeros(
        (cartesian.shape[0], 3, 3),
        dtype=cartesian.dtype,
        device=cartesian.device)

    bases[:, 0, :] = cartesian[:, 0, :] - means[:, :]
    bases[:, 2, :] = torch.mean(normals, dim=1)

    bases[:, 1, :] = torch.cross(bases[:, 2, :], bases[:, 0, :])
    bases[:, 0, :] = torch.cross(bases[:, 2, :], bases[:, 1, :])

    bases[:, 0, :] = bases[:, 0, :] / torch.norm(bases[:, 0, :], dim=1, keepdim=True)
    bases[:, 1, :] = bases[:, 1, :] / torch.norm(bases[:, 1, :], dim=1, keepdim=True)
    bases[:, 2, :] = bases[:, 2, :] / torch.norm(bases[:, 2, :], dim=1, keepdim=True)

    # Angles to centers in local tangent plane

    vectors = cartesian - means[:, None, :]
    geodesic = torch.norm(vectors, dim=2)

    vectors = torch.stack(
        (torch.einsum("abc,ac->ab", vectors, bases[:, 0, :]),
         torch.einsum("abc,ac->ab", vectors, bases[:, 1, :])),
        dim=2)
    euclidean = torch.norm(vectors, dim=2)

    a1 = vectors[:, :, 0]
    a1 /= (euclidean[:, :] * 1) + 0.000001
    a1 = torch.clamp(a1, -1, 1)
    a1 = torch.acos(a1)

    a2 = vectors[:, :, 1]

    half1 = a2 <= 0
    half2 = a2 > 0

    a = torch.zeros_like(a1)
    a[half1] = a1[half1]
    a[half2] = 2 * math.pi - a1[half2]

    # Angles in local tangent plane but geodesic distances

    polar = torch.stack((a, geodesic), dim=2)

    return polar


def sample_rbf(probes, points, values):
    assert len(probes.shape) == 2
    assert len(points.shape) == 3
    assert len(values.shape) == 3
    assert points.shape[0] == values.shape[0]

    scale = 1.0 / torch.max(points[:, :, 1], dim=1)[0]

    theta = probes[None, :, None, 0] - points[:, None, :, 0]
    theta = torch.abs(theta)

    a = probes[None, :, None, 1] * scale[:, None, None]
    b = points[:, None, :, 1] * scale[:, None, None]

    sqdist = a * a + b * b - 2 * a * b * torch.cos(theta)
    weights = torch.exp(-100 * sqdist)

    samples = torch.einsum("abc,acd->abd", weights, values)
    samples /= weights.sum(dim=2, keepdim=True) + 0.000001

    return samples


def cartesian_sample_rbf(probes, points, values):
    assert len(probes.shape) == 2
    assert len(points.shape) == 3
    assert len(values.shape) == 3
    assert points.shape[0] == values.shape[0]

    scale = numpy.linalg.norm(points, axis=2)
    scale = numpy.max(scale, axis=1)
    scale = 1 / scale

    probes = probes[None, :, None, :] * scale[:, None, None, None]
    points = points[:, None, :, :] * scale[:, None, None, None]

    sq = probes - points
    sq = numpy.power(sq, 2)
    sq = numpy.sum(sq, axis=3)

    weights = numpy.exp(-100 * sq)

    samples = numpy.einsum("abc,acd->abd", weights, values)
    samples /= weights.sum(axis=2)[:, :, None] + 0.000001

    return samples


def batches_of(count, array):
    indices = numpy.arange(array.shape[0])

    for i in range(0, indices.shape[0], count):
        if i + count < indices.shape[0]:
            subset = numpy.arange(i, i + count)
        else:
            subset = numpy.arange(i, indices.shape[0])

        yield indices[subset]


def convolve_norm_activate_surface(
        *, points_cpu, knn, points, normals, features,
        probes, conv_biases, conv_weights, norm_biases, norm_weights):

    conv_out = torch.zeros(
        (points.shape[0], conv_weights.shape[0]),
        dtype=conv_weights.dtype,
        device=conv_weights.device)

    # Find neighbors

    kdtree = scipy.spatial.cKDTree(points_cpu)

    # Convolution

    for batch in batches_of(100, points_cpu):
        _, neighbors = kdtree.query(points_cpu[batch], knn)

        neighbors = torch.tensor(
            neighbors,
            dtype=torch.long,
            device=points.device)

        polars = to_polar(points[neighbors, :], normals[neighbors, :])
        samples = sample_rbf(probes, polars, features[neighbors, :])
        maps = (conv_biases[None, :] +
                torch.einsum("abc,dbc->ad", samples, conv_weights))
        conv_out[batch, :] = maps

    # Batch norm

    var, mean = torch.var_mean(conv_out)
    conv1_out = (conv_out - mean) / torch.sqrt(var + 0.000001)
    conv1_out = norm_weights * conv1_out + norm_biases

    # Activation function after convolution

    conv_out = torch.tanh(conv1_out)

    return conv_out


def pool_local_surface(
        *, points_cpu, knn, poolset_cpu, points, features):

    pool_out = torch.zeros(
        (points.shape[0], features.shape[1]),
        dtype=torch.float, device=points.device)

    kdtree = scipy.spatial.cKDTree(points_cpu)

    for batch in batches_of(1000, poolset_cpu):
        _, neighbors = kdtree.query(points_cpu[poolset_cpu[batch]], knn)

        pool_out[batch, :] = torch.mean(features[neighbors, :], dim=1)

    return pool_out


class Parameters:
    def __init__(self, count_labels):
        probe1 = concentric_probes(0.5, 3.0, 32, 16)
        probe1 = torch.tensor(
            probe1, dtype=torch.float, device="cuda")

        probe2 = concentric_probes(0.5, 6.0, 32, 16)
        probe2 = torch.tensor(
            probe2, dtype=torch.float, device="cuda")

        self.conv1_radius = 3.0
        self.conv1_samples = 100
        self.conv1_probe = probe1
        self.conv1_shape0 = 32
        self.conv1_shape1 = 16
        self.conv1_maps = 32

        self.conv2_radius = 6.0
        self.conv2_samples = 100
        self.conv2_probe = probe2
        self.conv2_shape0 = 32
        self.conv2_shape1 = 16
        self.conv2_maps = 64

        self.fc_width = 128

        # Parameters for optimization

        self.conv1_bias = torch.zeros(
            (self.conv1_maps,),
            dtype=torch.float, device="cuda", requires_grad=True)
        tnni.uniform_(self.conv1_bias, -0.01, 0.01)

        self.conv1_weight = torch.zeros(
            (self.conv1_maps,
             self.conv1_shape0 * self.conv1_shape1,
             1),
            dtype=torch.float, device="cuda", requires_grad=True)
        tnni.uniform_(self.conv1_weight, -0.01, 0.01)

        self.bnorm1_weight = torch.ones(
            (self.conv1_maps,),
            dtype=torch.float, device="cuda", requires_grad=True)

        self.bnorm1_bias = torch.zeros(
            (self.conv1_maps,),
            dtype=torch.float, device="cuda", requires_grad=True)

        self.conv2_bias = torch.zeros(
            (self.conv2_maps,),
            dtype=torch.float, device="cuda", requires_grad=True)
        tnni.uniform_(self.conv2_bias, -0.01, 0.01)

        self.conv2_weight = torch.zeros(
            (self.conv2_maps,
             self.conv2_shape0 * self.conv2_shape1,
             self.conv1_maps),
            dtype=torch.float, device="cuda", requires_grad=True)
        tnni.uniform_(self.conv2_weight, -0.01, 0.01)

        self.bnorm2_weight = torch.ones(
            (self.conv2_maps,),
            dtype=torch.float, device="cuda", requires_grad=True)

        self.bnorm2_bias = torch.zeros(
            (self.conv2_maps,),
            dtype=torch.float, device="cuda", requires_grad=True)

        self.fc_bias = torch.zeros(
            (self.fc_width,),
            dtype=torch.float, device="cuda", requires_grad=True)
        tnni.uniform_(self.fc_bias, -0.01, 0.01)

        self.fc_weight = torch.zeros(
            (self.fc_width,
             self.conv2_maps),
            dtype=torch.float, device="cuda", requires_grad=True)
        tnni.uniform_(self.fc_weight, -0.01, 0.01)

        self.bnorm3_weight = torch.ones(
            (1,),
            dtype=torch.float, device="cuda", requires_grad=True)

        self.bnorm3_bias = torch.zeros(
            (1,),
            dtype=torch.float, device="cuda", requires_grad=True)

        self.clf_bias = torch.zeros(
            (count_labels,),
            dtype=torch.float, device="cuda", requires_grad=True)
        tnni.uniform_(self.clf_bias, -0.01, 0.01)

        self.clf_weight = torch.zeros(
            (count_labels, self.fc_width),
            dtype=torch.float, device="cuda", requires_grad=True)
        tnni.uniform_(self.clf_weight, -0.01, 0.01)

    def to_tuple(self):
        return (self.conv1_bias, self.conv1_weight,
                self.bnorm1_bias, self.bnorm1_weight,
                self.conv2_bias, self.conv2_weight,
                self.bnorm2_bias, self.bnorm2_weight,
                self.fc_bias, self.fc_weight,
                self.bnorm3_bias, self.bnorm3_weight,
                self.clf_bias, self.clf_weight)


def model_forward(params, points_cpu, normals_cpu, features_cpu):

    # Prepare two stage subsampled mesh

    subset1_cpu = subsample_to(
        params.conv1_samples, params.conv1_radius, points_cpu)

    subset2_cpu = subsample_to(
        params.conv2_samples, params.conv2_radius, points_cpu[subset1_cpu])

    # Upload to CUDA

    points = torch.tensor(
        points_cpu, dtype=torch.float, device="cuda")

    normals = torch.tensor(
        normals_cpu, dtype=torch.float, device="cuda")

    features = torch.tensor(
        features_cpu, dtype=torch.float, device="cuda")

    subset1 = torch.tensor(subset1_cpu, dtype=torch.long, device="cuda")

    subset2 = torch.tensor(subset2_cpu, dtype=torch.long, device="cuda")

    # First convolution

    conv1_out = convolve_norm_activate_surface(
        points_cpu=points_cpu[subset1_cpu],
        knn=params.conv1_samples,
        points=points[subset1],
        normals=normals[subset1],
        features=features[subset1],
        probes=params.conv1_probe,
        conv_biases=params.conv1_bias,
        conv_weights=params.conv1_weight,
        norm_biases=params.bnorm1_bias,
        norm_weights=params.bnorm1_weight)

    log(render_histogram("Conv1 out", conv1_out.detach().cpu().numpy()))

    # Local pooling layer

    pool1_out = pool_local_surface(
        points_cpu=points_cpu[subset1_cpu],
        knn=params.conv2_samples,
        poolset_cpu=subset2_cpu,
        points=points[subset1],
        features=conv1_out)

    log(render_histogram("Pool1 out", pool1_out.detach().cpu().numpy()))

    # Second convolution

    conv2_out = convolve_norm_activate_surface(
        points_cpu=points_cpu[subset1_cpu][subset2_cpu],
        knn=params.conv2_samples,
        points=points[subset1][subset2],
        normals=normals[subset1][subset2],
        features=pool1_out,
        probes=params.conv2_probe,
        conv_biases=params.conv2_bias,
        conv_weights=params.conv2_weight,
        norm_biases=params.bnorm2_bias,
        norm_weights=params.bnorm2_weight)

    log(render_histogram("Conv2 out", conv2_out.detach().cpu().numpy()))

    # Global Pooling layer

    pool2_out = torch.mean(conv2_out, dim=0)

    log(render_histogram("Pool2 out", pool2_out.detach().cpu().numpy()))

    # Fully connected layer

    fc_out = tnnf.linear(pool2_out, params.fc_weight, params.fc_bias)

    # Batch normalize fully connected output

    var, mean = torch.var_mean(fc_out)
    fc_out = (fc_out - mean) / torch.sqrt(var + 0.000001)
    fc_out = params.bnorm3_weight * fc_out + params.bnorm3_bias

    # Activation function after fully connected

    fc_out = torch.tanh(fc_out)

    log(render_histogram("FC out", fc_out.detach().cpu().numpy()))

    # Classifier

    clf_out = tnnf.linear(fc_out, params.clf_weight, params.clf_bias)

    log(render_histogram("CLF out", clf_out.detach().cpu().numpy()))

    # Collect intermediate values

    return clf_out[None, :]


class SerializedDataset:
    def __init__(self, metapath):
        with open(metapath, "r") as f:
            metadata = json.load(f)

        self.ply_paths = metadata["ply_paths"]
        self.mat_paths = metadata["mat_paths"]
        self.number_categories = metadata["categories_number"]
        self.numbers = numpy.array(metadata["numbers"], dtype=int)
        self.label_categories = metadata["categories_label"]
        self.labels = numpy.array(metadata["labels"], dtype=int)
        self._hashes = metadata["hashes"]
        self.num_classes = numpy.max(self.labels) + 1

    def __len__(self):
        return len(self._hashes)

    def __getitem__(self, item):
        points = numpy.load("cache/" + self._hashes[item] + "_points.npy")

        normals = numpy.load("cache/" + self._hashes[item] + "_normals.npy")

        features = numpy.load("cache/" + self._hashes[item] + "_features.npy")

        log("Read " + str(item) + " with prefix " + self._hashes[item][:10])

        return points, normals, features


def summarize_dataset(dataset):
    label_values, label_counts = numpy.unique(
        dataset.labels, return_counts=True)

    number_values, number_counts = numpy.unique(
        dataset.numbers, return_counts=True)

    repetitions = number_counts == 1
    repeated = numpy.isin(dataset.numbers, number_values[repetitions])

    unique_values, unique_counts = numpy.unique(
        dataset.labels[repeated], return_counts=True)

    return "\n".join([
        "Read dataset with {} classes and {} samples.".format(
            dataset.num_classes, dataset.labels.shape[0]),
        "   All label {} counts {}".format(
            label_values.tolist(), label_counts.tolist()),
        "Unique label {} counts {}".format(
            unique_values.tolist(), unique_counts.tolist()),
    ])


def plot_dataset_statistics(dataset):
    label_values, label_counts = numpy.unique(
        dataset.labels, return_counts=True)

    number_values, number_counts = numpy.unique(
        dataset.numbers, return_counts=True)

    repetitions = number_counts == 1
    repeated = numpy.isin(dataset.numbers, number_values[repetitions])

    unique_values, unique_counts = numpy.unique(
        dataset.labels[repeated], return_counts=True)

    figure = matplotlib.pyplot.figure(figsize=(4, 4), dpi=300)

    axes = figure.add_subplot(1, 1, 1)

    assert label_values.shape[0] == unique_values.shape[0]

    sparse = numpy.arange(label_values.shape[0])

    axes.bar(sparse, label_counts, color="red")

    axes.bar(sparse, unique_counts)

    axes.set_xticks(sparse)
    axes.set_xticklabels([dataset.label_categories[i] for i in label_values],
        fontdict=dict(rotation=45, ha="right"))

    axes.set_ylabel("Count of tablets")
    axes.set_title("Distribution of label categories")

    # Make room for oversize annotations
    figure.tight_layout()

    return figure


def load_document(plypath, matpath):
    vtk_reader = vtk.vtkPLYReader()
    vtk_reader.SetFileName(plypath)
    vtk_reader.Update()

    vtk_normals = vtk.vtkPolyDataNormals()
    vtk_normals.SplittingOff()
    vtk_normals.SetInputData(vtk_reader.GetOutput())
    vtk_normals.Update()

    vtk_polydata = vtk_normals.GetOutput()

    vtk_points = vtk_polydata.GetPoints()
    vtk_normals = vtk_polydata.GetPointData().GetNormals()

    num_polys = vtk_polydata.GetPoints().GetNumberOfPoints()

    points = numpy.zeros((num_polys, 3))
    normals = numpy.zeros((num_polys, 3))

    for i in range(num_polys):
        points[i] = vtk_points.GetPoint(i)
        normals[i] = vtk_normals.GetTuple(i)

    matfile = numpy.loadtxt(matpath)

    assert matfile.shape[0] == num_polys

    features = numpy.zeros((num_polys, matfile.shape[1] - 1))
    features[matfile[:, 0].astype(int), :] = matfile[:, 1:]

    # Condition features

    features = features[:, 0, None]
    mean = numpy.mean(features)
    var = numpy.var(features)
    features = (features - mean) / numpy.sqrt(var + 0.000001)

    return vtk_polydata, points, normals, features


def render_scalars(plypath, indices, scalars):
    assert len(scalars.shape) == 1
    assert indices.shape[0] == scalars.shape[0]

    vtk_reader = vtk.vtkPLYReader()
    vtk_reader.SetFileName(plypath)
    vtk_reader.Update()

    vtk_normals = vtk.vtkPolyDataNormals()
    vtk_normals.SplittingOff()
    vtk_normals.SetInputData(vtk_reader.GetOutput())
    vtk_normals.Update()

    vtk_polydata = vtk_normals.GetOutput()

    points = vtk_polydata.GetPoints()

    colors_vtk = vtk.vtkUnsignedCharArray()
    colors_vtk.SetName("Colors")
    colors_vtk.SetNumberOfComponents(3)

    colormap = matplotlib.cm.ScalarMappable()
    colors = numpy.zeros((points.GetNumberOfPoints(), 4))
    colors[:] = (0.3, 0.3, 0.3, 1)
    colors[indices] = colormap.to_rgba(scalars)
    colors *= 255

    for i in range(colors.shape[0]):
        colors_vtk.InsertNextTuple(colors[i, :3])

    vtk_polydata.GetPointData().SetScalars(colors_vtk)

    poly_data_mapper = vtk.vtkPolyDataMapper()
    poly_data_mapper.SetInputData(vtk_polydata)

    actor = vtk.vtkActor()
    actor.SetMapper(poly_data_mapper)
    actor.GetProperty().LightingOff()

    renderer = vtk.vtkRenderer()
    renderer.SetBackground(1, 1, 1)
    renderer.AddActor(actor)

    window = vtk.vtkRenderWindow()
    window.SetOffScreenRendering(1)
    window.AddRenderer(renderer)
    window.SetSize(2048, 2048)
    window.Render()

    imagefilter = vtk.vtkWindowToImageFilter()
    imagefilter.SetInput(window)
    imagefilter.Update()

    writer = vtk.vtkPNGWriter()
    writer.SetWriteToMemory(1)
    writer.SetInputConnection(imagefilter.GetOutputPort())
    writer.Write()

    data = memoryview(writer.GetResult()).tobytes()

    return data


def estimate_density(points, radius):
    kdtree = scipy.spatial.cKDTree(points)
    counts = numpy.zeros((10,))
    for i in range(10):
        j = random.randrange(0, points.shape[0])
        n = kdtree.query_ball_point(points[j], radius)
        counts[i] = len(n)
    return counts


def subsample_to(count, radius, points):
    upper = numpy.max(estimate_density(points, radius))
    factor = count / upper

    subset = numpy.random.random((points.shape[0],)) < factor
    subset = numpy.nonzero(subset)[0]
    subset = numpy.random.permutation(subset)

    log("Decimated {} by {:.3f} from {} to {} @{} giving {}.".format(
        points.shape[0], factor, upper, count, radius, subset.shape[0]))

    return subset


def interpolate_local_surface(
        *, from_points, from_features, to_points):

    to_features = numpy.zeros(
        (to_points.shape[0], from_features.shape[1]))

    kdtree = scipy.spatial.cKDTree(from_points)

    for batch in batches_of(1000, to_points):
        _, neighbors = kdtree.query(to_points[batch], k=1)

        to_features[batch, :] = from_features[neighbors, :]

    return to_features


def render_embedding_on_mesh(*, interact):

    # Embedding

    ply_path = "../data/HS_0270c_GMOCF_r1.50_n4_v512.ply"
    mat_path = "../data/HS_0270c_GMOCF_r1.50_n4_v512.volume.mat.bz2"

    description = "HS 0270c"

    vtk_polydata, points, normals, features = \
        load_document(ply_path, mat_path)

    subset = subsample_to(100, 3.0, points)

    kdtree = scipy.spatial.cKDTree(points[subset])

    _, local = kdtree.query(points[subset[0]], 100)

    neighbors = subset[local]

    points_local = torch.tensor(points[neighbors])[None, :, :]
    normals_local = torch.tensor(normals[neighbors])[None, :, :]

    polars = to_polar(points_local, normals_local)[0]

    base = local_tangent_planes(points_local, normals_local)[0]

    ones = torch.ones(
        (polars.shape[0],),
        dtype=polars.dtype,
        device=polars.device)

    euclidean = torch.stack(
        (polars[:, 1] * torch.cos(polars[:, 0]),
         polars[:, 1] * -torch.sin(polars[:, 0]),
         ones * 5),
        dim=1)

    embedded = euclidean.matmul(base) + torch.mean(points_local[0, :, :], dim=0)

    embedded = embedded.numpy()

    scalars = interpolate_local_surface(
        from_points=points[subset],
        from_features=features[subset],
        to_points=points)

    scalars = scalars[:, 0]

    # VTK

    vtk_points = vtk_polydata.GetPoints()

    vtk_normals = vtk_polydata.GetPointData().GetNormals()

    num_points = vtk_polydata.GetPoints().GetNumberOfPoints()

    colors_vtk = vtk.vtkUnsignedCharArray()
    colors_vtk.SetName("Colors")
    colors_vtk.SetNumberOfComponents(3)

    colormap = matplotlib.cm.ScalarMappable()
    colors = colormap.to_rgba(scalars)
    colors *= 255

    for i in range(colors.shape[0]):
        colors_vtk.InsertNextTuple(colors[i, :3])

    vtk_polydata.GetPointData().SetScalars(colors_vtk)

    renderer = vtk.vtkRenderer()
    renderer.SetBackground(1, 1, 1)

    mean = numpy.mean(points[neighbors], axis=0)
    normal = numpy.mean(normals[neighbors], axis=0)
    normal = normal / numpy.linalg.norm(normal)

    # Camera is offset to base_x from the mean

    camera_position = mean + \
                      base[0].cpu().numpy() * 10 + \
                      base[2].cpu().numpy() * 20

    camera = renderer.GetActiveCamera()
    camera.SetPosition(camera_position)
    camera.SetFocalPoint(mean)
    camera.SetViewUp(points[neighbors[0]])
    camera.OrthogonalizeViewUp()

    poly_data_mapper = vtk.vtkPolyDataMapper()
    poly_data_mapper.SetInputData(vtk_polydata)
    actor = vtk.vtkActor()
    actor.SetMapper(poly_data_mapper)

    renderer.AddActor(actor)

    # for i in neighbors:
    #     algorithm = vtk.vtkSphereSource()
    #     algorithm.SetCenter(points[i])
    #     algorithm.SetRadius(0.1)
    #     algorithm.Update()
    #
    #     mapper = vtk.vtkPolyDataMapper()
    #     mapper.SetInputData(algorithm.GetOutput())
    #     mapper.ScalarVisibilityOff()
    #
    #     actor = vtk.vtkActor()
    #     actor.SetMapper(mapper)
    #     actor.GetProperty().SetColor(colors[i, :3] / 255)
    #
    #     renderer.AddActor(actor)

    for i, e in zip(neighbors, embedded):
        algorithm = vtk.vtkSphereSource()
        algorithm.SetCenter(e)
        algorithm.SetRadius(0.1)
        algorithm.Update()

        mapper = vtk.vtkPolyDataMapper()
        mapper.SetInputData(algorithm.GetOutput())
        mapper.ScalarVisibilityOff()

        actor = vtk.vtkActor()
        actor.SetMapper(mapper)
        actor.GetProperty().SetColor(colors[i, :3] / 255)

        renderer.AddActor(actor)

    for i, e in zip(neighbors, embedded):
        algorithm = vtk.vtkLineSource()
        algorithm.SetPoint1(points[i])
        algorithm.SetPoint2(e)
        algorithm.Update()

        mapper = vtk.vtkPolyDataMapper()
        mapper.SetInputData(algorithm.GetOutput())
        mapper.ScalarVisibilityOff()

        actor = vtk.vtkActor()
        actor.SetMapper(mapper)
        actor.GetProperty().SetColor((0.2, 0.2, 0.2))

        renderer.AddActor(actor)

    if interact is True:
        interactor = vtk.vtkRenderWindowInteractor()

        window = vtk.vtkRenderWindow()
        window.SetInteractor(interactor)
        window.AddRenderer(renderer)
        window.Render()

        interactor.Start()

    window = vtk.vtkRenderWindow()
    window.SetOffScreenRendering(1)
    window.AddRenderer(renderer)
    window.SetSize(2048, 2048)
    window.Render()

    imagefilter = vtk.vtkWindowToImageFilter()
    imagefilter.SetInput(window)
    imagefilter.Update()

    writer = vtk.vtkPNGWriter()
    writer.SetWriteToMemory(1)
    writer.SetInputConnection(imagefilter.GetOutputPort())
    writer.Write()

    data = memoryview(writer.GetResult()).tobytes()

    return data


def plot_model_parameters(params):

    # Conv 1 weights

    final_weight = params.conv1_weight.detach().cpu().numpy()
    final_weight = final_weight.reshape((params.conv1_maps,
                                         params.conv1_shape0,
                                         params.conv1_shape1,
                                         1))

    figure = matplotlib.pyplot.figure(figsize=(10, 10), dpi=300)

    for i in range(min(final_weight.shape[0], 24)):
        axes = figure.add_subplot(4, 6, i + 1)
        axes.imshow(final_weight[i, :, :, 0])

    figure.tight_layout()
    figure.savefig("results/proposed_conv1_weights.png")

    # Conv 2 weights

    final_weight = params.conv2_weight.detach().cpu().numpy()
    final_weight = final_weight.reshape((params.conv2_maps,
                                         params.conv2_shape0,
                                         params.conv2_shape1,
                                         params.conv1_maps))

    figure = matplotlib.pyplot.figure(figsize=(10, 10), dpi=300)

    for i in range(min(final_weight.shape[0], 24)):
        axes = figure.add_subplot(4, 6, i + 1)
        axes.imshow(final_weight[i, :, :, 0])

    figure.tight_layout()
    figure.savefig("results/proposed_conv2_weights.png")

    # Polar embedding conv1

    p_polar = params.conv1_probe.detach().cpu().numpy()
    values = params.conv1_weight.detach().cpu().numpy()

    points = numpy.stack(
        (numpy.cos(p_polar[:, 0]) * p_polar[:, 1],
         -numpy.sin(p_polar[:, 0]) * p_polar[:, 1]),
        axis=1)
    points = numpy.tile(points, (values.shape[0], 1, 1))

    p_cart = numpy.meshgrid(
        numpy.linspace(-3, 3, num=100, endpoint=False),
        numpy.linspace(-3, 3, num=100), sparse=False, indexing="ij")
    p_cart = numpy.stack(
        (p_cart[0].ravel(), p_cart[1].ravel()), axis=1)

    samples = cartesian_sample_rbf(p_cart, points, values)

    figure = matplotlib.pyplot.figure(figsize=(10, 10), dpi=300)

    for i in range(min(samples.shape[0], 16)):
        axes = figure.add_subplot(4, 4, i+1)
        axes.imshow(samples[i, :, 0].reshape(100, 100),
                    origin="lower",
                    extent=(-3, 3, -3, 3))
        axes.scatter(points[0, :, 1],
                     points[0, :, 0],
                     c='k',  # values[i, :, 0],
                     marker='.')

    figure.tight_layout()
    figure.savefig("results/proposed_conv1_cartesian.png")

    # Polar embedding conv2

    p_polar = params.conv2_probe.detach().cpu().numpy()
    values = params.conv2_weight.detach().cpu().numpy()

    points = numpy.stack(
        (numpy.cos(p_polar[:, 0]) * p_polar[:, 1],
         -numpy.sin(p_polar[:, 0]) * p_polar[:, 1]),
        axis=1)
    points = numpy.tile(points, (values.shape[0], 1, 1))

    p_cart = numpy.meshgrid(
        numpy.linspace(-6, 6, num=100, endpoint=False),
        numpy.linspace(-6, 6, num=100), sparse=False, indexing="ij")
    p_cart = numpy.stack(
        (p_cart[0].ravel(), p_cart[1].ravel()), axis=1)

    samples = cartesian_sample_rbf(p_cart, points, values)

    figure = matplotlib.pyplot.figure(figsize=(10, 10), dpi=300)

    for i in range(min(samples.shape[0], 16)):
        axes = figure.add_subplot(4, 4, i+1)
        axes.imshow(samples[i, :, 0].reshape(100, 100),
                    origin="lower",
                    extent=(-6, 6, -6, 6))
        axes.scatter(points[0, :, 1],
                     points[0, :, 0],
                     c='k',  # values[i, :, 0],
                     marker='.')

    figure.tight_layout()
    figure.savefig("results/proposed_conv2_cartesian.png")


def render_activation_maps(params):
    ply_path = "../data/HS_0092_GMOCF_r1.50_n4_v512.ply"
    mat_path = "../data/HS_0092_GMOCF_r1.50_n4_v512.volume.mat"

    vtk_polydata, points_cpu, normals_cpu, features_cpu = \
        load_document(ply_path, mat_path)

    all_indices = numpy.arange(points_cpu.shape[0])

    # Render initial features image

    image = render_scalars(ply_path, all_indices, features_cpu[:, 0])

    with open("results/proposed_features.png", "wb") as f:
        f.write(image)

    # Prepare two stage subsampled mesh

    subset1_cpu = subsample_to(
        params.conv1_samples, params.conv1_radius, points_cpu)

    subset2_cpu = subsample_to(
        params.conv2_samples, params.conv2_radius, points_cpu[subset1_cpu])

    # Upload to CUDA

    points = torch.tensor(
        points_cpu, dtype=torch.float, device="cuda")

    normals = torch.tensor(
        normals_cpu, dtype=torch.float, device="cuda")

    features = torch.tensor(
        features_cpu, dtype=torch.float, device="cuda")

    subset1 = torch.tensor(subset1_cpu, dtype=torch.long, device="cuda")

    subset2 = torch.tensor(subset2_cpu, dtype=torch.long, device="cuda")

    # First convolution

    with torch.no_grad():
        conv1_out = convolve_norm_activate_surface(
            points_cpu=points_cpu[subset1_cpu],
            knn=params.conv1_samples,
            points=points[subset1],
            normals=normals[subset1],
            features=features[subset1],
            probes=params.conv1_probe,
            conv_biases=params.conv1_bias,
            conv_weights=params.conv1_weight,
            norm_biases=params.bnorm1_bias,
            norm_weights=params.bnorm1_weight)

    # Render activation map

    interpolated = interpolate_local_surface(
        from_points=points_cpu[subset1_cpu],
        from_features=conv1_out.detach().cpu().numpy(),
        to_points=points_cpu)

    for i in range(8):
        name = "results/proposed_conv1_map_{}.png".format(i)

        image = render_scalars(
            ply_path, all_indices, interpolated[:, i])

        with open(name, "wb") as f:
            f.write(image)

    # First pooling

    with torch.no_grad():
        pool1_out = pool_local_surface(
            points_cpu=points_cpu[subset1_cpu],
            knn=params.conv2_samples,
            poolset_cpu=subset2_cpu,
            points=points[subset1],
            features=conv1_out)

    # Render activation map

    interpolated = interpolate_local_surface(
        from_points=points_cpu[subset1_cpu[subset2_cpu]],
        from_features=pool1_out.detach().cpu().numpy(),
        to_points=points_cpu)

    for i in range(8):
        name = "results/proposed_pool1_map_{}.png".format(i)

        image = render_scalars(
            ply_path, all_indices, interpolated[:, i])

        with open(name, "wb") as f:
            f.write(image)

    # Second convolution

    with torch.no_grad():
        conv2_out = convolve_norm_activate_surface(
            points_cpu=points_cpu[subset1_cpu][subset2_cpu],
            knn=params.conv2_samples,
            points=points[subset1][subset2],
            normals=normals[subset1][subset2],
            features=pool1_out,
            probes=params.conv2_probe,
            conv_biases=params.conv2_bias,
            conv_weights=params.conv2_weight,
            norm_biases=params.bnorm2_bias,
            norm_weights=params.bnorm2_weight)

    # Render activation map

    interpolated = interpolate_local_surface(
        from_points=points_cpu[subset1_cpu[subset2_cpu]],
        from_features=conv2_out.detach().cpu().numpy(),
        to_points=points_cpu)

    for i in range(8):
        name = "results/proposed_conv2_map_{}.png".format(i)

        image = render_scalars(
            ply_path, all_indices, interpolated[:, i])

        with open(name, "wb") as f:
            f.write(image)


def render_histogram(description, array):
    h = numpy.max(numpy.abs(array))
    v, m = numpy.var(array), numpy.mean(array)
    values, _ = numpy.histogram(array, bins=30, range=(-h, +h))
    bins = numpy.linspace(0, numpy.max(values), num=8)
    values = numpy.digitize(values, bins=bins)
    chars = numpy.array(list("  _.-+*=#"))
    output = "{:>17} {:> 7.3f}{:>+7.3f}: [{}] {:7.3f}"
    output = output.format(
        description, m, v, "".join(chars[values]), h)
    return output


def summarize_model_parameters(params):
    return "\n".join([
        render_histogram(
            "Conv1 bias", params.conv1_bias.detach().cpu().numpy()),
        render_histogram(
            "Conv1 weight", params.conv1_weight.detach().cpu().numpy()),
        render_histogram(
            "BN1 bias", params.bnorm1_bias.detach().cpu().numpy()),
        render_histogram(
            "BN1 weight", params.bnorm1_weight.detach().cpu().numpy()),

        render_histogram(
            "Conv2 bias", params.conv2_bias.detach().cpu().numpy()),
        render_histogram(
            "Conv2 weight", params.conv2_weight.detach().cpu().numpy()),
        render_histogram(
            "BN2 bias", params.bnorm2_bias.detach().cpu().numpy()),
        render_histogram(
            "BN2 weight", params.bnorm2_weight.detach().cpu().numpy()),

        render_histogram(
            "FC bias", params.fc_bias.detach().cpu().numpy()),
        render_histogram(
            "FC weight", params.fc_weight.detach().cpu().numpy()),
        render_histogram(
            "BN3 bias", params.bnorm3_bias.detach().cpu().numpy()),
        render_histogram(
            "BN3 weight", params.bnorm3_weight.detach().cpu().numpy()),

        render_histogram(
            "CLF bias", params.clf_bias.detach().cpu().numpy()),
        render_histogram(
            "CLF weight", params.clf_weight.detach().cpu().numpy()),

        render_histogram(
            "dConv1 bias", params.conv1_bias.grad.detach().cpu().numpy()),
        render_histogram(
            "dConv1 weight", params.conv1_weight.grad.detach().cpu().numpy()),
        render_histogram(
            "dBN1 bias", params.bnorm1_bias.grad.detach().cpu().numpy()),
        render_histogram(
            "dBN1 weight", params.bnorm1_weight.grad.detach().cpu().numpy()),

        render_histogram(
            "dConv2 bias", params.conv2_bias.grad.detach().cpu().numpy()),
        render_histogram(
            "dConv2 weight", params.conv2_weight.grad.detach().cpu().numpy()),
        render_histogram(
            "dBN2 bias", params.bnorm2_bias.grad.detach().cpu().numpy()),
        render_histogram(
            "dBN2 weight", params.bnorm2_weight.grad.detach().cpu().numpy()),

        render_histogram(
            "dFC bias", params.fc_bias.grad.detach().cpu().numpy()),
        render_histogram(
            "dFC weight", params.fc_weight.grad.detach().cpu().numpy()),
        render_histogram(
            "dBN3 bias", params.bnorm3_bias.grad.detach().cpu().numpy()),
        render_histogram(
            "dBN3 weight", params.bnorm3_weight.grad.detach().cpu().numpy()),

        render_histogram(
            "dCLF bias", params.clf_bias.grad.detach().cpu().numpy()),
        render_histogram(
            "dCLF weight", params.clf_weight.grad.detach().cpu().numpy()),
    ])


def summarize_training_step(epoch, dataset, index, loss, predicted):
    return "{} Epoch: {:>3} Mesh: {:<13} " \
       "Loss: {:>5.4f} Target: {:>3} Predicted: {:>3}".format(
            time.strftime("%H:%M"),
            epoch,
            dataset.number_categories[dataset.numbers[index]],
            loss.item(),
            dataset.labels[index],
            predicted)


def summarize_evaluation_epoch(epoch, target, predicted):
    return "\n".join([
        "{} Epoch: {:>3}".format(time.strftime("%H:%M"), epoch),
        sklearn.metrics.classification_report(
            target, predicted, zero_division=0),
        str(sklearn.metrics.confusion_matrix(
            target, predicted)),
    ])


def train_one_epoch(*, train_set, parameters, criterion, optimizer, epoch):

    # Shuffle train set on each epoch to prevent the network
    # becoming dependant on the order.

    shuffle = numpy.random.permutation(len(train_set))

    targets = []
    predictions = []
    accumulated_loss = 0

    for i in shuffle:

        # Convert and upload data to CUDA

        trgt = torch.tensor(
            (train_set.labels[i],),
            dtype=torch.long, device="cuda")

        points, normals, features = train_set[i]

        # Run optimization step

        optimizer.zero_grad()
        clf_out = model_forward(parameters, points, normals, features)
        loss = criterion(clf_out, trgt)
        loss.backward()
        optimizer.step()

        # Record and summarize training step progress

        pred = clf_out.argmax().item()
        targets.append(train_set.labels[i])
        predictions.append(pred)
        accumulated_loss += loss.item()

        log(summarize_model_parameters(parameters))
        log(summarize_training_step(epoch, train_set, i, loss, pred))

    # Record and summarize training epoch statistics

    predictions = numpy.array(predictions)
    targets = numpy.array(targets)

    epoch_loss = accumulated_loss / len(train_set)
    epoch_accuracy = numpy.sum(predictions == targets) / len(train_set)

    epoch_status = "{} Epoch {} Training loss {:.4f} " \
                   "Training accuracy {:.3f}.".format(
        time.strftime("%H:%M"), epoch, epoch_loss, epoch_accuracy)

    print(epoch_status)
    log(epoch_status)

    # Write out learned parameters for later visualization

    with open("results/proposed_parameters.pickle", "wb") as f:
        pickle.dump(parameters, f)


def evaluate_one_epoch(*, test_set, parameters, epoch):

    predictions, targets = [], []

    for i in range(len(test_set)):

        # Convert and upload data to CUDA

        trg = torch.tensor(
            (test_set.labels[i],),
            dtype=torch.long, device="cuda")

        points, normals, features = test_set[i]

        # Run forward pass with disabled gradients

        with torch.no_grad():
            clf_out = model_forward(parameters, points, normals, features)

        # Record prediction for evaluation statistics

        trg = trg.item()
        prd = numpy.argmax(clf_out.detach().cpu().numpy())

        targets.append(trg)
        predictions.append(prd)

    # Summarize evaluation epoch

    predictions = numpy.array(predictions)
    targets = numpy.array(targets)

    epoch_accuracy = numpy.sum(predictions == targets) / len(test_set)

    epoch_status = "{} Epoch {} Evaluation accuracy {:.3f}.".format(
        time.strftime("%H:%M"), epoch, epoch_accuracy)

    print(epoch_status)
    log(epoch_status)

    log(summarize_evaluation_epoch(epoch, predictions, targets))


def train():
    # Load training dataset

    train_set = SerializedDataset("results/heicubeda_train.json")

    test_set = SerializedDataset("results/heicubeda_test.json")

    log(summarize_dataset(train_set))
    log(summarize_dataset(test_set))

    # Initialize parameters and optimizer

    parameters = Parameters(train_set.num_classes)

    optimizer = torch.optim.Adam(
        parameters.to_tuple(), lr=0.01)

    criterion = torch.nn.CrossEntropyLoss()
    criterion = criterion.cuda()

    # Train network

    for epoch in range(200):
        train_one_epoch(
            train_set=train_set,
            parameters=parameters,
            criterion=criterion,
            optimizer=optimizer,
            epoch=epoch)

        evaluate_one_epoch(
            test_set=test_set,
            parameters=parameters,
            epoch=epoch)


def visualize():

    # Load datasets

    train_set = SerializedDataset("results/heicubeda_train.json")

    test_set = SerializedDataset("results/heicubeda_test.json")

    # Plot dataset statistics

    plot_dataset_statistics(train_set)\
        .savefig("results/proposed_train_set_stats.png")

    plot_dataset_statistics(test_set)\
        .savefig("results/proposed_test_set_stats.png")

    # Load and verify model parameters

    with open("results/proposed_parameters.pickle", "rb") as f:
        parameters = pickle.load(f)

    assert isinstance(parameters, Parameters)
    assert parameters.clf_weight.shape[0] == train_set.num_classes

    # Visualize model parameters

    with open("results/proposed_samples_on_mesh.png", "wb") as f:
        f.write(render_embedding_on_mesh(interact=False))

    plot_model_parameters(parameters)

    render_activation_maps(parameters)


if __name__ == "__main__":
    log_filename = "logs/" + time.strftime("%Y %m %d %H %M %S ") + \
                   "proposed.log"

    if len(sys.argv) == 2 and sys.argv[1] == "visualize":
        visualize()
    elif len(sys.argv) == 2 and sys.argv[1] == "train":
        train()
    else:
        assert False, "Specify one of main.py [train|visualize]"
