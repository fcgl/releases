#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import re
import math
import time
import json
import random

import PIL

import numpy

import torch
import torchvision

import sklearn.preprocessing
import sklearn.model_selection

import matplotlib.pyplot


class Dataset:
    def __init__(self):

        self._accept = 50
        self._take = 50

        with open("../data/database.json", "rb") as fobj:
            data = json.load(fobj)

        # Extract all tablets names and paths with a period is defined

        numbers1, paths1, labels1 = [], [], []

        for key, value in data.items():
            if "from_cdli_archival_view" in value:
                info = value["from_gigamesh_info_program"]
                archival = value["from_cdli_archival_view"]
                period = archival["period"]
                if period != "":
                    numbers1.append(key)
                    paths1.append(info["filename"])
                    labels1.append(archival["period"])

        # Resample to equal class counts

        self._categories = sorted(set(labels1))

        encode_numbers1 = sklearn.preprocessing.OrdinalEncoder()
        ord_numbers1 = encode_numbers1.fit_transform([(n,) for n in numbers1])
        ord_numbers1 = numpy.squeeze(ord_numbers1).astype(int)

        encode_labels1 = sklearn.preprocessing.OrdinalEncoder(
            categories=(self._categories,))
        ord_labels1 = encode_labels1.fit_transform([(l,) for l in labels1])
        ord_labels1 = numpy.squeeze(ord_labels1).astype(int)

        equal = self._resample_to_equal_classes(
            ord_labels1, accept=self._accept, take=self._take)

        self._orig_groups = ord_numbers1
        self._orig_labels = ord_labels1

        numbers1 = [numbers1[i] for i in equal]
        paths1 = [paths1[i] for i in equal]
        labels1 = [labels1[i] for i in equal]

        # Expand to side views available for a tablet

        numbers2, paths2, labels2 = [], [], []

        for num, root, label in zip(numbers1, paths1, labels1):
            for side in ["_03_front.png"]:  # "_06_back.png"]:
                path = ("/export/home/bbogacz/vNGG/tmp/Hilprecht_Sammlung"
                        "/08_FINAL/HeiDATA/Images_MSII_Filter/"
                        + root + side)
                path = path.replace("GMOCF", "HeiCuBeDa_GMOCF")
                assert os.path.exists(path), "File {} not found.".format(path)

                numbers2.append(num)
                paths2.append(path)
                labels2.append(label)

        # Expand to random crops

        numbers3, paths3, labels3, images3 = [], [], [], []

        for num, path, label in zip(numbers2, paths2, labels2):
            image = PIL.Image.open(path)
            image = image.resize((image.width // 3, image.height // 3))
            image = image.convert("L")

            if image.width <= 224 or image.height <= 224:
                continue

            samples = (image.width * image.height) / (224 * 224)
            samples = samples * 3
            samples = math.ceil(samples)

            for _ in range(samples):
                right = random.randrange(224, image.width)
                lower = random.randrange(224, image.height)
                left = right - 224
                upper = lower - 224

                crop = image.crop((left, upper, right, lower))

                numbers3.append(num)
                paths3.append(path)
                labels3.append(label)
                images3.append(crop)

        # Encode labels to ordinals

        self._encode_numbers = sklearn.preprocessing.OrdinalEncoder()
        ord_numbers = self._encode_numbers.fit_transform([(n,) for n in numbers3])
        ord_numbers = numpy.squeeze(ord_numbers).astype(int)

        self._encode_labels = sklearn.preprocessing.OrdinalEncoder(
            categories=(self._categories,))
        ord_labels = self._encode_labels.fit_transform([(l,) for l in labels3])
        ord_labels = numpy.squeeze(ord_labels).astype(int)

        # Equalize sampled classes once again!

        equal = self._resample_to_equal_classes(
            ord_labels, accept=200, take=200)

        # Slice and persist state

        self._hs_numbers = [numbers3[i] for i in equal]
        self._groups = ord_numbers[equal]
        self._paths = [paths3[i] for i in equal]
        self._desc_labels = [labels3[i] for i in equal]
        self._labels = ord_labels[equal]
        self._images = [images3[i] for i in equal]

        # Visible attributes

        self.num_labels = numpy.max(self._labels) + 1
        self.group_names = self._encode_numbers.categories_[0]
        self.label_names = self._encode_labels.categories_[0]
        self.short_names = numpy.array([
            re.search(r"([^(]*)", name)[0].strip()
            for name in self.label_names])

        # Transform

        self._transform = torchvision.transforms.Compose([
            torchvision.transforms.ToTensor(),
            torchvision.transforms.Normalize((0.0,), (1.0,)),
        ])

    @staticmethod
    def _resample_to_equal_classes(labels, *, accept, take):
        assert take <= accept
        values, counts = numpy.unique(labels, return_counts=True)
        accepted = counts > accept
        result = []
        for label in values[accepted]:
            indices = numpy.isin(labels, label).nonzero()[0]
            indices = numpy.random.choice(indices, (take,), replace=False)
            result.append(indices)
        return numpy.concatenate(result)

    def visualize_class_distribution(self):
        figure = matplotlib.pyplot.figure(figsize=(10, 10))

        # We need a special num_labels as we work on the
        # original label counts
        num_labels = numpy.max(self._orig_labels) + 1

        # Plot bar diagram of label and tablet counts

        axes = figure.add_subplot(1, 1, 1)
        values, counts = numpy.unique(self._labels, return_counts=True)
        axes.bar(values, counts)
        values, counts = numpy.unique(self._orig_labels, return_counts=True)
        axes.bar(values, counts, color="red")
        axes.set_xticks(
            numpy.arange(num_labels))
        axes.set_xticklabels(
            self.label_names, fontdict=dict(rotation=45, ha="right"))

        axes.plot((0, num_labels), (self._accept, self._accept),
                  color="gray", linestyle="dashed")
        axes.annotate(
            "Acceptance threshold for classification",
            xy=(3, self._accept), xytext=(0, 10), textcoords="offset pixels")

        axes.set_ylabel("Count of tablets (red) and extracted views (blue)")
        axes.set_title("Distribution of tablet (red) and extracted views (blue)")

        # Make room for oversize annotations
        figure.tight_layout()

        return figure

    def visualize_train_test(self, train, test):
        figure = matplotlib.pyplot.figure(figsize=(15, 5))

        axes = figure.add_subplot(1, 3, 1)
        values, counts = numpy.unique(self._labels, return_counts=True)
        axes.bar(values, counts)
        axes.set_xticks(
            numpy.arange(self.num_labels))
        axes.set_xticklabels(
            self.short_names, fontdict=dict(rotation=45, ha="right"))

        axes = figure.add_subplot(1, 3, 2)
        values, counts = numpy.unique(self._labels[train], return_counts=True)
        axes.bar(values, counts)
        axes.set_xticks(
            numpy.arange(self.num_labels))
        axes.set_xticklabels(
            self.short_names, fontdict=dict(rotation=45, ha="right"))

        axes = figure.add_subplot(1, 3, 3)
        values, counts = numpy.unique(self._labels[test], return_counts=True)
        axes.bar(values, counts)
        axes.set_xticks(
            numpy.arange(self.num_labels))
        axes.set_xticklabels(
            self.short_names, fontdict=dict(rotation=45, ha="right"))

        # Make room for oversize annotations
        figure.tight_layout()

        return figure

    def visualize_examples(self, indices):
        figure = matplotlib.pyplot.figure(figsize=(15, 15))

        images, groups, labels = next(self[indices])

        for i in range(20):
            axes = figure.add_subplot(4, 5, i + 1)
            axes.set_title("{}\n{}".format(
                self.group_names[groups[i]],
                self.short_names[labels[i]]))
            axes.imshow(images[i, 0, :, :].detach().cpu().numpy())

        return figure

    @staticmethod
    def _split_into_batches(array, *, size=20):
        indices = numpy.arange(array.shape[0])
        batches = []

        for i in range(0, indices.shape[0], size):
            if i + size < indices.shape[0]:
                subset = numpy.arange(i, i + size)
            else:
                subset = numpy.arange(i, indices.shape[0])
            batches.append(indices[subset])

        return batches

    def __getitem__(self, items):
        batches = self._split_into_batches(items)

        for batch in batches:
            tensors = [self._transform(self._images[items[i]]) for i in batch]
            tensors = torch.stack(tensors)
            groups = torch.tensor(self._groups[items[batch]], dtype=torch.long)
            labels = torch.tensor(self._labels[items[batch]], dtype=torch.long)

            yield tensors, groups, labels

    def summarize(self):
        return "{} crops".format(len(self._images))


def main():
    dataset = Dataset()

    dataset\
        .visualize_class_distribution()\
        .savefig("../figures/baseline_class_distribution.png")

    print(dataset.summarize())

    selection = sklearn.model_selection.GroupShuffleSplit(
        n_splits=5)
    train, test = next(selection.split(
        dataset._labels, dataset._labels, dataset._groups))

    train = numpy.random.permutation(train)
    test = numpy.random.permutation(test)

    dataset\
        .visualize_train_test(train, test)\
        .savefig("../figures/baseline_train_test.png")

    dataset\
        .visualize_examples(train)\
        .savefig("../figures/baseline_training_examples.png")

    resnet = torchvision.models.resnet50()
    resnet.conv1 = torch.nn.Conv2d(
        in_channels=1,
        out_channels=64,
        kernel_size=(7, 7),
        stride=(2, 2),
        padding=(3, 3),
        bias=False)
    resnet.fc = torch.nn.Linear(
        in_features=resnet.fc.in_features,
        out_features=dataset.num_labels,
        bias=True)
    network = resnet.cuda()

    criterion = torch.nn.CrossEntropyLoss()
    criterion = criterion.cuda()

    optimizer = torch.optim.Adam(
        network.parameters(), lr=0.0001)

    for epoch in range(200):

        train = numpy.random.permutation(train)
        test = numpy.random.permutation(test)

        # Train network

        network.train()

        count = 0
        acc = 0

        for count, (images, groups, labels) in enumerate(dataset[train]):
            images = images.cuda()
            labels = labels.cuda()

            optimizer.zero_grad()

            results = network(images)
            loss = criterion(results, labels)

            loss.backward()
            optimizer.step()

            acc += loss.item()

        # Evaluate network

        network.eval()

        images = []
        groups = []
        predicted = []
        labels = []

        for imgs, grps, lbls in dataset[test]:
            imgs = imgs.cuda()
            lbls = lbls.cuda()

            with torch.no_grad():
                results = network(imgs)

            _, prds = torch.max(results, dim=1)

            images.extend(imgs.detach().cpu().numpy())
            groups.extend(grps.detach().cpu().numpy())
            labels.extend(lbls.detach().cpu().numpy())
            predicted.extend(prds.detach().cpu().numpy())

        images = numpy.array(images)
        groups = numpy.array(groups)
        predicted = numpy.array(predicted)
        labels = numpy.array(labels)

        assert predicted.shape[0] == test.shape[0]
        assert labels.shape[0] == test.shape[0]

        correct = numpy.sum(predicted == labels)

        print("{} Epoch: {:3}, loss: {:.4f}, accuracy: {:.2f}"
              .format(time.strftime("%H:%M"), epoch, acc / count, correct / test.shape[0]))

        # Save classification report

        report = sklearn.metrics.classification_report(
            dataset.short_names[labels],
            dataset.short_names[predicted],
            zero_division=0)

        confusion = sklearn.metrics.confusion_matrix(labels, predicted)

        with open("../logs/baseline_test.log", "a") as f:
            print("Experiment on", time.strftime("%c"), "epoch", epoch, file=f)
            print(report, file=f)
            print(confusion, file=f)

    # Show example images

    figure = matplotlib.pyplot.figure(figsize=(15, 15))

    for n in range(20):
        axes = figure.add_subplot(4, 5, n + 1)
        axes.set_title("{}\nTrue: {}\nPred: {}".format(
            dataset.group_names[groups[n]],
            dataset.short_names[labels[n]],
            dataset.short_names[predicted[n]]))
        axes.imshow(images[n, 0, :, :])

    figure.savefig("../figures/baseline_test_examples.png")

    # Show classification report on exit

    print("Experiment on", time.strftime("%c"))
    print(report)
    print(confusion)


if __name__ == "__main__":
    main()
