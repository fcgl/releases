#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import json
import hashlib

import numpy

import matplotlib.pyplot

import sklearn.model_selection
import sklearn.preprocessing

import tqdm


class LocalDataSource:
    def __init__(self):
        number_categories = \
            ["HS_0092", "HS_0109", "HS_0270c",
             "HS_1006", "HS_1298", "HS_1535", "HS_1619"]

        numbers = numpy.array([0, 1, 2, 3, 4, 5, 6])

        label_categories = \
            ["0", "1", "2", "3", "4", "5", "6"]

        labels = numpy.array([0, 1, 2, 3, 4, 5, 6])

        ply_paths = \
            ["data/HS_0092_GMOCF_r1.50_n4_v512.ply",
             "data/HS_0109_GMOCF_r1.50_n4_v512.ply",
             "data/HS_0270c_GMOCF_r1.50_n4_v512.ply",
             "data/HS_1006_GMOCF_r1.50_n4_v512.ply",
             "data/HS_1298_GMOCF_r1.50_n4_v512.ply",
             "data/HS_1535_GMOCF_r1.50_n4_v512.ply",
             "data/HS_1619_GMOCF_r1.50_n4_v512.ply"]

        mat_paths = \
            ["data/HS_0092_GMOCF_r1.50_n4_v512.volume.mat",
             "data/HS_0109_GMOCF_r1.50_n4_v512.volume.mat",
             "data/HS_0270c_GMOCF_r1.50_n4_v512.volume.mat.bz2",
             "data/HS_1006_GMOCF_r1.50_n4_v512.volume.mat.bz2",
             "data/HS_1298_GMOCF_r1.50_n4_v512.volume.mat.bz2",
             "data/HS_1535_GMOCF_r1.50_n4_v512.volume.mat.bz2",
             "data/HS_1619_GMOCF_r1.50_n4_v512.volume.mat.bz2"]

        self.number_cats = number_categories
        self.label_cats = label_categories
        self.numbers = numbers
        self.labels = labels
        self.ply_paths = ply_paths
        self.mat_paths = mat_paths


def equalize_classes(*, labels, accept, take, train, test):

    # Select all labels with more than accept instances in train

    values, counts = numpy.unique(labels[train], return_counts=True)
    accepted = counts > accept

    # Select from train equal counts of classes

    train_equal = []

    for label in values[accepted]:
        indices = numpy.isin(labels[train], label).nonzero()[0]
        if take > indices.shape[0]:
            indices = numpy.random.choice(indices, (take,), replace=True)
        else:
            indices = numpy.random.choice(indices, (take,), replace=False)
        train_equal.append(indices)

    train_equal = numpy.concatenate(train_equal)

    # Select from test only labels used in train, however, without
    # re-sampling, thus, with unequal class distribution

    test_equal = []

    for label in values[accepted]:
        indices = numpy.isin(labels[test], label).nonzero()[0]
        test_equal.append(indices)

    test_equal = numpy.concatenate(test_equal)

    # Return re-sampling indices,
    # train_equal indexes into train, and
    # test_equal indexes into test.

    return train_equal, test_equal


def threshold_classes(*, labels, accept, take, train, test):

    # Select all labels with more than accept instances in train

    values, counts = numpy.unique(labels[train], return_counts=True)
    accepted = counts > accept

    # Select instances from train over the acceptance threshold

    train_accepted = []

    for label in values[accepted]:
        indices = numpy.isin(labels[test], label).nonzero()[0]
        train_accepted.append(indices)

    train_accepted = numpy.concatenate(train_accepted)

    # Select instances from test over the acceptance threshold

    test_accepted = []

    for label in values[accepted]:
        indices = numpy.isin(labels[test], label).nonzero()[0]
        test_accepted.append(indices)

    test_accepted = numpy.concatenate(test_accepted)

    # Return indices, classes will be imbalanced,
    # however, equally imbalanced between train and test
    # train_equal indexes into train, and
    # test_equal indexes into test.

    return train_accepted, test_accepted


def generate_from_vngg():

    # Find all tablets and associated having a period defined

    with open("data/database.json", "rb") as fobj:
        data = json.load(fobj)

    numbers, labels = [], []

    for key, value in data.items():
        if "from_cdli_archival_view" in value:
            archival = value["from_cdli_archival_view"]
            period = archival["period"]
            if period != "":
                numbers.append(key)
                labels.append(archival["period"])

    root = "/export/data/vNGG/tmp/Hilprecht_Sammlung/08_FINAL/HeiDATA/"

    all_paths = []

    for dirpath, dirnames, filenames in os.walk(root):
        for name in filenames:
            all_paths.append(os.path.join(dirpath, name))

    plypaths, matpaths = [], []

    for number in numbers:
        ply_spec = number.replace(" ", "_") + \
                   "_GMOCF_r1.50_n4_v512.ply"
        for path in all_paths:
            if ply_spec in path:
                plypaths.append(path)
                break

        mat_spec = number.replace(" ", "_") + \
            "_GMOCF_r1.50_n4_v512.volume.mat.bz2"
        for path in all_paths:
            if mat_spec in path:
                matpaths.append(path)
                break

    # Remove files that are too large

    less_then = []
    for i in range(len(plypaths)):
        info = os.stat(plypaths[i])
        if info.st_size > (300 * 1024 * 1024):
            continue
        less_then.append(i)

    numbers = [numbers[i] for i in less_then]
    labels = [labels[i] for i in less_then]
    plypaths = [plypaths[i] for i in less_then]
    matpaths = [matpaths[i] for i in less_then]

    # Assert that all columns are in sync

    assert len(labels) == len(numbers)
    assert len(labels) == len(plypaths)
    assert len(labels) == len(matpaths)

    # Encode numbers and labels

    encode_numbers = sklearn.preprocessing.OrdinalEncoder()
    numbers = encode_numbers.fit_transform([(l,) for l in numbers])
    numbers = numpy.squeeze(numbers).astype(int)

    encode_labels = sklearn.preprocessing.OrdinalEncoder()
    labels = encode_labels.fit_transform([(l,) for l in labels])
    labels = numpy.squeeze(labels).astype(int)

    # Generate ids

    hashes = [hashlib.sha224((plypaths[i]+matpaths[i]).encode()).hexdigest()
              for i in range(len(plypaths))]

    # Create train test split

    selection = sklearn.model_selection.KFold(n_splits=5, shuffle=True)
    train, test = next(selection.split(labels, labels))

    # Equalize classes

    train_chosen, test_chosen = equalize_classes(
        labels=labels,
        accept=30,
        take=100,
        train=train,
        test=test)

    # Check that no documents are in train _and_ test at the same time

    assert set(numbers[i] for i in train[train_chosen])\
        .intersection(numbers[i] for i in test[test_chosen]) == set(),\
        "Test set contaminated"

    # Save to JSON

    with open("results/heicubeda_train.json", "w") as f:
        json.dump({
            "categories_number": encode_numbers.categories_[0].tolist(),
            "categories_label": encode_labels.categories_[0].tolist(),
            "numbers": [int(numbers[i]) for i in train[train_chosen]],
            "labels": [int(labels[i]) for i in train[train_chosen]],
            "ply_paths": [plypaths[i] for i in train[train_chosen]],
            "mat_paths": [matpaths[i] for i in train[train_chosen]],
            "hashes": [hashes[i] for i in train[train_chosen]]
        }, f, indent=4)

    with open("results/heicubeda_test.json", "w") as f:
        json.dump({
            "categories_number": encode_numbers.categories_[0].tolist(),
            "categories_label": encode_labels.categories_[0].tolist(),
            "numbers": [int(numbers[i]) for i in test[test_chosen]],
            "labels": [int(labels[i]) for i in test[test_chosen]],
            "ply_paths": [plypaths[i] for i in test[test_chosen]],
            "mat_paths": [matpaths[i] for i in test[test_chosen]],
            "hashes": [hashes[i] for i in test[test_chosen]]
        }, f, indent=4)

    # Load PLY and save .mat files

    chosen_indices = set(train[train_chosen].tolist())\
        .union(test[test_chosen].tolist())

    for i in tqdm.tqdm(chosen_indices):
        if os.path.exists("cache/" + hashes[i] + "_indices.npy"):
            continue

        indices, points, normals, faces, features = \
            load_heicubeda(plypaths[i], matpaths[i])
        numpy.save("cache/" + hashes[i] + "_indices.npy", indices)
        numpy.save("cache/" + hashes[i] + "_points.npy", points)
        numpy.save("cache/" + hashes[i] + "_normals.npy", normals)
        numpy.save("cache/" + hashes[i] + "_faces.npy", faces)
        numpy.save("cache/" + hashes[i] + "_features.npy", features)


def load_heicubeda(ply_path, mat_path):
    import vtk

    vtk_reader = vtk.vtkPLYReader()
    vtk_reader.SetFileName(ply_path)
    vtk_reader.Update()

    # Compute normals

    vtk_normals = vtk.vtkPolyDataNormals()
    vtk_normals.SplittingOff()
    vtk_normals.SetInputData(vtk_reader.GetOutput())
    vtk_normals.Update()

    vtk_polydata = vtk_normals.GetOutput()

    # Get points and normals

    vtk_points = vtk_polydata.GetPoints()
    vtk_normals = vtk_polydata.GetPointData().GetNormals()

    num_points = vtk_polydata.GetPoints().GetNumberOfPoints()

    indices = numpy.arange(num_points)
    points = numpy.zeros((num_points, 3))
    normals = numpy.zeros((num_points, 3))

    for n in range(indices.shape[0]):
        points[n] = vtk_points.GetPoint(indices[n])
        normals[n] = vtk_normals.GetTuple(indices[n])

    # Get polygons

    num_cells = vtk_polydata.GetNumberOfCells()

    faces = []

    for i in range(num_cells):
        vtk_cell = vtk_polydata.GetCell(i)
        if vtk_cell.GetNumberOfPoints() == 3:
            faces.append((vtk_cell.GetPointId(0),
                          vtk_cell.GetPointId(1),
                          vtk_cell.GetPointId(2)))

    faces = numpy.array(faces, dtype=int)

    # Load computed features

    matfile = numpy.loadtxt(mat_path)

    assert matfile.shape[0] == num_points

    features = numpy.zeros((num_points, matfile.shape[1] - 1))
    features[matfile[:, 0].astype(int), :] = matfile[:, 1:]

    # Condition features

    features = features[:, 0, None]
    mean = numpy.mean(features)
    var = numpy.var(features)
    features = (features - mean) / numpy.sqrt(var + 0.000001)

    return indices, points, normals, faces, features


def visualize_dataset_filtering():
    # Find all tablets and associated having a period defined

    with open("data/database.json", "rb") as fobj:
        data = json.load(fobj)

    numbers, labels = [], []

    for key, value in data.items():
        if "from_cdli_archival_view" in value:
            archival = value["from_cdli_archival_view"]
            period = archival["period"]
            if period != "":
                numbers.append(key)
                labels.append(archival["period"])

    root = "/export/data/vNGG/tmp/Hilprecht_Sammlung/08_FINAL/HeiDATA/"

    all_paths = []

    for dirpath, dirnames, filenames in os.walk(root):
        for name in filenames:
            all_paths.append(os.path.join(dirpath, name))

    plypaths, matpaths = [], []

    for number in numbers:
        ply_spec = number.replace(" ", "_") + \
            "_GMOCF_r1.50_n4_v512.ply"
        for path in all_paths:
            if ply_spec in path:
                plypaths.append(path)
                break

        mat_spec = number.replace(" ", "_") + \
            "_GMOCF_r1.50_n4_v512.volume.mat.bz2"
        for path in all_paths:
            if mat_spec in path:
                matpaths.append(path)
                break

    # Assert that all columns are in sync

    assert len(labels) == len(numbers)
    assert len(labels) == len(plypaths)
    assert len(labels) == len(matpaths)

    # Mark files that are too large

    less_then = []
    for i in range(len(plypaths)):
        info = os.stat(plypaths[i])
        if info.st_size > (300 * 1024 * 1024):
            continue
        less_then.append(i)

    less_then = numpy.array(less_then)

    # Encode numbers and labels

    encode_numbers = sklearn.preprocessing.OrdinalEncoder()
    ord_numbers = encode_numbers.fit_transform([(l,) for l in numbers])
    ord_numbers = numpy.squeeze(ord_numbers).astype(int)
    number_categories = encode_numbers.categories_[0].tolist()

    encode_labels = sklearn.preprocessing.OrdinalEncoder()
    ord_labels = encode_labels.fit_transform([(l,) for l in labels])
    ord_labels = numpy.squeeze(ord_labels).astype(int)
    label_categories = encode_labels.categories_[0].tolist()

    # Create train test split

    selection = sklearn.model_selection.KFold(n_splits=5, shuffle=True)
    train, test = next(selection.split(ord_labels[less_then],
                                       ord_labels[less_then]))
    train, test = less_then[train], less_then[test]

    # Equalize classes

    train_chosen, test_chosen = equalize_classes(
        labels=ord_labels,
        accept=30,
        take=100,
        train=train,
        test=test)

    # Check that no documents are in train _and_ test at the same time

    assert set(numbers[i] for i in train[train_chosen])\
        .intersection(numbers[i] for i in test[test_chosen]) == set(),\
        "Test set contaminated"

    # Generate ids

    hashes = [hashlib.sha224((plypaths[i]+matpaths[i]).encode()).hexdigest()
              for i in range(len(plypaths))]

    # Save to JSON

    with open("results/heicubeda_train.json", "w") as f:
        json.dump({
            "categories_number": encode_numbers.categories_[0].tolist(),
            "categories_label": encode_labels.categories_[0].tolist(),
            "numbers": [int(ord_numbers[i]) for i in train[train_chosen]],
            "labels": [int(ord_labels[i]) for i in train[train_chosen]],
            "ply_paths": [plypaths[i] for i in train[train_chosen]],
            "mat_paths": [matpaths[i] for i in train[train_chosen]],
            "hashes": [hashes[i] for i in train[train_chosen]]
        }, f, indent=4)

    with open("results/heicubeda_test.json", "w") as f:
        json.dump({
            "categories_number": encode_numbers.categories_[0].tolist(),
            "categories_label": encode_labels.categories_[0].tolist(),
            "numbers": [int(ord_numbers[i]) for i in test[test_chosen]],
            "labels": [int(ord_labels[i]) for i in test[test_chosen]],
            "ply_paths": [plypaths[i] for i in test[test_chosen]],
            "mat_paths": [matpaths[i] for i in test[test_chosen]],
            "hashes": [hashes[i] for i in test[test_chosen]]
        }, f, indent=4)

    # Print unique counts for train and test

    print("All {}, above threshold train {} test {}".format(
        len(ord_labels),
        len(numpy.unique(train_chosen)),
        len(numpy.unique(test_chosen))))

    # Visualize distribution of classes

    figure = matplotlib.pyplot.figure(figsize=(5, 5), dpi=300)

    num_labels = len(label_categories)

    # Plot bar diagram of label and tablet counts

    axes = figure.add_subplot(1, 1, 1)

    values, counts = numpy.unique(ord_labels, return_counts=True)
    axes.bar(values, counts)

    axes.set_xticks(numpy.arange(num_labels))
    axes.set_xticklabels(label_categories,
                         fontdict=dict(rotation=45, ha="right"))

    axes.plot((0, num_labels - 1), (30, 30), color="gray", linestyle="dashed")
    axes.annotate(
        "Accept > 30",
        xy=(4, 30), xytext=(0, 10), textcoords="offset pixels")

    axes.set_ylabel("Count of tablets")
    axes.set_title("Distribution of label categories")

    # Make room for oversize annotations
    figure.tight_layout()

    figure.savefig("results/proposed_class_distribution.png")


if __name__ == "__main__":
    if len(sys.argv) == 2 and sys.argv[1] == "visualize":
        visualize_dataset_filtering()
    elif len(sys.argv) == 2 and sys.argv[1] == "extract":
        generate_from_vngg()
    else:
        assert False, "Specify one of dataset.py [visualize|extract]"
